/*
 * logger.h
 *
 * Created: 12/19/2012 10:53:41 PM
 *  Author: Meyer
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include "drivers/SDFAT/SDHandler.h"
#include "drivers/SDFAT/diskio.h"
#include "OBD/OBD.h"
#include "stdbool.h"

/**
 * @def WRITE_TO_SD
 * Write to the SD card instead of printing on serial.
 *
 * Should be defined in release mode
 */
#define WRITE_TO_SD

/**
 * @def LOGGER_PRINT
 * Print what is to be written to the log out on the debug UART port
 */
#define LOGGER_PRINT

/** @def LOG_MAX_PIDS
 * The maximum number of PIDs that can be recorded by the logger
 */
#define LOG_MAX_PIDS 30

/**
 * @def LOG_FILE_NAME_LENGTH
 *
 * Number of characters in the log file's name
 */
#define LOG_FILE_NAME_LENGTH 21

/**
 * @def LOG_ENTRY_LENGTH
 * The max number of characters per entry on the PID log
 */
#define LOG_ENTRY_LENGTH 8

/**
 * @def LOG_SEPARATOR_CHAR
 * Character used to separate log entries
 */
#define LOG_SEPARATOR_CHAR '\t'

typedef struct {
    //char	logFileName[LOG_FILE_NAME_LENGTH];	/* Name of file that is being written to */
    char	*logFileName;						/* Name of file that is being written to */
    uint8_t pids[LOG_MAX_PIDS];					/* PIDs in order to be written. Index 0 is first */
    uint8_t numberOfPids;						/* How many PIDs are being stored */
    uint8_t frequency;							/* Number of 100ms intervals between entries */
    volatile obd_pid_list *queryList;			/* PID list to attach the PIDs to */
} logger;

logger globalLogger;

typedef enum {
    LOGGER_LINE_HEADER,	/* Line to write is contains the PID and its unit */
    LOGGER_LINE_DATA	/* Line contains the data for each PID */
} loggerLineType;

extern uint8_t loggerPids[LOG_MAX_PIDS];

FRESULT logger_init(logger *inLogger, uint8_t inPids[], uint8_t numberOfPids, uint8_t frequency, volatile obd_pid_list *inQueryList);
FRESULT logger_interrupt_handler(void);
FRESULT logger_write_line(logger *inLogger, loggerLineType lineType);
bool logger_add_pid(logger *inLogger, uint8_t inPid);
//void logger_free(logger *inLogger);
bool logger_remove_pid(logger *inLogger, uint8_t inPid);
void logger_clear(logger *inLogger);
void logger_write_header(logger *inLogger);
#endif /* LOGGER_H_ */