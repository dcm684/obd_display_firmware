/*
 * InterruptRoutines.h
 *
 * Created: 9/19/2012 11:50:30 AM
 *  Author: Meyer
 */


#ifndef INTERRUPTROUTINES_H_
#define INTERRUPTROUTINES_H_

/* /def LED_UPDATE_FREQ
	How long between LED update in 0.1s increments
*/
#define LED_UPDATE_FREQ 3

/* /def DISPLAY_UPDATE_FREQ
	How long between display updates in 0.1s increments
*/
#define DISPLAY_UPDATE_FREQ 3

/* /def ACCEL_UPDATE_FREQ
	How long between accelerometer readings in 0.1s increments
*/
#define ACCEL_UPDATE_FREQ 1

/* /def OBD_REQUEST_SET_FREQ
	How long between sets of requests in 0.1s increments
*/
#define OBD_REQUEST_SET_FREQ 3

volatile uint8_t loggingCounter;
volatile uint8_t ledUpdateCounter;
volatile uint8_t displayUpdateCounter;
volatile uint8_t accelUpdateCounter;
volatile uint8_t obdUpdateCounter;

volatile uint8_t counter_10Hz;
volatile uint8_t counter_100Hz;

volatile bool process_display;
volatile bool process_led_bar;
volatile bool process_OBD;
volatile bool process_log;

void init_1kHz_interrupt(void);
//static void interrupt_1kHz(void);

#endif /* INTERRUPTROUTINES_H_ */