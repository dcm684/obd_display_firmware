/**
 * \file
 *
 * \brief Empty user application template
 *
 */

/*
 * Include header files for all drivers that have been imported from
 * Atmel Software Framework (ASF).
 */
#include <asf.h>
#include <inttypes.h>
#include <stdio.h>

#include "OBD_Display.h"
#include "drivers/LED_Bar.h"
#include "drivers/MMA7361.h"
#include "drivers/MCP2515.h"
#include "drivers/MCP7940.h"
#include "drivers/USB_Handler.h"
#include "drivers/Nav_Switch.h"

#include "drivers/GDM/GDM12864H.h"
#include "drivers/GDM/GDM12864H_Text.h"
#include "drivers/GDM/GDM12864H_Draw.h"

#include "drivers/USART_Handler.h"

#include "drivers/SDFAT/SDHandler.h"
#include "drivers/SDFAT/ff.h"

#include "logger.h"
#include "Display.h"

#include "OBD/OBD.h"
#include "OBD/OBDPidFunctions.h"

#include "InterruptRoutines.h"

#include "TestRoutines/TestRoutines.h"

int main(void)
{
    rtc_time timeNow = {
        .hour = 11,
        .minute = 59,
        .second = 56,
        .am_pm = RTC_PM
    };
    rtc_date dateNow = {
        .year = 13,
        .month = 02,
        .date = 28,
        .day = RTC_SUN
    };

    sysclk_init(); /* Initialize system clock. Used by ADC and USB */
    board_init();
    ioport_init();

    usart_init();
    //usb_init();

    led_bar_init();
    nav_init();

    twi_init();

    if(!rtc_battery_powered()) {
        rtc_init(&timeNow, &dateNow);

    } else {
        rtc_clear_battery_timestamps();
    }

#ifndef WRITE_TO_SD
    fputs_P(PSTR("Logger will not write to SD\r\n"), &usart_serial_stream);
#endif

    accel_init(&accelChans, ACCEL_GS_15, 10);

    /* Initializes MCP2515 and OBD functions */
    if(can_init(can_baud_500k)) {
        obd_init();

#ifdef CAN_LOOPBACK_TEST
        fputs_P(PSTR("CAN Loopback Mode\r\n"), &usart_serial_stream);

        if(can_set_mode(can_mode_loopback)) {
#else

        if(can_set_mode(can_mode_normal)) {
#endif
            fputs_P(PSTR("CAN init successful\r\n"), &usart_serial_stream);

        } else {
            fputs_P(PSTR("Failed to enter end mode\r\n"), &usart_serial_stream);
        }

    } else {
        fputs_P(PSTR("Failed to init\r\n"), &usart_serial_stream);
    }

    /* Initialize the timer used to control all asynch functions */
    init_1kHz_interrupt();

    /* Initializes the Graphic Display Module */
    gdm_init();
    /* Init again since the GDM needs init-ed twice after power up */
    gdm_init();
    gdm_font_init();
    gdm_puts_P(PSTR("Hello computer."));
    delay_ms(500);
    gdm_clear_display();
    gdm_move_cursor(0, 0);

    fputs(PSTR("\r\nWelcome to my program...\r\n\n"), &usart_serial_stream);

    pmic_init(); /* Enable peripheral interrupts */
    irq_initialize_vectors();

    //led_bar_set_pattern(0, 10, LED_PATTERN_RADIATE |LED_PATTERN_FILL);
    //led_bar_set_leds(0x1111);

    fputc(0x07, &usart_serial_stream);

    /* If an SD card is present initialize it */
    sd_check_and_init();

    /* Initialize the logger */
#if 0
    uint8_t loggerPids[] = {pid_maf_rate,
                            pid_engine_rpm,
                            pid_vehicle_speed,
                            pid_fuel_economy,
                            pid_control_module_volt,
                            pid_accelerometer_x,
                            pid_accelerometer_y,
                            pid_accelerometer_z
                           };

    if(FR_OK != logger_init(&globalLogger, loggerPids, 8, 10, &pidsToQuery)) {
        fprintf(&usart_serial_stream, "Failed to init logger\r\n");

    }
#endif


    /* Start requesting PIDs */
    obd_send_multiple_requests(&pidsToQuery);

    //test_logger_obd(); for(;;);
    //test_sd_create_read_delete(&usart_serial_stream); for(;;);
    //test_pid_translate(); for(;;);
#if 1
    test_settings_load_file();
    for(;;) {
        if(process_led_bar) {
            test_print_accel();
            led_bar_interrupt_handler();
            process_led_bar = false;
        }
    }
#endif
    for(;;) {
        //test_gdm_draw_bar_graph();
        //test_gdm_struct();
        //test_gdm_draw_object();
    }

    for(;;) {

        stdio_usb_vbus_event(USB_ATTACHED());

        if(sd_check_and_init()) {
            fputs_P(PSTR("Restarting log\r\n"), &usart_serial_stream);

            if(globalLogger.numberOfPids > 0) {
                /* Start a new log file once the SD card has been
                inserted. Use the values from before since they have
                not changed */
                if(FR_OK != logger_init(&globalLogger,
                                        loggerPids,
                                        globalLogger.numberOfPids,
                                        globalLogger.frequency,
                                        &pidsToQuery)) {
                    fprintf(&usart_serial_stream, "Failed to re-init logger\r\n");

                }
            }
        }

        /* Process everything that is based on time */
        if(process_log) {
            logger_interrupt_handler();
            process_log = false;
        }

        if(process_led_bar) {
            led_bar_interrupt_handler();
            process_led_bar = false;
        }

        if(process_display) {

            display_refresh();

            process_display = false;
        }

        if(process_OBD) {
            obd_timer_interrupt_handler();
            process_OBD = false;
        }
    }
}
