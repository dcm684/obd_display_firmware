/*
 * InterruptRoutines.c
 *
 * Created: 9/19/2012 11:31:58 AM
 *  Author: Meyer
 */

#include <tc.h>

#include "InterruptRoutines.h"
#include "drivers/SDFAT/SDHandler.h"
#include "drivers/LED_Bar.h"
#include "drivers/MCP2515.h"
#include "drivers/MMA7361.h"
#include "OBD/OBD.h"
#include "logger.h"

/**
 * Interrupts every 1ms
 */
static void interrupt_1kHz(void)
{

    process_OBD = true;

    counter_100Hz++;

    if(counter_100Hz >= 10) {
        counter_100Hz = 0;

        /* 100Hz Block */
        /* Let the SD card do its 100Hz thing */
        sd_100hz();

        /* 10Hz block */
        counter_10Hz++;

        if(counter_10Hz >= 10) {
            counter_10Hz = 0;

            /* Write a new line to the log? */
            loggingCounter++;

            if(loggingCounter >= globalLogger.frequency) {
                loggingCounter = 0;
                process_log = true;
            }

            /* Update the LED bar? */
            ledUpdateCounter++;

            if(ledUpdateCounter >= LED_UPDATE_FREQ) {
                ledUpdateCounter = 0;
                process_led_bar = true;
            }

            /* Update the display? */
            displayUpdateCounter++;

            if(displayUpdateCounter >= DISPLAY_UPDATE_FREQ) {
                displayUpdateCounter = 0;
                process_display = true;
            }

            /* Reread the accelerometer levels? */
            accelUpdateCounter++;

            if(accelUpdateCounter >= ACCEL_UPDATE_FREQ) {
                accelUpdateCounter = 0;
                if (accelChans.activeAxis == accel_axis_type_none) {
                    accelChans.activeAxis = accel_axis_type_x;
                    accel_start_read(&accelChans);
                }
            }

            obdUpdateCounter++;

            /* If the multiple PID request is idle, request
            	the PIDs again. Needs to be after the logger write and
            	display refresh since they need actual data and not
            	pending requests */
            if((obdUpdateCounter >= OBD_REQUEST_SET_FREQ) &&
                    (pids_being_sent > 0) &&
                    (multiple_pids_remaining == 0)) {

                obd_send_multiple_requests(&pidsToQuery);
            }
        }

    }
}

/**
 * Starts a timer that interrupts with a 100Hz frequency
 */
void init_1kHz_interrupt(void)
{
    /* Put a 1kHz clock on Timer 0 */
    tc_enable(&TCC0);
    tc_set_overflow_interrupt_callback(&TCC0, interrupt_1kHz);
    tc_set_wgm(&TCC0, TC_WG_NORMAL);
    /* Got value from scope since the calc value, 31, is wrong */
    tc_write_period(&TCC0, 24);
    tc_set_overflow_interrupt_level(&TCC0, TC_INT_LVL_LO);
    tc_write_clock_source(&TCC0, TC_CLKSEL_DIV1024_gc);

    counter_100Hz = 0;
    counter_10Hz = 0;

    loggingCounter = 0;
    ledUpdateCounter = 0;
    displayUpdateCounter = 0;
    accelUpdateCounter = 0;
    /* Send an update request ASAP. Keep it out of phase with the other
    updates in order to provide fresh data. */
    obdUpdateCounter = OBD_REQUEST_SET_FREQ - 1;

    process_display = false;
    process_led_bar = false;
    process_log = false;
    process_OBD = false;
}