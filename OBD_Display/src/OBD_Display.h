/*
 * OBD_Display.h
 *
 * Created: 7/20/2012 2:12:27 PM
 *  Author: Meyer
 */


#ifndef OBD_DISPLAY_H_
#define OBD_DISPLAY_H_

#include <stdint.h>

#include "drivers/MMA7361.h"
#include "drivers/GDM/GDM12864H_Draw.h"

#if 0 /* Only used for testing LED bar */
uint8_t axis_used;
#endif

gdm_bar_graph *bar_graph_a;
gdm_bar_graph *bar_graph_b;
gdm_bar_graph *bar_graph_c;
gdm_bar_graph *bar_graph_d;

#endif /* OBD_DISPLAY_H_ */