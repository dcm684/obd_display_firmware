/*
 * Display.h
 *
 * Handles the drawing on the display
 *
 * Created: 4/5/2013 11:51:07 AM
 *  Author: cmeyer
 */


#ifndef DISPLAY_H_
#define DISPLAY_H_

#include "drivers/GDM/GDM12864H_Draw.h"
#include "drivers/GDM/GDM12864H_Text.h"

/**
 * Various types of things that can be displayed on the GDM
 */
typedef enum {
    display_type_text		= 1,    /*< Text */
    display_type_line		= 2,    /*< Line */
    display_type_rectangle	= 3,    /*< Rectangle */
    display_type_arc		= 4,    /*< Arc */
    display_type_circle		= 5,    /*< Circle */
    display_type_gauge		= 6,    /*< Gauge */
    display_type_bar_graph	= 7,    /*< Bar graph */
    display_type_none       = 0xFF, /*< No type */
} display_types;

/**
 * A generic container for all things that can be displayed on the GDM
 */
typedef struct {
    display_types type; /*< Type of graphical struct implemented in this struct */
    union {
        gdm_text		text;       /*< Struct with static text */
        gdm_line		line;       /*< Struct describing a line */
        gdm_rectangle	rectangle;  /*< Struct describing a rectangle */
        gdm_arc			arc;        /*< Struct describing an arc */
        gdm_circle		circle;     /*< Struct describing a circle */
        gdm_analog_dial	dial;       /*< Struct describing a gauge */
        gdm_bar_graph	graph;      /*< Struct describing a bar graph */
    };
} display_struct;

void display_draw_object(display_struct *toDraw);
void display_refresh(void);
#endif /* DISPLAY_H_ */