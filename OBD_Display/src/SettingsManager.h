#include "drivers\SDFAT\ff.h"
#include "Display.h"
#include <stdbool.h>
/*
 * SettingsManager.h
 *
 * Created: 4/12/2014 11:36:36 PM
 *  Author: Meyer
 */


#ifndef SETTINGSMANAGER_H_
#define SETTINGSMANAGER_H_
/**
 * /def SETTING_KEY_MAIN
 * Tag indicating that file is is a settings file
 */
#define SETTING_KEY_MAIN	"OBD_Settings"

/**
 * /def SETTING_VERSION
 * Version for settings file format
 */
#define SETTING_VERSION 1


/**
 * /def SETTING_KEY_LOGGER
 * XML tag indicating enclosed area is a logger
 */
#define SETTING_KEY_LOGGER	"logger"

/**
 * /def SETTING_KEY_LED_BAR
 * Tag indicating that following area is what is to be represented by the LED bar
 */
#define SETTING_KEY_LED_BAR "led_bar"

/**
 * /def SETTING_KEY_DISPLAY
 * Tag indicating following area is what is to be drawn
 */
#define SETTING_KEY_DISPLAY	"display"

/**
 * /def SETTING_BUFFER_SIZE
 * Bytes in the read buffer used to read in a settings file
 *
 * Shall be at least as long as the longest section key + 1
 */
#define SETTING_BUFFER_SIZE 15

/**
 * /def SETTING_MAX_STRING_LENGTH
 * Maximum number of chars a string to be displayed can have
 *
 * Remember to include the null terminator in this value
 */
#define SETTING_MAX_STRING_LENGTH 30

/**
 * /def SETTINGS_DISPLAY_ITEMS
 * Maximum number of display items that can be loaded from the settings file
 */
#define SETTINGS_DISPLAY_ITEMS 10

typedef enum {
    settings_load_ok			= 0, /*< The settings were loaded successfully */
    settings_load_corrupt		= 1, /*< The settings file was corrupt */
    settings_load_file_error	= 2, /*< There was an error loading the settings file */
    settings_load_logger_error	= 3, /*< There was an error initializing the logger */
    settings_load_eof			= 4, /*< End of settings file was reached */
} settings_load_result;

/**
 * @enum settings_allow_hex
 * Should hexadecimal values be allowed when parsing a number?
 */
typedef enum {
    settings_hex_yes, /*< Allow hexadecimal values when parsing ints */
    settings_hex_no   /*< Do not allow hexadecimal values when parsing ints */
} settings_allow_hex;

/**
 * @enum settings_allow_negative
 * Should negative values be allowed when parsing a number?
 */
typedef enum {
    settings_negative_yes, /*< Allow negative values when parsing floats and ints */
    settings_negative_no   /*< Do not allow negative values when parsing floats and ints */
} settings_allow_negative;

uint8_t settingsDisplayCount; /*< Number of items in settingsDisplayStructs */
display_struct settingsDisplayStructs[SETTINGS_DISPLAY_ITEMS]; /*< Display items read from settings file */

bool settings_look_for_string(FIL *inFile, char *goalString, settings_load_result *outResult);
settings_load_result setting_load_new_line(FIL *inFile);
settings_load_result settings_load_version(FIL *inFile, int *outVersion);
settings_load_result settings_load_logger(FIL *inFile, char *currGoal);
settings_load_result settings_load_led_bar( FIL *inFile, char *currGoal );
settings_load_result settings_load_display( FIL *inFile, char *currGoal );
settings_load_result settings_load_file(char *fileName);
FRESULT settings_read_int(FIL *inFile, char *buffer, settings_allow_hex allowHex, settings_allow_negative allowNegative, int *outValue);
FRESULT settings_read_float(FIL *inFile, char *buffer, settings_allow_negative allowNegative, float *outValue);
FRESULT settings_read_string(FIL *inFile, char *inBuffer, char *outString, uint8_t maxStringLength);
settings_load_result settings_load_text(display_struct *outStruct, FIL *inFile, char *currGoal );
settings_load_result settings_load_line(display_struct *outStruct, FIL *inFile, char *currGoal );
settings_load_result settings_load_rect(display_struct *outStruct, FIL *inFile, char *currGoal );
settings_load_result settings_load_arc( display_struct *outStruct, FIL *inFile, char *currGoal );
settings_load_result settings_load_circle(display_struct *outStruct, FIL *inFile, char *currGoal );
settings_load_result settings_load_gauge(display_struct *outStruct, FIL *inFile, char *currGoal );
settings_load_result settings_load_bar_graph(display_struct *outStruct, FIL *inFile, char *currGoal );
#endif /* SETTINGSMANAGER_H_ */