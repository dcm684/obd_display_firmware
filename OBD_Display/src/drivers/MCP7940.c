/*
 * MCP7940.c
 *
 * Created: 7/23/2012 2:19:22 PM
 *  Author: Meyer
 */

#include "MCP7940.h"

#include <conf_twim.h>
#include "Pin_Defines.h"

/**
 * Initializes the I2C interface
 */
void twi_init(void)
{

    rtc_interface = &RTC_TWI_MODULE;

    twi_master_options_t opt = {
        .speed = TWI_SPEED,
        .chip = TWI_MASTER_ADDRESS
    };

    twi_master_setup(rtc_interface, &opt);
}

/**
 * Send a a packet of data to a given address on RTC
 *
 * /param address Address to set on RTC
 * /param outData Data to send out
 * /param dataLength Number of items in that packet
 *
 */
void rtc_write(uint8_t address, uint8_t *outData, uint8_t dataLength)
{
    twi_package_t outPackage = {
        .addr = {address},
        .addr_length = 1,
        .chip = RTC_ADDRESS_PREFIX,
        .buffer = outData,
        .length = dataLength
    };

    while(twi_master_write(rtc_interface, &outPackage) != TWI_SUCCESS);
}

/**
 * Read data from an address on the RTC
 *
 * \param address Address to read data from
 * \param readData Data read from given address
 * \param readLength Number of bytes of data to read
 */
void rtc_read(uint8_t address, uint8_t *readData, uint8_t readLength)
{
    twi_package_t inPackage = {
        .addr = {address},
        .addr_length = 1,
        .chip = RTC_ADDRESS_PREFIX,
        .buffer = readData,
        .length = readLength
    };

    if(twi_master_read(rtc_interface, &inPackage) == TWI_SUCCESS) {
        return;
    }

    return;
}

/**
 * Sets the time and date, starts the timer and enables the backup battery
 *
 * Code is written with the assumption that the order of time bytes on the
 * RTC is seconds, minutes, hours. Those 3 bytes are read in one burst.
 *
 * /param inTime Time to set the clock to
 * /param inDate Date to set
 */
void rtc_init(rtc_time *inTime, rtc_date *inDate)
{
    uint8_t dataByte;

    rtc_set_time(inTime);
    rtc_set_date(inDate);

    /* Turn the oscillator on */
    rtc_read(RTC_ADDR_START, &dataByte, 1);
    dataByte |= _BV(RTC_BIT_START);
    rtc_write(RTC_ADDR_START, &dataByte, 1);

    /* Turn battery backup on */
    rtc_read(RTC_ADDR_BAT_OSCON, &dataByte, 1);
    dataByte |= _BV(RTC_BIT_BAT_EN);
    rtc_write(RTC_ADDR_BAT_OSCON, &dataByte, 1);
}

/**
 * Sets the clock to the given time
 * Code is written with the assumption that the order of time bytes on the
 * RTC is day, date, month, year. Those 4 bytes are read in one burst.
 *
 * /param inTime Time to set the clock to
 */
void rtc_set_time(rtc_time *inTime)
{
    uint8_t readData[3];

    rtc_read(RTC_CLK_SECOND, &readData[0], 3);

    /* Don't update the time when its the 59th second */
    while((readData[0] ^ 0b11101001) == 0) {
        rtc_read(RTC_CLK_SECOND, &readData[0], 3);
    }

    readData[0] &= ~(RTC_MASK_SECOND_ONE);
    readData[0] &= ~(RTC_MASK_SECOND_TEN);
    readData[0] |= ((inTime->second) % 10) << RTC_SHIFT_SECOND_ONE;
    readData[0] |= ((inTime->second) / 10) << RTC_SHIFT_SECOND_TEN;

    readData[1] &= ~(RTC_MASK_MINUTE_ONE);
    readData[1] &= ~(RTC_MASK_MINUTE_TEN);
    readData[1] |= ((inTime->minute) % 10) << RTC_SHIFT_MINUTE_ONE;
    readData[1] |= ((inTime->minute) / 10) << RTC_SHIFT_MINUTE_TEN;

    readData[2] &= ~(RTC_MASK_HOUR_ONE);
    readData[2] &= ~(RTC_MASK_HOUR_TEN_24);

    if((inTime->am_pm) == RTC_NO_AMPM) {
        readData[2] &= ~(_BV(RTC_CLK_12_HR_BIT));

    } else {
        if(inTime->am_pm == RTC_PM) {
            readData[2] |= _BV(RTC_CLK_AM_PM_BIT);
        }

        readData[2] |= _BV(RTC_CLK_12_HR_BIT);
    }

    readData[2] |= ((inTime->hour) % 10) << RTC_SHIFT_HOUR_ONE;
    readData[2] |= ((inTime->hour) / 10) << RTC_SHIFT_HOUR_TEN;

    rtc_write(RTC_CLK_SECOND, &readData[0], 3);
}

/**
 * Sets the day and date
 *
 * If the year given is less than 100, it is assumed to be a date in
 * the 21st century. Otherwise the valid date range is from 1980
 * through 2079. Years outside of this range may cause problems.
 *
 * /param inDate Date and day to set
 */
void rtc_set_date(rtc_date *inDate)
{
    uint8_t readData[4];

    rtc_read(RTC_CLK_SECOND, &readData[0], 1);

    /* Don't update the date when its the 59th second */
    while((readData[0] ^ 0b11101001) == 0) {
        rtc_read(RTC_CLK_SECOND, &readData[0], 1);
    }

    rtc_read(RTC_CLK_DAY, &readData[0], 4);
    readData[0] &= ~(RTC_MASK_DAY);
    readData[0] |= inDate->day;

    readData[1] &= ~(RTC_MASK_DATE_ONE);
    readData[1] &= ~(RTC_MASK_DATE_TEN);
    readData[1] |= ((inDate->date) % 10) << RTC_SHIFT_DATE_ONE;
    readData[1] |= ((inDate->date) / 10) << RTC_SHIFT_DATE_TEN;

    readData[2] &= ~(RTC_MASK_MONTH_ONE);
    readData[2] &= ~(RTC_MASK_MONTH_TEN);
    readData[2] |= ((inDate->month) % 10) << RTC_SHIFT_MONTH_ONE;
    readData[2] |= ((inDate->month) / 10) << RTC_SHIFT_MONTH_TEN;
    /*readData |= _BV((inDate->leap_year));*/ //Leap year is RO

    /* For those who decided it was a good idea to ignore the Y2K problem
    this assumes all two digit years are in the 21st century */
    if(inDate->year < 100) {
        inDate->year = inDate->year + 2000;
    }

    readData[3] &= ~(RTC_MASK_YEAR_ONE);
    readData[3] &= ~(RTC_MASK_YEAR_TEN);
    readData[3] |= ((inDate->year - RTC_YEAR_ZERO) % 10) << RTC_SHIFT_YEAR_ONE;
    readData[3] |= ((inDate->year - RTC_YEAR_ZERO) / 10) << RTC_SHIFT_YEAR_TEN;

    rtc_write(RTC_CLK_DAY, &readData[0], 4);
}

/**
 * Returns the current time
 *
 * /return outTime Current time
 */
void rtc_get_time(rtc_time *outTime)
{
    uint8_t readData[3];
    rtc_read(RTC_CLK_SECOND, &readData[0], 3);

    outTime->second = (readData[0] & RTC_MASK_SECOND_ONE) >> RTC_SHIFT_SECOND_ONE;
    outTime->second += ((readData[0] & RTC_MASK_SECOND_TEN) >> RTC_SHIFT_SECOND_TEN) * 10;

    outTime->minute = (readData[1] & RTC_MASK_MINUTE_ONE) >> RTC_SHIFT_MINUTE_ONE;
    outTime->minute += ((readData[1] & RTC_MASK_MINUTE_TEN) >> RTC_SHIFT_MINUTE_TEN) * 10;

    outTime->hour = (readData[2] & RTC_MASK_HOUR_ONE) >> RTC_SHIFT_HOUR_ONE;

    if((readData[2] & _BV(RTC_CLK_12_HR_BIT)) > 0) {
        outTime->hour += ((readData[2] & RTC_MASK_HOUR_TEN_12) >> RTC_SHIFT_HOUR_TEN) * 10;
        outTime->am_pm = ((readData[2] >> RTC_CLK_AM_PM_BIT) & 1) ? RTC_PM : RTC_AM;

    } else {
        outTime->hour += ((readData[2] & RTC_MASK_HOUR_TEN_24) >> RTC_SHIFT_HOUR_TEN) * 10;
        outTime->am_pm = RTC_NO_AMPM;
    }
}

/**
 * Returns the current date
 *
 * /return outDate Current date
 */
void rtc_get_date(rtc_date *outDate)
{
    uint8_t readData[4];

    rtc_read(RTC_CLK_DAY, &readData[0], 4);

    outDate->day = (readData[0] & RTC_MASK_DAY) >> RTC_SHIFT_DAY;

    outDate->date = (readData[1] & RTC_MASK_DATE_ONE) >> RTC_SHIFT_DATE_ONE;
    outDate->date += ((readData[1] & RTC_MASK_DATE_TEN) >> RTC_SHIFT_DATE_TEN) * 10;

    outDate->month = (readData[2] & RTC_MASK_MONTH_ONE) >> RTC_SHIFT_MONTH_ONE;
    outDate->month += ((readData[2] & RTC_MASK_MONTH_TEN) >> RTC_SHIFT_MONTH_TEN) * 10;
    outDate->leap_year = ((readData[2] & _BV(RTC_BIT_LEAP_YEAR)) > 0);

    outDate->year = (readData[3] & RTC_MASK_YEAR_ONE) >> RTC_SHIFT_YEAR_ONE;
    outDate->year += ((readData[3] & RTC_MASK_YEAR_TEN) >> RTC_SHIFT_YEAR_TEN) * 10;
    outDate->year += RTC_YEAR_ZERO;
}

/**
 * Returns if the system lost power and switched to battery power
 *
 * /returns Whether the system lost power
 */
bool rtc_battery_powered(void)
{
    uint8_t readData;
    rtc_read(RTC_ADDR_BAT_OSCON, &readData, 1);
    return bit_is_set(readData, RTC_BIT_VBAT);
}

/**
 * Clears the battery switchover timestamp registers and the
 * bit indicating that the system was battery powered.
 */
void rtc_clear_battery_timestamps(void)
{
    uint8_t readData;
    rtc_read(RTC_ADDR_BAT_OSCON, &readData, 1);
    readData = readData & (!_BV(RTC_BIT_VBAT));
    rtc_write(RTC_ADDR_BAT_OSCON, &readData, 1);
}