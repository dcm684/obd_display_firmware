/*
 * SDHandler.h
 *
 * Created: 9/11/2012 2:05:31 PM
 *  Author: Meyer
 */


#ifndef SDHANDLER_H_
#define SDHANDLER_H_

#include <inttypes.h>
#include <stdio.h>
#include <stdbool.h>

#include "ff.h"

bool sdInitialized;
FATFS sdFAT[1];	/* FAT for the SD card */

uint32_t get_fattime(void);
void put_rc( FILE *inFile, FRESULT rc );
void sd_100hz(void);
FRESULT sd_dump_file_to_display(FILE *inDisplay, char *fileToDump);
FRESULT sd_append_file(char *inPath, char *inString);
FRESULT sd_init_and_mount(void);
bool sd_check_and_init(void);
#endif /* SDHANDLER_H_ */