/*
 * SDHandler.c
 *
 * Created: 9/11/2012 1:57:46 PM
 *  Author: Meyer
 */
#include <stdio.h>
#include <string.h>

//#include "mmc.c"

#include "SDHandler.h"
#include "../MCP7940.h"
#include "diskio.h"
#include "../USART_Handler.h"
#include "Pin_Defines.h"

extern void disk_timerproc(void);

volatile uint8_t sd_timer;

/**
 * Run on the 100Hz clock required by the FATfs and SD functions
 */
void sd_100hz( void )
{
    sd_timer++;
    disk_timerproc();
}

/**
 * Get the current time time in FAT file time
 *
 * /return The current time and date converted into FAT file time
 */
uint32_t get_fattime(void)
{

    uint32_t retVal;
    uint8_t hour;

    rtc_time currTime;
    rtc_date currDate;

    rtc_get_time(&currTime);
    rtc_get_date(&currDate);

    if(currTime.am_pm == RTC_PM) {
        hour = currTime.hour + 12;
    } else if((currTime.am_pm == RTC_AM) &&
              (currTime.hour == 12)) {

        hour = 0;
    } else {
        hour  = currTime.hour;
    }

    /* FAT file times are rounded down to the nearest 2 second */
    retVal =	((uint32_t) (currDate.year - 1980)	<< 25) |
                ((uint32_t) currDate.month			<< 21) |
                ((uint32_t) currDate.date			<< 16) |
                ((uint32_t) hour					<< 11) |
                ((uint32_t) currTime.minute			<< 5)  |
                ((uint32_t) currTime.second			>> 1);

    return retVal;
}

/**
 * Prints the error code from the SD action
 *
 * /param inFile Where to display the error text
 * /param rc File operation result
 */
void put_rc( FILE *inFile, FRESULT rc )
{
    const char *p;
    static const char str[] PROGMEM =
        "OK\0" "DISK_ERR\0" "INT_ERR\0" "NOT_READY\0" "NO_FILE\0" "NO_PATH\0"
        "INVALID_NAME\0" "DENIED\0" "EXIST\0" "INVALID_OBJECT\0" "WRITE_PROTECTED\0"
        "INVALID_DRIVE\0" "NOT_ENABLED\0" "NO_FILE_SYSTEM\0" "MKFS_ABORTED\0" "TIMEOUT\0"
        "LOCKED\0" "NOT_ENOUGH_CORE\0" "TOO_MANY_OPEN_FILES\0";
    FRESULT i;

    for (p = str, i = 0; i != rc && pgm_read_byte_near(p); i++) {
        while(pgm_read_byte_near(p++));
    }

    fprintf_P(inFile, PSTR("FR_%S\r\n"), (wchar_t*) p);
}

/**
 * Appends the string to the file at the given path
 *
 * /param inString	String to append to file
 * /param inPath	Path to file
 */
FRESULT sd_append_file( char *inPath, char *inString )
{
    FRESULT result;
    FIL theFile;

    UINT byteCount;

    /* Open the file so it can be appended. If it does not exist already,
    create a new file */
    result = f_open(&theFile,
                    (TCHAR *) inPath,
                    FA_WRITE | FA_OPEN_ALWAYS);
    if(result != FR_OK) {
        fputs_P(PSTR("Open for Append Failed: "), &usart_serial_stream);
        return result;
    }

    /* Now move the pointer to the write location to the end of the file */
    if(result == FR_OK) {
        result = f_lseek(&theFile, f_size(&theFile));
        if(result != FR_OK) {
            fputs_P(PSTR("Moving the cursor failed: "), &usart_serial_stream);
        }
    }

    /* Write the data to the file */
    if(result == FR_OK) {
        result = f_write(&theFile, inString, strlen(inString), &byteCount);
        if(result != FR_OK) {
            fputs_P(PSTR("Writing a String Failed: "), &usart_serial_stream);
        } else if (byteCount != strlen(inString)) {
            fprintf_P(&usart_serial_stream,
                      PSTR("Number of bytes written don't match, %u != %u\r\n"),
                      byteCount,
                      strlen(inString));
        }
    }

    /* You are done appending to the file. It is time to test closing the
    file */
    result = f_close(&theFile);
    if(result != FR_OK) {
        fputs_P(PSTR("Post Append Close Failed: "), &usart_serial_stream);
    }

    return result;
}

/**
 * Opens a file for display on the given display.
 *
 * The drive must be mounted before this function is called.
 *
 * /param inDisplay Where to display the contents
 * /param fileToDump Full path to the file to be deleted
 *
 * /param The result of the file operation
 */
FRESULT sd_dump_file_to_display( FILE *inDisplay, char *fileToDump )
{

    FRESULT result;
    FIL theFile;

    uint16_t charsRemaining;
    uint16_t singleReadSize;
    uint16_t bytesRead = 0;
    char buffer[20];

    /* Open the given file  */
    result = f_open(&theFile, fileToDump, FA_READ);
    if(result == FR_OK) {
        /* Load the file length */
        charsRemaining = f_size(&theFile);

        while(charsRemaining) {
            /* If the number of bytes that are to be read is more
            than the buffer can hold, set then number of bytes to
            read to one less than the buffer size. The final byte
            will need to be null terminated */
            if(charsRemaining >= sizeof buffer - 1) {
                singleReadSize = sizeof buffer - 1;
                charsRemaining -= singleReadSize;
            } else {
                singleReadSize = charsRemaining;
                charsRemaining = 0;
            }
            result = f_read(&theFile, buffer, (UINT) singleReadSize, &bytesRead);
            if(result != FR_OK) {
                break;
            } else {
                /* The buffer is not null terminated */
                buffer[bytesRead] = '\0';
                fputs(buffer, inDisplay);
            }

            /* End of file reached */
            if(singleReadSize != bytesRead) {
                break;
            }
        }
        fputs_P(PSTR("\r\n"), inDisplay);

        result = f_close(&theFile);
    }
    return result;
}

/**
 * Initializes and mounts the SD card
 */
FRESULT sd_init_and_mount( void )
{
    //FATFS myFAT[1];
    FRESULT result;

    disk_initialize(0);

    /* Disk must be mounted */
    result = f_mount(0, &sdFAT[0]);
    if(result != FR_OK) {
        fputs_P(PSTR("Mount Failed: "), &usart_serial_stream);
    }

    return result;
}

/**
 *	Checks if an SD card is present and initializes it if it isn't already
 *
 * /returns Was the card just mounted?
 */
bool sd_check_and_init( void )
{
    bool retVal;
    FRESULT result;
    retVal = false;

    /* Is there a card inserted ? */
    if(!(SD_DETECT_PORT.IN & SD_DETECT)) {
        /* Has it already been initialized? */
        if(!sdInitialized) {
            disk_initialize(0);
            sdInitialized = true;
        }
    } else {
        sdInitialized = false;
    }

    /* Disk must be mounted */
    result = f_mount(0, &sdFAT[0]);
    if(result != FR_OK) {
        fputs_P(PSTR("Mount Failed: "), &usart_serial_stream);
        put_rc(&usart_serial_stream, result);
        return false;
    }

    return retVal;
}