/*
 * SPILocal.c
 *
 * Created: 7/20/2012 11:01:11 PM
 *  Author: Meyer
 */

#include <sysclk.h>

#include "SPILocal.h"
#include "Pin_Defines.h"

/**
 * Initializes the given spi device as an SPI master device
 *
 * The direction of the given pins is set properly and the levels of
 * output pins are initialized. The SPI registers are also set for
 * Mode 0 master with the MSb first and a frequency of
 * peripheral clock  / 4;
 *
 * /param inDevice SPI device to be initialized
 */
void init_master_spi(spi_entire_device *inDevice)
{

    ioport_configure_pin(inDevice->mosi, IOPORT_DIR_OUTPUT | IOPORT_INIT_LOW);
    ioport_configure_pin(inDevice->miso, IOPORT_DIR_INPUT);
    ioport_configure_pin(inDevice->clock, IOPORT_DIR_OUTPUT | IOPORT_INIT_LOW);
    ioport_configure_pin(inDevice->cs, IOPORT_DIR_OUTPUT | IOPORT_INIT_HIGH);

    sysclk_enable_module(SYSCLK_PORT_C, PR_SPI_bm);

    inDevice->spi->INTCTRL = SPI_INTLVL_OFF_gc;
    /**
     * Bit 7	- Don't double clock
     * Bit 6	- Enable
     * Bit 5	- Most significant bit first
     * Bit 4	- Master select
     * Bit 3:2	- Mode 00
     * Bit 1:0	- Peripheral Clock / 4 (when Bit 7 is 0)
     */
    inDevice->spi->CTRL = 0b01010000;
}

/**
 * Put one byte on the given SPI device and wait for it to be sent before
 * returning
 *
 * /param inDevice SPI device to send the byte over
 * /param outByte Data to send
 */
void spi_naked_send_byte(spi_entire_device *inDevice, uint8_t outByte)
{
    inDevice->spi->DATA = outByte;

    while(!(inDevice->spi->STATUS & SPI_IF_bm));
}

/**
 * Wait for and get one byte on the given SPI device
 *
 * Does not clock data through
 *
 * /param inDevice SPI device to get byte from
 *
 * /returns Byte received
 */
uint8_t spi_naked_read_byte(spi_entire_device *inDevice)
{
    while(!(inDevice->spi->STATUS & SPI_IF_bm));

    return inDevice->spi->DATA;
}

/**
 * Wait for and get a packet on the given SPI device
 *
 * Clocks a zero through for each byte to be read.
 *
 * /param inDevice SPI device to get packet from
 * /param data Where to store received packet
 * /param dataLength Number bytes in the packet
 */
void spi_simple_read_packet(spi_entire_device *inDevice, uint8_t *data, size_t dataLength)
{
    size_t i;

    for(i = 0; i < dataLength; i++) {
        spi_naked_send_byte(inDevice, 0);
        *(data + i) = spi_naked_read_byte(inDevice);
    }
}

/**
 * Put a packet of data on the given SPI device and wait for it to be sent
 * before returning
 *
 * /param inDevice SPI device to send the packet over
 * /param outByte Data to send
 * /param dataLength How many bytes of data to be sent
 */
void spi_simple_send_packet(spi_entire_device *inDevice, uint8_t *data, size_t dataLength)
{
    size_t i;

    for(i = 0; i < dataLength; i++) {
        spi_naked_send_byte(inDevice, *(data + i));
    }
}