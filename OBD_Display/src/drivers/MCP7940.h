/*
 * MCP7940.h
 *
 * Created: 7/23/2012 2:19:32 PM
 *  Author: Meyer
 */


#ifndef MCP7940_H_
#define MCP7940_H_

#include <twi_master.h>

/* /def TWI_SPEED Data rate when communicating on TWI */
#define TWI_SPEED 400000
/* /def TWI_ADDRESS Address of the MCU on the TWI */
#define TWI_MASTER_ADDRESS 0x84

/* /def RTC_ADDRESS_PREFIX Used to reference the RTC on the TWI.
	0xDE shifted right 1 bit because the framework will shift it back */
#define RTC_ADDRESS_PREFIX 0b01101111
/* /def RTC_WRITE OR'd with the address to indicating a write operation to RTC memory */
//#define RTC_WRITE 0b00000000
/* /def RTC_READ OR'd with the address to indicating a read operation to RTC memory */
//#define RTC_READ  0b00000001

/* /def RTC_ADDR_START Address where the start clock bit is */
#define RTC_ADDR_START 0x00
/* /def RTC_BIT_START Start clock bit number */
#define RTC_BIT_START 7
/* /def RTC_CLK_SECOND Address of clock seconds */
#define RTC_CLK_SECOND 0x00

/* /def RTC_CLK_MINUTE Address of clock minutes */
#define RTC_CLK_MINUTE 0x01
/* /def RTC_CLK_HOUR Address of clock hours */
#define RTC_CLK_HOUR 0x02
/* /def RTC_CLK_DAY Address of day of the week */
#define RTC_CLK_DAY 0x03
/* /def RTC_CLK_DATE Address of the date */
#define RTC_CLK_DATE 0x04
/* /def RTC_CLK_MONTH Address of the month */
#define RTC_CLK_MONTH 0x05
/* /def RTC_CLK_YEAR Address of the year */
#define RTC_CLK_YEAR 0x06

/* /def RTC_MASK_SECOND_ONE Mask to get ones digit of BCD seconds */
#define RTC_MASK_SECOND_ONE 0x0F
/* /def RTC_SHIFT_DAY_ONE Amount to shift masked seconds ones digit */
#define RTC_SHIFT_SECOND_ONE 0
/* /def RTC_MASK_SECOND_ONE Mask to get ones digit of BCD seconds */
#define RTC_MASK_SECOND_TEN 0x70
/* /def RTC_SHIFT_SECOND_TEN Amount to shift masked seconds tens digit */
#define RTC_SHIFT_SECOND_TEN 4

/* /def RTC_MASK_MINUTE_ONE Mask to get ones digit of BCD minutes */
#define RTC_MASK_MINUTE_ONE 0x0F
/* /def RTC_SHIFT_MINUTE_ONE Amount to shift masked minutes ones digit */
#define RTC_SHIFT_MINUTE_ONE 0
/* /def RTC_MASK_MINUTE_ONE Mask to get ones digit of BCD minutes */
#define RTC_MASK_MINUTE_TEN 0x70
/* /def RTC_SHIFT_MINUTE_TEN Amount to shift masked minutes tens digit */
#define RTC_SHIFT_MINUTE_TEN 4

/* /def RTC_MASK_HOUR_ONE Mask to get ones digit of BCD hours */
#define RTC_MASK_HOUR_ONE 0x0F
/* /def RTC_SHIFT_HOUR_ONE Amount to shift masked hours ones digit */
#define RTC_SHIFT_HOUR_ONE 0
/* /def RTC_MASK_HOUR_TEN_24 Mask to get tens digit of BCD hours when using 24 hour clock*/
#define RTC_MASK_HOUR_TEN_24 0x30
/* /def RTC_MASK_HOUR_TEN_12 Mask to get tens digit of BCD hours when using 12 hour clock*/
#define RTC_MASK_HOUR_TEN_12 0x10
/* /def RTC_SHIFT_HOUR_TEN Amount to shift masked hours tens digit */
#define RTC_SHIFT_HOUR_TEN 4
/* /def RTC_CLK_AM_PM_BIT When 12hr clock is used, this is to indicate AM/PM */
#define RTC_CLK_AM_PM_BIT 5
/* /def RTC_CLK_12_HR_BIT Set to use a 12hr clock */
#define RTC_CLK_12_HR_BIT 6

/* /def RTC_ADDR_BAT Address of battery settings and oscillator on bit */
#define RTC_ADDR_BAT_OSCON 0x03
/* /def RTC_BIT_OSCON Indicates that the oscillator is running when clear */
#define RTC_BIT_OSCON 5
/* /def RTC_BIT_VBAT Bit indicating the battery backup is in use */
#define RTC_BIT_VBAT 4
/* /def RTC_BIT_BAT_EN Set to enable battery backup */
#define RTC_BIT_BAT_EN 3

/* /def RTC_MASK_DAY Mask for the day of the week */
#define RTC_MASK_DAY 0x07
/* /def RTC_SHIFT_DAY Bits to shift to get the day of the week */
#define RTC_SHIFT_DAY 0

/* /def RTC_MASK_DATE_ONE Mask to get ones digit of BCD date */
#define RTC_MASK_DATE_ONE 0x0F
/* /def RTC_SHIFT_DATE_ONE Amount to shift masked date ones digit */
#define RTC_SHIFT_DATE_ONE 0
/* /def RTC_MASK_DATE_ONE Mask to get ones digit of BCD date */
#define RTC_MASK_DATE_TEN 0x30
/* /def RTC_SHIFT_DATE_TEN Amount to shift masked date tens digit */
#define RTC_SHIFT_DATE_TEN 4

/* /def RTC_MASK_MONTH_ONE Mask to get ones digit of BCD month */
#define RTC_MASK_MONTH_ONE 0x0F
/* /def RTC_SHIFT_MONTH_ONE Amount to shift masked month ones digit */
#define RTC_SHIFT_MONTH_ONE 0
/* /def RTC_MASK_MONTH_ONE Mask to get ones digit of BCD month */
#define RTC_MASK_MONTH_TEN 0x01
/* /def RTC_SHIFT_MONTH_TEN Amount to shift masked month tens digit */
#define RTC_SHIFT_MONTH_TEN 4
/* /def RTC_BIT_LEAP_YEAR Bit set to indicate that the year is a leap year */
#define RTC_BIT_LEAP_YEAR 5

/* /def RTC_MASK_YEAR_ONE Mask to get ones digit of BCD year */
#define RTC_MASK_YEAR_ONE 0x0F
/* /def RTC_SHIFT_YEAR_ONE Amount to shift masked year ones digit */
#define RTC_SHIFT_YEAR_ONE 0
/* /def RTC_MASK_YEAR_ONE Mask to get ones digit of BCD year */
#define RTC_MASK_YEAR_TEN 0xF0
/* /def RTC_SHIFT_YEAR_TEN Amount to shift masked year tens digit */
#define RTC_SHIFT_YEAR_TEN 4

/* /def RTC_ADDR_CTL Address of the control register */
#define RTC_ADDR_CTL 0x07
/* /def RTC_CTL_OUT Bit to set MFP output level when not outputting square wave */
#define RTC_CTL_OUT 7
/* /def RTC_CTL_SQWE Bit set to enable divided output from the crystal */
#define RTC_CTL_SQWE 6
/* /def RTC_CTL_ALRM_1 Bit set to enable alarm 1 */
#define RTC_CTL_ALRM_1 5
/* /def RTC_CTL_ALRM_1 Bit set to enable alarm 0 */
#define RTC_CTL_ALRM_0 4
/* /def RTC_CTL_EXTOSC Use an external 32.768kHz signal drive the RTCC instead of a crystal */
#define RTC_CTL_EXTOSC 3
/* /def RTC_CTL_CALIB Bit set to enable the Cal output function */
#define RTC_CTL_CALIB 2
/* /def RTC_CTL_32768 Bit 1:0 value to output a 32.768kHz signal on MFP */
#define RTC_CTL_32768 11
/* /def RTC_CTL_8192 Bit 1:0 value to output a 8.192kHz signal on MFP */
#define RTC_CTL_8192 10
/* /def RTC_CTL_4096 Bit 1:0 value to output a 4.096kHz signal on MFP */
#define RTC_CTL_4096 01
/* /def RTC_CTL_0010 Bit 1:0 value to output a 1 Hz signal on MFP */
#define RTC_CTL_0010 00

/* /def RTC_ADDR_CAL Address of the calibration register */
#define RTC_ADDR_CAL 0x08

/* /def RTC_ALRM0_SECOND Address of alarm 0 seconds */
#define RTC_ALRM0_SECOND 0x0A
/* /def RTC_ALRM0_MINUTE Address of alarm 0 minutes */
#define RTC_ALRM0_MINUTE 0x0B
/* /def RTC_ALRM0_HOUR Address of alarm 0 hours */
#define RTC_ALRM0_HOUR 0x0C
/* /def RTC_ALRM0_DAY Address of alarm 0 day */
#define RTC_ALRM0_DAY 0x0D
/* /def RTC_ALRM0_DATE Address of alarm 0 date */
#define RTC_ALRM0_DATE 0x0E
/* /def RTC_ALRM0_MONTH Address of alarm 0 month */
#define RTC_ALRM0_MONTH 0x0F

/* /def RTC_ALRM1_SECOND Address of alarm 1 seconds */
#define RTC_ALRM1_SECOND 0x11
/* /def RTC_ALRM1_MINUTE Address of alarm 1 minutes */
#define RTC_ALRM1_MINUTE 0x12
/* /def RTC_ALRM1_HOUR Address of alarm 1 hours */
#define RTC_ALRM1_HOUR 0x13
/* /def RTC_ALRM1_DAY Address of alarm 1 day */
#define RTC_ALRM1_DAY 0x14
/* /def RTC_ALRM1_DATE Address of alarm 1 date */
#define RTC_ALRM1_DATE 0x15
/* /def RTC_ALRM1_MONTH Address of alarm 1 month */
#define RTC_ALRM1_MONTH 0x16

/* /def RTC_ALARM_BIT_OUTPUT Bit to set MFP output level */
#define RTC_ALARM_BIT_OUTPUT 7
/* /def RTC_ALARM_BIT_IF Interrupt flag for alarm */
#define RTC_ALARM_BIT_IF 3
/* /def RTC_ALARM_MASK_MATCH Mask for bits that indicate what matches */
#define RTC_ALARM_MASK_MATCH 0x70
/* /def RTC_ALARM_SHIFT_OUTPUT Bits to shift to indicate what matches */
#define RTC_ALARM_SHIFT_MATCH 4

/* /def RTC_ALRM_MATCH_SECONDS Match with alarm seconds */
#define RTC_ALRM_MATCH_SECONDS 0b000
/* /def RTC_ALRM_MATCH_MINUTES Match with alarm minutes */
#define RTC_ALRM_MATCH_MINUTES 0b001
/* /def RTC_ALRM_MATCH_HOURS Match with alarm hours */
#define RTC_ALRM_MATCH_HOURS 0b010
/* /def RTC_ALRM_MATCH_DAY Match with alarm day */
#define RTC_ALRM_MATCH_DAY 0b011
/* /def RTC_ALRM_MATCH_DATE Match with alarm date */
#define RTC_ALRM_MATCH_DATE 0b100
/* /def RTC_ALRM_MATCH_ALL Match with alarm seconds, minutes, hour, day,
		date, month */
#define RTC_ALRM_MATCH_ALL 0b111

/* /def When the value stored on the RTC is zero, this is the year
	it means */
#define RTC_YEAR_ZERO 1980

typedef enum {
    RTC_AM = 0, /* <AM for 12hr clock */
    RTC_PM = 1, /* <PM for 12hr clock */
    RTC_NO_AMPM = 99 /* <Use 24 hour clock */
} rtc_am_pm;

typedef enum {
    RTC_SUN  = 1, /* Day of the week is Sunday */
    RTC_MON  = 2, /* Day of the week is Monday */
    RTC_TUES = 3, /* Day of the week is Tuesday */
    RTC_WED  = 4, /* Day of the week is Wednesday */
    RTC_THUR = 5, /* Day of the week is Thursday */
    RTC_FRI  = 6, /* Day of the week is Friday */
    RTC_SAT  = 7  /* Day of the week is Saturday */
} rtc_weekday;

typedef struct {
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
    rtc_am_pm am_pm;
} rtc_time;

typedef struct {
    rtc_weekday day;
    uint16_t year;
    uint8_t month;
    uint8_t date;
    bool leap_year;
} rtc_date;

twi_master_t rtc_interface;

void twi_init(void);
void rtc_init(rtc_time *inTime, rtc_date *inDate);
void rtc_set_time(rtc_time *inTime);
void rtc_set_date(rtc_date *inDate);
void rtc_get_time(rtc_time *outTime);
void rtc_get_date(rtc_date *outDate);
void rtc_write(uint8_t address, uint8_t *outData, uint8_t dataLength);
void rtc_read(uint8_t address, uint8_t *readData, uint8_t readLength);
bool rtc_battery_powered(void);
void rtc_clear_battery_timestamps(void);
#endif /* RTC_H_ */