/*
 * USART.c
 *
 * Created: 8/6/2012 2:46:33 PM
 *  Author: Meyer
 */
#include <ioport.h>
#include "config/conf_usart_serial.h"
#include <usart_serial.h>
#include <avr/io.h>
//#include <stdio.h>

#include "Pin_Defines.h"
#include "USART_Handler.h"

#include "LED_Bar.h"

FILE usart_serial_stream =
    FDEV_SETUP_STREAM(usart_local_put, usart_local_get, _FDEV_SETUP_RW);

static usart_serial_options_t usart_options = {
    .baudrate = USART_SERIAL_BAUDRATE,
    .charlength = USART_SERIAL_CHAR_LENGTH,
    .paritytype = USART_SERIAL_PARITY,
    .stopbits = USART_SERIAL_STOP_BIT
};

void usart_init()
{

    cli();

    ioport_configure_pin(USART_TX, IOPORT_DIR_OUTPUT | IOPORT_INIT_HIGH);
    ioport_configure_pin(USART_RX, IOPORT_DIR_INPUT);

    *USART_SERIAL.CTRLA |= USART_RXCINTLVL_LO_gc;

    usart_serial_init(USART_SERIAL, &usart_options);

    sei();
}

enum status_code usart_local_put(char inChar, FILE ignore)
{
    return usart_putchar(USART_SERIAL, inChar);
}

uint8_t usart_local_get(FILE ignore)
{
    return usart_getchar(USART_SERIAL);
}

ISR(USART_SERIAL_INT_RX)
{
    char msg;
    //uint16_t big_msg;

    msg = *USART_SERIAL.DATA;
    //big_msg = msg;
    //usart_putchar(USART_SERIAL, msg);
    fprintf(&usart_serial_stream, "%c", msg);
    //led_bar_set_leds(&big_msg);
}
