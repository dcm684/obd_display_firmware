/*
 * Nav_Switch.c
 *
 * Created: 7/23/2012 11:29:37 AM
 *  Author: Meyer
 */

#include <ioport.h>

#include "Pin_Defines.h"
#include "Nav_Switch.h"

/**
 * \brief Initializes the navigation switch I/O
 */
void nav_init()
{
    struct adc_config adc_cfg;
    struct adc_channel_config chan_cfg;

    adc_read_configuration(&NAV_ADC_PORT, &adc_cfg);
    adcch_read_configuration(&NAV_ADC_PORT,
                             NAV_ADC_CHANNEL_MASK, &chan_cfg);

    adc_set_conversion_parameters(&adc_cfg,
                                  ADC_SIGN_ON, /* Unsigned */
                                  ADC_RES_12, /* 12-bit conversion, right justified */
                                  ADC_REF_VCCDIV2); /* VCC / 2 */

    adc_set_conversion_trigger(&adc_cfg,
                               ADC_TRIG_MANUAL, /* Manually trigger conversion */
                               1, /* Number of channels to run a conversion on */
                               NAV_ADC_PIN); /* Channel to trigger the check of multiple conversions */

    /* Conversion clock to approx. 100kHz */
    adc_set_clock_rate(&adc_cfg, 100000UL);

    adcch_set_input(&chan_cfg,
                    NAV_ADC_POSITIVE, /* Set what pin has the positive voltage */
                    ADCCH_NEG_PAD_GND, /* Set the negative value to external ground,
								saw that this increases accuracy */
                    0); /* 0 = 1/2 Set voltage gain applied before ADC read */

    adc_write_configuration(&NAV_ADC_PORT, &adc_cfg);
    adcch_write_configuration(&NAV_ADC_PORT, NAV_ADC_CHANNEL_MASK, &chan_cfg);

    ioport_configure_pin(NAV_SELECT, IOPORT_DIR_INPUT);

    nav_all_switches.down = NAV_OFF;
    nav_all_switches.left = NAV_OFF;
    nav_all_switches.up = NAV_OFF;
    nav_all_switches.right = NAV_OFF;
    nav_all_switches.select = NAV_OFF;

}

/**
 * \brief Returns the state of the navigation switch
 *
 * First it checks if the select is pressed then it checks to see if
 * any of the directions is pressed
 *
 * \returns State of the select switch
 *	- NAV_PRESS_UP
 *	- NAV_PRESS_RIGHT
 *	- NAV_PRESS_DOWN
 *	- NAV_PRESS_LEFT
 *	- NAV_PRESS_SELECT
 *	- NAV_PRESS_NONE
 */
uint8_t nav_check()
{
    int16_t result;
    uint8_t retVal;

    retVal = NAV_PRESS_NONE;

    if(nav_button_update(&(nav_all_switches.select),
                         (ioport_get_pin_level(NAV_SELECT)))) {
        retVal |= NAV_PRESS_SELECT;
    }

    if(nav_all_switches.select != NAV_PRESSED)	{
        adc_enable(&NAV_ADC_PORT);
        adc_start_conversion(&NAV_ADC_PORT, NAV_ADC_CHANNEL_MASK);
        adc_wait_for_interrupt_flag(&NAV_ADC_PORT, NAV_ADC_CHANNEL_MASK);
        result = adc_get_signed_result(&NAV_ADC_PORT, NAV_ADC_CHANNEL_MASK);

        if(nav_button_update(&(nav_all_switches.left),
                             (result > NAV_ADC_LEFT))) {
            retVal |= NAV_PRESS_LEFT;
        }

        if(nav_button_update(&(nav_all_switches.down),
                             (result < NAV_ADC_LEFT) && (result > NAV_ADC_DOWN))) {
            retVal |= NAV_PRESS_DOWN;
        }

        if(nav_button_update(&(nav_all_switches.right),
                             (result < NAV_ADC_DOWN) && (result > NAV_ADC_RIGHT))) {
            retVal |= NAV_PRESS_RIGHT;
        }

        if(nav_button_update(&(nav_all_switches.up),
                             (result < NAV_ADC_RIGHT) && (result > NAV_ADC_UP))) {
            retVal |= NAV_PRESS_UP;
        }
    }

    return retVal;
}

/**
 * Updates the given nav_button_state in accordance to the current
 * button state
 *
 * /param inState nav_button_state containing the past state of the button,
 *	this value will be updated
 * /param pressed The pressed state, on or off, that will be used to update
 *  the given nav_button_state
 *
 * /returns Was the button just pressed or released
 */
bool nav_button_update(nav_button_state *inState, bool pressed)
{
    bool retVal = false;
    if(pressed) {
        if((*inState == NAV_PRESSED) ||
                (*inState == NAV_HELD)) {
            *inState = NAV_HELD;
        } else {
            *inState = NAV_PRESSED;
            retVal = true;
        }
    } else {
        if((*inState == NAV_PRESSED) ||
                (*inState == NAV_HELD)) {
            *inState = NAV_RELEASED;
            retVal = true;
        } else {
            *inState = NAV_OFF;
        }
    }
    return retVal;
}
