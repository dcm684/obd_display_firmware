/**
 * MMA7361.c - MMA7361 Accelerometer Library
 * Written by Christopher Meyer, July 2011
 * Version 1.0
 *
 * This library is used to interface an Arduino compatible system with an
 * MMA7361 accelerometer. After being initialized this library can produce
 * an averaged raw, offset, or g-reading for a given axis.
 *
 * The sign of the force is directly related to the value of the value received
 * from the ADC. Higher voltages received from the MMA7361 will always result in
 * greater values than lower voltage readings.
 *
 * This library should be extensible to other accelerometers. The only changes
 * that will need to be made are to those calculating g-forces. The formulas
 * used are listed in the header file with the voltage and g-range #defines.
 *
 */

#include <ioport.h>
#include "MMA7361.h"

static void accel_finish_read(ADC_t *adc, uint8_t ch_mask, adc_result_t result);

/**
* Applies an infinite impulse response filter to get a smoothed average accelerometer value
*
* WIthout this there would be some noise being picked up
*
* @param toAdd	Value to add to the average
* @param runningAverage	Previous average value
*
@return Averaged accelerometer value
*/
uint16_t accel_apply_iir(uint16_t toAdd, uint16_t runningAverage)
{
    int32_t averageScratch;

    averageScratch = toAdd;
    averageScratch -= runningAverage;

    if (averageScratch < 0) {
        averageScratch = -averageScratch;
        averageScratch = averageScratch >> ACCEL_IIR_SHIFT;
        averageScratch = -averageScratch;
    } else {
        averageScratch = averageScratch >> ACCEL_IIR_SHIFT;
    }
    runningAverage += averageScratch;

    return runningAverage;
}

/**
 * Initialize and AccelCalibrate the accelerometer
 *
 * In addition to setting the pins for all 3 axes, g-range, and the number
 * of points to average over the chip will be calibrated assuming that the
 * chip is sitting on stationary and level surface.
 *
 * Whenever the reference voltage is changed or g-range selector is modified,
 * this function will need to be re-run.
 *
 * If an invalid value is received for gRange, the default is 1.5g.
 *
 * \param in_channel_x ADC channel for the x-axis
 * \param in_channel_y ADC channel for the y-axis
 * \param in_channel_z ADC channel for the z-axis
 * \param gRange Set sensitivity and range of accelerometer
 *          ACCEL_GS_60 -- -6G to 6G
 *          ACCEL_GS_15 -- -1.5G to 1.5G
 * \param point Number of points to average when reading an axis
 *
 */
void accel_init(accel_all_chans *inAllChans,
                int gRange,
                unsigned long points)
{
    _z_bits_zero = 0;
    _g_conversion = 0;

    ioport_configure_pin(ACCEL_G_SEL, IOPORT_DIR_OUTPUT);

    inAllChans->axis_x.port = &ACCEL_ADC_PORT;
    inAllChans->axis_x.channel_mask = ACCEL_MASK_X;
    inAllChans->axis_x.channel_pos_input = ACCEL_POSITIVE_X;
    inAllChans->axis_x.pin_number = ACCEL_PIN_X;
    inAllChans->axis_x.offset = 0;
    accel_single_axis_init(&(inAllChans->axis_x));

    inAllChans->axis_y.port = &ACCEL_ADC_PORT;
    inAllChans->axis_y.channel_mask = ACCEL_MASK_Y;
    inAllChans->axis_y.channel_pos_input = ACCEL_POSITIVE_Y;
    inAllChans->axis_y.pin_number = ACCEL_PIN_Y;
    inAllChans->axis_y.offset = 0;
    accel_single_axis_init(&(inAllChans->axis_y));

    inAllChans->axis_z.port = &ACCEL_ADC_PORT;
    inAllChans->axis_z.channel_mask = ACCEL_MASK_Z;
    inAllChans->axis_z.channel_pos_input = ACCEL_POSITIVE_Z;
    inAllChans->axis_z.pin_number = ACCEL_PIN_Z;
    inAllChans->axis_z.offset = 0;
    accel_single_axis_init(&(inAllChans->axis_z));

    /* Set the voltage and g-range specific conversion constants */
    if(gRange == ACCEL_GS_60) {
        _z_bits_zero = ACCEL_Z_BITS_ZERO_33_60;
        _g_conversion = ACCEL_G_CONVERSION_33_60;
        ioport_set_pin_high(ACCEL_G_SEL);

    } else {
        _z_bits_zero = ACCEL_Z_BITS_ZERO_33_15;
        _g_conversion = ACCEL_G_CONVERSION_33_15;
        ioport_set_pin_low(ACCEL_G_SEL);
    }

    adc_set_callback(&ACCEL_ADC_PORT, &accel_finish_read);
    inAllChans->activeAxis = accel_axis_type_x;
    accel_start_read(&accelChans);

//accel_calibrate(in_channel_x, in_channel_y, in_channel_z);
}

/**
 * Sets up one ADC channel for use with the accelerometer
 *
 * \param inChannel ADC channel to set up for use
 */
void accel_single_axis_init(adcchan *inChannel)
{
    struct adc_config adc_cfg;
    struct adc_channel_config chan_cfg;

    adc_read_configuration(inChannel->port, &adc_cfg);
    adcch_read_configuration(inChannel->port,
                             inChannel->channel_mask, &chan_cfg);

    adc_set_conversion_parameters(&adc_cfg,
                                  ADC_SIGN_ON,	/* Signed. Necessary for differnetial mode */
                                  ADC_RES_12,	/* 12-bit conversion, right justified */
                                  ADC_REF_VCCDIV2 /* VCC / 2 (Input signal is reducef by 1/2 gain
								  in adcch_set_input()*/
                                 );

    adc_set_conversion_trigger(&adc_cfg,
                               ADC_TRIG_MANUAL, /* Manually trigger conversion */
                               1, /* Number of channels to run a conversion on */
                               inChannel->pin_number); /* Channel to trigger the check of multiple conversions */

    /* Conversion clock to approx. 100kHz */
    adc_set_clock_rate(&adc_cfg, 100000UL);

    adcch_set_input(&chan_cfg,
                    inChannel->channel_pos_input, /* Set what pin has the positive voltage */
                    ADCCH_NEG_PAD_GND, /* Set the negative value to external ground,
                    saw that this increases accuracy. Differential mode */
                    0); /* 0 = 1/2 Set voltage gain applied before ADC read */

    /* Enable interrupts on the channel */
    adcch_enable_interrupt(&chan_cfg);

    adc_write_configuration(inChannel->port, &adc_cfg);
    adcch_write_configuration(inChannel->port, inChannel->channel_mask, &chan_cfg);
}

/**
 * Begins the read on the given axis and returns
 *
 * @param inAxes	Struct of axis ADC channels with the activeAxis variable set to the
 *				variable that shall be read
 */
void accel_start_read(accel_all_chans *inAxes)
{
    adcchan *activeChan;

    switch (inAxes->activeAxis) {
    case accel_axis_type_x:
        activeChan = &(inAxes->axis_x);
        break;
    case accel_axis_type_y:
        activeChan = &(inAxes->axis_y);
        break;
    case accel_axis_type_z:
        activeChan = &(inAxes->axis_z);
        break;
    default:
        return;
    }

    adc_enable(activeChan->port);
    adc_start_conversion(activeChan->port, activeChan->channel_mask);

    activeAllChans = inAxes;
}

/**
 * Reads the result of the started and now completed ADC read and applies the IIR filter
 *
 * @param	startNext	Should the next axis be read at the completion of the previous read?
 */
static void accel_finish_read(ADC_t *adc, uint8_t ch_mask, adc_result_t result)
{
    adcchan *activeChan;
    bool validChannel = true;

    if ((adc == activeAllChans->axis_x.port)
            && (ch_mask == activeAllChans->axis_x.channel_mask)) {
        activeChan = &(activeAllChans->axis_x);

    } else if ((adc == activeAllChans->axis_y.port)
               && (ch_mask == activeAllChans->axis_y.channel_mask)) {
        activeChan = &(activeAllChans->axis_y);

    } else if ((adc == activeAllChans->axis_z.port)
               && (ch_mask == activeAllChans->axis_z.channel_mask)) {
        activeChan = &(activeAllChans->axis_z);

    } else {
        validChannel = false;
    }

    /* If a valid channel was just read get the value and process it */
    if (validChannel) {
        activeChan->rawValue = result;
        activeChan->value = accel_apply_iir(result, activeChan->value);
    }

    /* If the next read should be started, see what axis should be started */
    switch (activeAllChans->activeAxis) {
    case accel_axis_type_x:
        activeAllChans->activeAxis = accel_axis_type_y;
        break;
    case accel_axis_type_y:
        activeAllChans->activeAxis = accel_axis_type_z;
        break;
    case accel_axis_type_z:
    default:
        activeAllChans->activeAxis = accel_axis_type_none;
        break;
    }

    /* Start the read if there is an axis to be read */
    if (activeAllChans->activeAxis != accel_axis_type_none) {
        accel_start_read(activeAllChans);
    }
}

/**
 * Returns the filtered, unoffset value received from the A/D pin for a given axis.
 *
 * \param inAxis ADC channel to read from
 *
 * \return Non-offset value received from the ADC
 *
 */
uint16_t accel_get_raw(adcchan *axis)
{
    return axis->value;
}

/**
 * Returns the value received from the A/D pin offset where 0g is 0.
 *
 * If the axis is invalid, the returned value is ACCEL_BAD_AXIS_VAL.
 *
 * \param axis ADC channel to read from
 *
 * \return ADC value offset where 0g is 0.
 *
 */
int16_t accel_get_raw_with_offset(adcchan *axis)
{
    return (accel_get_raw(axis) - (axis->offset) - ACCEL_MID_POINT_33);
}

/**
 * Read the sensor for the given axis and returns the force in milli-gs
 *
 * If an invalid axis is given ACCEL_BAD_AXIS_VAL will be returned.
 *
 * The resolution for the value depends on the reference voltage and g-scale
 * used. The resolutions in g/b are as follows:
 *
 *                      Voltage (V)
 *                  3.3         5.0
 * Scale    1.5     0.00403     0.0156
 * (g)      9.0     0.00610     0.0237
 *
 * \param ADC channel to read from
 *
 * \return Floating point value of Gs in milliGs exerted on the given axis.
 *
 */
int16_t accel_get_in_g(adcchan *axis)
{
    int32_t retVal;

    //retVal = get_accel_raw_offset(axis);
    retVal = accel_get_raw(axis);
    retVal = retVal - ACCEL_MID_POINT_33;

    /* Multiply by 1000 so milli-gs will be returned */
    retVal = retVal * 1000;
    retVal = (retVal * ACCEL_VALUE_SCALE / _g_conversion);

    return (int16_t) retVal;
}

/**
 * Converts a stored raw accelerometer value for a given axis into milli-gs
 *
 * If an invalid axis is given ACCEL_BAD_AXIS_VAL will be returned.
 *
 * The resolution for the value depends on the reference voltage and g-scale
 * used. The resolutions in g/b are as follows:
 *
 *                      Voltage (V)
 *                  3.3         5.0
 * Scale    1.5     0.00403     0.0156
 * (g)      6.0     0.00610     0.0237
 *
 * \param ADC channel to read from
 *
 * \return Floating point value of Gs in milliGs exerted on the given axis.
 *
 */
int16_t accel_calc_in_g(adcchan *axis)
{
    int32_t retVal;

    retVal = axis->value;
    retVal = retVal - ACCEL_MID_POINT_33;

    /* Multiply by 1000 so milli-gs will be returned */
    retVal = retVal * 1000;
    retVal = (retVal * ACCEL_VALUE_SCALE / _g_conversion);

    return (int16_t) retVal;
}

/**
 * Calculates and stores the offsets for all three axes.
 *
 * The assumption is that the system is stationary and on a level surface.
 * The system does not necessarily need to be flat, e.g. it can be on placed
 * at a 45deg angle. As long as the system will remain at 45deg it will
 * operate properly. The 1g due to gravity will spread among all three axis.
 *
 */
void accel_calibrate(adcchan *in_channel_x,
                     adcchan *in_channel_y,
                     adcchan *in_channel_z)
{
    int32_t tempOffset;

    tempOffset = ((((int32_t)accel_get_raw(in_channel_x)) << 4) - ACCEL_XY_BITS_ZERO_33) >> 4;
    in_channel_x->offset = tempOffset;

    tempOffset = ((((int32_t)accel_get_raw(in_channel_y)) << 4) - ACCEL_XY_BITS_ZERO_33) >> 4;
    in_channel_y->offset = tempOffset;

    tempOffset = ((((int32_t)accel_get_raw(in_channel_z)) << 4) - _z_bits_zero) >> 4;
    in_channel_z->offset = tempOffset;
}