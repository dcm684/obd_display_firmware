/*
 * LED_Bar.c
 * Controls the 16 LEDs on the unit.
 *
 * The 16 LEDs bar is used to provide a non-textual representation of some
 * given data.
 *
 * Created: 7/18/2012 2:53:02 PM
 *  Author: Christopher Meyer
 */
#include <inttypes.h>
#include <ioport.h>

#include "Pin_Defines.h"
#include "LED_Bar.h"
#include "OBD\OBDPidFunctions.h"
#include "OBD\OBD.h"
//#include "USART_Handler.h"

void led_bar_init()
{
    /* Set DDR for LEDs */
    ioport_set_pin_dir(LED_BAR_CLOCK, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(LED_BAR_DATA, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(LED_BAR_LATCH, IOPORT_DIR_OUTPUT);
    ioport_set_pin_dir(LED_BAR_OE, IOPORT_DIR_OUTPUT);

    /* Initialize pin values. Value is irrelevant here */
    ioport_set_pin_low(LED_BAR_CLOCK);
    ioport_set_pin_low(LED_BAR_LATCH);
    ioport_set_pin_high(LED_BAR_OE);

    bar_centered = false;
    bar_fill = false;
    bar_invert = false;
    globalLEDPID = 0x00;
    bar_rangesScaled = false;
}

/**
 * Sets up the pattern, range, and PID for the LED bar
 *
 * \param inList List to add the PID to track
 * \param inPid PID to track
 * \param low_end Bottom of the number range, anything less results in the
 *	all off state
 * \param high_end Upper end of number range, anything at or above the
 *	value, results in the all on state.
 * \param pattern Set the pattern of the LEDs
 */
void led_bar_set_up_interrupt( volatile obd_pid_list *inList, uint16_t inPid, int min, int max, uint8_t pattern )
{
    globalLEDPID = inPid;
    obd_add_real_pids_to_list(inList, inPid);
    led_bar_set_pattern(min, max, pattern);
}

/**
 * Sets up the pattern, range, and PID for the LED bar
 *
 * \param inList List to add the PID to track
 * \param inPid PID to track
 * \param low_end Bottom of the number range, anything less results in the
 *	all off state
 * \param high_end Upper end of number range, anything at or above the
 *	value, results in the all on state.
 * \param pattern Set the pattern of the LEDs
 */
void led_bar_set_up_interrupt_float( volatile obd_pid_list *inList, uint16_t inPid, float min, float max, uint8_t pattern )
{
    int outMin;
    int outMax;

    outMin = (int) (min * LED_SCALAR);
    outMax = (int) (max * LED_SCALAR);
    bar_rangesScaled = true;

    led_bar_set_up_interrupt(inList, inPid, outMin, outMax, pattern);
}

/**
 * Called after an interrupt to update the led bar
 */
void led_bar_interrupt_handler(void)
{
    obd_pid tempPID;
    obd_value_with_unit obdValue;
    float tempFloat = 0;
    int16_t tempInt;


    /* Get the value and convert it to the used units */
    if (globalLEDPID < PID_NON_STD_START) {
        obd_get_pid(&pidsToQuery, globalLEDPID, &tempPID);
        tempPID.unit_type = obd_unit_english;
        obd_translate_pid(&tempPID, &obdValue);

        /* Check if it is an accelerometer value */
    } else if(!obd_check_and_process_accel_pid(globalLEDPID, &obdValue)) {
        /* Non-standard PID that isn't an accelerometer value */
        tempPID.pid = globalLEDPID;
        tempPID.unit_type = obd_unit_english; /* @TODO: This needs a better method
												of choosing combo unit types */

        obd_translate_nonstd_pid(&tempPID, &obdValue);
    }

    if (bar_rangesScaled) {
        tempFloat = (obdValue.value * LED_SCALAR);
    }

    tempInt = (int16_t) tempFloat;
    led_bar_set_value(&(tempInt));
    //fprintf_P(&usart_serial_stream, PSTR("Value: %f (%f) -> %i %X\r\n"), obdValue.value, tempFloat, tempInt, globalLEDPID);
    //fprintf_P(&usart_serial_stream, PSTR("Range: %i - %i\r\n"), bar_points[0], bar_points[15]);

}

/**
 * Sets the pattern of the LED bar.
 *
 * LEDs on board should be bit 0, 1, 2, ..., 14, 15
 *
 * \param led_pattern Reverse ordered LED pattern, LSB is rightmost LED
 */
void led_bar_set_leds(uint16_t led_pattern)
{
    uint16_t mask; /* Mask used to test each bit of input */

    ioport_set_pin_low(LED_BAR_LATCH);

    for(mask = 0x8000; mask != 0; mask = mask >> 1) {
        ioport_set_pin_low(LED_BAR_CLOCK);
        ioport_set_pin_level(LED_BAR_DATA, led_pattern & mask);
        ioport_set_pin_high(LED_BAR_CLOCK);
    }

    ioport_set_pin_high(LED_BAR_LATCH);
    ioport_set_pin_low(LED_BAR_OE);
}

/**
 * \brief Sets the start, stop, distribution, and inversion of sub points
 * for the LED bar
 *
 * \param low_end Bottom of the number range, anything less results in the
 *	all off state
 * \param high_end Upper end of number range, anything at or above the
 *	value, results in the all on state.
 * \param pattern Set the pattern of the LEDs
 */
void led_bar_set_pattern(int low_end, int high_end, uint8_t pattern)
{
    int i;

    float step_size;
    float last_value;
    float temp_value;

    bar_invert = (pattern & LED_PATTERN_INVERT) > 0;
    bar_fill = (pattern & LED_PATTERN_FILL) > 0;

    bar_centered = false;
    bar_radiate = false;

    if((pattern & LED_PATTERN_TYPE_MASK) == LED_PATTERN_CENTERED) {
        bar_centered = true;

    } else if((pattern & LED_PATTERN_TYPE_MASK) == LED_PATTERN_RADIATE) {
        bar_radiate = true;
    }

    step_size = (high_end - low_end);
    last_value = low_end;

    /* Non-radiating patterns have a smaller for each LED and start at
    LED 0 */
    if(!bar_radiate) {
        step_size = step_size / 15;

        bar_points[0] = low_end;
        bar_points[15] = high_end;

        for(i = 1; i < 15; i++) {
            temp_value = last_value + step_size;
            bar_points[i] = temp_value + 0.5;
            last_value = temp_value;
        }

    } else {
        /* The LED range for radiating patterns is twice as big and start
        at LED 8 */
        step_size = step_size / 7;

        bar_points[7] = low_end;
        bar_points[8] = low_end;
        bar_points[0] = high_end;
        bar_points[15] = high_end;

        for(i = 1; i < 7; i++) {
            temp_value = last_value + step_size;
            bar_points[8 + i] = temp_value + 0.5;
            bar_points[7 - i] = temp_value + 0.5;
            last_value = temp_value;
        }
    }
}

/**
 * Set a new value for the LED bar to represent
 *
 * \param Value to represent on the LED bar
 */
void led_bar_set_value(int16_t *value)
{
    uint16_t led_pattern;
    int i;

    led_pattern = 0;

    /*if(bar_fill) {
    	for (i = 0; i < 16; i++) {
    		if((*value) >= bar_points[i]) {
    			led_pattern = led_pattern | (1 << i);
    		}
    	}
    } else {*/
    /* Find LEDs on transition */
    for(i = 0; i < 16; i++) {
        /* Is the value at least as big as the LEDs value? */
        if(((*value) >= bar_points[i]) &&
                /* When bar_radiate is set only pay attention to values
                for last 8 LEDs */
                ((!bar_radiate) || (i >= 8)) &&
                /* Only illuminate the LED if the next LED isn't, its
                the last LED, or you are to fill the LED bar */
                (((i < 15) && ((*value) < bar_points[i + 1])) ||
                 (i == 15) ||
                 (bar_fill && (!bar_radiate || i >= 8)))) {
            led_pattern = led_pattern | (1 << i);

            if(bar_radiate) {
                led_pattern = led_pattern | (1 << (15 - i));
            }
        }
    }

    //}

    if(bar_centered) {
        led_pattern = led_pattern ^ (0x00FF);
    }

    if(bar_invert) {
        led_pattern = ~(led_pattern);
    }

    led_bar_set_leds(led_pattern);
}