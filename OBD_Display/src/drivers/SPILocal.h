/*
 * SPILocal.h
 *
 * Created: 7/20/2012 11:01:32 PM
 *  Author: Meyer
 */


#ifndef SPILOCAL_H_
#define SPILOCAL_H_

#include <ioport.h>

typedef struct {
    port_pin_t mosi;
    port_pin_t miso;
    port_pin_t clock;
    port_pin_t cs;
    SPI_t *spi;
} spi_entire_device;

void init_master_spi(spi_entire_device *inDevice);
void spi_simple_read_packet(spi_entire_device *inDevice, uint8_t *data, size_t dataLength);
void spi_simple_send_packet(spi_entire_device *inDevice, uint8_t *data, size_t dataLength);
//uint8_t spi_simple_read_single(spi_entire_device *inDevice);
//void spi_simple_send_single(spi_entire_device *inDevice, uint8_t data);
void spi_naked_send_byte(spi_entire_device *inDevice, uint8_t outByte);
uint8_t spi_naked_read_byte(spi_entire_device *inDevice);
#endif /* SPILOCAL_H_ */