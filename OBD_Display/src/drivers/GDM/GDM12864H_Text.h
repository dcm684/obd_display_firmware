/*
 * GDM12864H_Text.h
 *
 * Created: 8/2/2012 10:40:58 PM
 *  Author: Meyer
 */


#ifndef GDM12864H_TEXT_H_
#define GDM12864H_TEXT_H_

#include <inttypes.h>
#include <stdio.h>
#include "GDM12864H.h"

/* /def GDM_text_scroll_rows Scrolls the screen (row * text_height)
	pixel rows. If line spacing is to be maintained the value must be
	included in the text_height value */
#define GDM_text_scroll_rows(rows, text_height) \
	gdm_scroll_screen(rows * text_height)

/* /def GDM_TAB_SIZE Number of characters per tab */
#define GDM_TAB_SIZE 4

typedef struct {
    uint8_t char_width;
    uint8_t char_height;
    const uint8_t *charArray;
    uint8_t char_spacing;
    uint8_t line_spacing;
    pixel_state activeState;
} gdm_font;

/**
 * Used to track a text entry that has been displayed
 */
typedef struct {
    gdm_font	*font;
    char		*string;
    uint8_t		column;
    uint8_t		row;
    uint8_t		length;
    bool		wordWrap;
} gdm_text;

gdm_font *activeFont;
uint8_t gdm_cursor_x;
uint8_t gdm_cursor_y;

FILE gdm_serial_stream;

void gdm_move_cursor(uint8_t row, uint8_t col);
int gdm_putchar(char inChar, void *unused);
void gdm_puts(const char *inStr);
void gdm_puts_P(const char *inStr);
void gdm_font_init(void);
void gdm_load_font(uint8_t font_height, uint8_t font_width, const uint8_t fontData[], uint8_t char_spacing, uint8_t line_spacing, pixel_state active_state);
void gdm_text_set_row_value(uint8_t row, pixel_state value);
void gdm_text_set_current_row_value(pixel_state value);
void gdm_text_set_state(pixel_state inState);

void gdm_text_print_struct(gdm_text *inStruct);
#endif /* GDM12864H_TEXT_H_ */