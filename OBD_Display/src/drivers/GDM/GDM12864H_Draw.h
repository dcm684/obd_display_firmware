/**
 * GDM12864H_Draw.h
 *
 * Created: 8/3/2012 2:51:10 PM
 *  Author: Meyer
 */


#ifndef GDM12864H_DRAW_H_
#define GDM12864H_DRAW_H_

#include <inttypes.h>
#include <stdbool.h>
#include "GDM12864H.h"

/**
 * @def GDM_TITLE_LENGTH
 * Maximum length of a title for a GDM drawing
 */
#define GDM_TITLE_LENGTH 20

typedef enum {
    gdm_quadrant_ne	= 1,	/*< Top right quadrant (northeast) quadrant */
    gdm_quadrant_se	= 2,	/*< Bottom right quadrant (southeast) quadrant */
    gdm_quadrant_sw	= 4,	/*< Top left quadrant (southwest) quadrant */
    gdm_quadrant_nw	= 8,	/*< Bottom left quadrant (northwest) quadrant */
    gdm_quadrant_all = 15	/*< All four quadrants */
} gdm_circle_quadrant;

enum graph_orientation {
    vertical_bottom,		/*!< Bar is vertical and min value is at the bottom */
    vertical_top,			/*!< Bar is vertical and min value is at the top */
    horizontal_right,		/*!< Bar is horizontal and min value is on the right */
    horizontal_left			/*!< Bar is horizontal and min value is on the left */
};

enum dial_style {
    circle_3_4 = 0,			/*!< 3/4 circle gauge, where lower 1/4 of circle is blank */
    circle_full,			/*!< Fill circle dial */
    semicircle_left = 10,	/*!< Half circle on left hand side */
    semicircle_right,		/*!< Half circle on right hand side */
    semicircle_up,			/*!< Half circle on the upper half */
    semicircle_down			/*!< Half circle on the lower half */
};

/**
 * /struct All of the values needed to draw a line
 */
typedef struct {
    int16_t x1;					/*!< X-coordinate of one of the lines end
										points */
    int16_t y1;					/*!< Y-coordinate of one of the lines end
										points */
    int16_t x2;					/*!< X-coordinate of the line's other end
										point */
    int16_t y2;					/*!< Y-coordinate of the line's other end
										point */
    pixel_state activeState;	/*!< Active state of the pixels */
} gdm_line;

/**
 * /struct All of the values needed to draw a rectangle
 */
typedef struct {
    int16_t x1;					/*!< X-coordinate of corner one of the
										rectangle */
    int16_t y1;					/*!< Y-coordinate of corner one of the
										rectangle */
    int16_t x2;					/*!< X-coordinate of corner two of the
										rectangle */
    int16_t y2;					/*!< Y-coordinate of corner two of the
										rectangle */
    bool filled;				/*!< Should the rectangle be filled? */
    pixel_state activeState;	/*!< Active state of the pixels */
} gdm_rectangle;

/**
 * /struct All of the points needed to draw an arc
 */
typedef struct {
    int16_t x_center;			/*!< X-coordinate for the arc's
									center */
    int16_t y_center;			/*!< Y-coordinate for the arc's
									center */
    uint8_t radius;				/*!< Radius of the arc */
    int16_t start_angle;		/*!< Angle (degrees) where the arc starts.
										0 degrees is 3 o'clock */
    int16_t stop_angle;			/*!< Angle (degrees) where the arc stops.
										0 degrees is 3 o'clock */
    pixel_state activeState;	/*!< Active state of the pixels */
} gdm_arc;

/**
 * /struct All of the values needed to draw a circle
 */
typedef struct {
    int16_t x_center;			/*!< X-coordinate for the circle's
									center */
    int16_t y_center;			/*!< Y-coordinate for the circle's
									center */
    int16_t radius;				/*!< Radius of the circle */
    uint8_t quadrants;			/*!< Quadrants of the circle to draw */
    bool filled;				/*!< Should the circle be filled? */
    pixel_state activeState;	/*!< Active state of the pixels */
} gdm_circle;

/**
 * /struct All of the values needed to draw a bar graph
 */
typedef struct {
    float		value;					/*!< Value to represent on graph */
    float		min;					/*!< Minimum value for the bar graph */
    float		max;					/*!< Maximum value for the bar graph */
    uint8_t		coordX;					/*!< X coordinate of upper left corner */
    uint8_t		coordY;					/*!< Y coordinate of upper left corner */
    uint8_t		length;					/*!< Length of bar graph */
    uint8_t		thickness;				/*!< Thickness of bar in graph */
    enum graph_orientation orientation;	/*!< Direction of bar graph */
    pixel_state	activeState;			/*!< "Color" of active part of graph */
    bool		drawBorder;				/*!< Draw aboarder around the graph? */
    uint8_t		lastPixel;				/*!< Row or column of last pixel drawn last time graph was drawn (Internal) */
    bool		alreadyDrawn;			/*!< Has the graph already been drawn? (Internal) */
} gdm_bar_graph;

/**
 * /struct All of the values needed to draw an analog dial
 */
typedef struct {
    float value;				/*!< Value to represent on dial */
    float min;					/*!< Minimum value for dial */
    float max;					/*!< Maximum value for dial */
    uint8_t centerX;			/*!< X position for the center of the dial */
    uint8_t centerY;			/*!< Y position for the center of the dial */
    uint8_t radius;				/*!< Radius of the dial */
    enum dial_style dialType;	/*!< Style of dial */
    pixel_state activeState;	/*!< Line "color" */
    char title[GDM_TITLE_LENGTH];   /*!< Title for dial */
    bool displayTitle;			/*!< Display the dial's title? */
    float coarseInterval;		/*!< Interval between coarse ticks */
    float fineInterval;			/*!< Interval between fine ticks */
    bool displayCoarseValues;	/*!< Display values for coarse ticks */
    bool alreadyDrawn;			/*!< Has the dial already been drawn (Internal) */
    int32_t lastValue;			/*!< The last value drawn (Internal) */
    float angleStart;			/*!< Angle for the needle at the minimum value (Internal) */
    float angleRange;			/*!< Total needle range (Internal) */
} gdm_analog_dial;

void gdm_draw_line(int16_t x1, int16_t y1, int16_t x2, int16_t y2, pixel_state activeState);
void gdm_draw_line_struct(gdm_line *inStruct);

void gdm_draw_rectangle(int16_t x1, int16_t y1, int16_t x2, int16_t y2, bool filled, pixel_state activeState);
void gdm_draw_rectangle_struct( gdm_rectangle *inStruct);

void gdm_draw_arc(int16_t center_x, int16_t center_y, uint8_t radius, int16_t startAngle, int16_t stopAngle, pixel_state activeState);
void gdm_draw_arc_struct(gdm_arc *inStruct);

void gdm_draw_circle(int16_t center_x, int16_t center_y, uint8_t radius, gdm_circle_quadrant quadrants, bool filled, pixel_state activeState);
void gdm_draw_circle_struct (gdm_circle *inStruct);

void gdm_draw_bar_graph(gdm_bar_graph *graph);

void gdm_draw_dial(gdm_analog_dial *inDial);
void gdm_draw_dial_needle(gdm_analog_dial *inDial, int32_t *inValue, bool undraw);

#endif /* GDM12864H_DRAW_H_ */