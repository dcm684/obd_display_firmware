/*
 * GDMDebug.c
 *
 * Created: 5/8/2014 9:48:20 PM
 *  Author: Meyer
 */
#include "GDMDebug.h"
#include "..\USART_Handler.h"
#include "GDM12864H_Draw.h"
#include "GDM12864H.h"
#include "Display.h"

/**
 * Prints out the value of a pixel_state
 */
void gdm_debug_active_state(pixel_state inState)
{

    fprintf_P(&usart_serial_stream, PSTR("Pixel State:\t"));
    switch (inState) {
    case pixel_off:
        fprintf_P(&usart_serial_stream, PSTR("Off\r\n"));
        break;
    case pixel_on:
        fprintf_P(&usart_serial_stream, PSTR("On\r\n"));
        break;
    case pixel_invert:
        fprintf_P(&usart_serial_stream, PSTR("Invert\r\n"));
        break;
    case pixel_nochange:
        fprintf_P(&usart_serial_stream, PSTR("No change\r\n"));
        break;
    default:
        fprintf_P(&usart_serial_stream, PSTR("ERROR\r\n"));
        break;
    }
}

/**
 * Prints out the contents of a text struct
 */
void gdm_debug_text_struct(gdm_text *inStruct)
{
    fprintf_P(&usart_serial_stream, PSTR("Text Struct:\r\n"\
                                         "Font: Unimplemented\r\n"\
                                         "String: %s\r\n"\
                                         "Column: %u\r\n"\
                                         "Row: %u\r\n"\
                                         "Length: %u\r\n"\
                                         "Wrap: %u\r\n"),
              //inStruct->font,
              inStruct->string,
              inStruct->column,
              inStruct->row,
              inStruct->length,
              inStruct->wordWrap
             );
}

/**
 * Prints out the contents of a line struct
 */
void gdm_debug_line_struct(gdm_line *inStruct)
{
    fprintf_P(&usart_serial_stream, PSTR("Line Struct:\r\n"\
                                         "X1: %i\r\n"\
                                         "Y1: %i\r\n"\
                                         "X2: %i\r\n"\
                                         "Y2: %i\r\n"
                                        ),
              inStruct->x1,
              inStruct->y1,
              inStruct->x2,
              inStruct->y2
             );

    gdm_debug_active_state(inStruct->activeState);
}

/**
 * Prints out the contents of a rectangle struct
 */
void gdm_debug_rectangle_struct(gdm_rectangle *inStruct)
{
    fprintf_P(&usart_serial_stream, PSTR("Rectangle Struct:\r\n"\
                                         "X1: %i\r\n"\
                                         "Y1: %i\r\n"\
                                         "X2: %i\r\n"\
                                         "Y2: %i\r\n"\
                                         "Fill: %u\r\n"
                                        ),
              inStruct->x1,
              inStruct->y1,
              inStruct->x2,
              inStruct->y2,
              inStruct->filled
             );

    gdm_debug_active_state(inStruct->activeState);
}

/**
 * Prints the contents of an arc struct
 */
void gdm_debug_arc_struct(gdm_arc *inStruct)
{
    fprintf_P(&usart_serial_stream, PSTR("Arc Struct:\r\n"\
                                         "Center X: %i\r\n"\
                                         "Center Y: %i\r\n"\
                                         "Radius: %i\r\n"\
                                         "Start Angle: %i\r\n"\
                                         "Stop Angle: %i\r\n"
                                        ),
              inStruct->x_center,
              inStruct->y_center,
              inStruct->radius,
              inStruct->start_angle,
              inStruct->stop_angle
             );

    gdm_debug_active_state(inStruct->activeState);
}

/**
 * Prints the human understandable version the circle quadrant enum
 */
void gdm_debug_circle_quadrant(gdm_circle_quadrant inEnum)
{
    bool aQuadrantFound = false;

    fputs_P(PSTR("Circle Quadrants: "), &usart_serial_stream);

    if ((inEnum & gdm_quadrant_all) == gdm_quadrant_all) {
        if (inEnum & gdm_quadrant_ne) {
            fputs_P(PSTR("FULL"), &usart_serial_stream);
        }

        aQuadrantFound = true;

    } else {
        if (inEnum & gdm_quadrant_ne) {
            fputs_P(PSTR("NE"), &usart_serial_stream);
            aQuadrantFound = true;
        }

        if (inEnum & gdm_quadrant_se) {
            if (inEnum != gdm_quadrant_se) {
                fputs_P(PSTR(" | "), &usart_serial_stream);
            }
            fputs_P(PSTR("SE"), &usart_serial_stream);
            aQuadrantFound = true;
        }

        if (inEnum & gdm_quadrant_sw) {
            if (inEnum != gdm_quadrant_sw) {
                fputs_P(PSTR(" | "), &usart_serial_stream);
            }
            fputs_P(PSTR("SW"), &usart_serial_stream);
            aQuadrantFound = true;
        }

        if (inEnum & gdm_quadrant_nw) {
            if (inEnum != gdm_quadrant_se) {
                fputs_P(PSTR(" | "), &usart_serial_stream);
            }
            fputs_P(PSTR("NW"), &usart_serial_stream);
            aQuadrantFound = true;
        }
    }

    if (!aQuadrantFound) {
        fputs_P(PSTR("Empty"), &usart_serial_stream);
    }
    fputs_P(PSTR("\r\n"), &usart_serial_stream);
}

/**
 * Prints the contents of a circle struct
 */
void gdm_debug_circle_struct(gdm_circle *inStruct)
{
    fprintf_P(&usart_serial_stream, PSTR("Circle Struct:\r\n"\
                                         "Center X: %i\r\n"\
                                         "Center Y: %i\r\n"\
                                         "Radius: %i\r\n"\
                                         "Filled: %u\r\n"
                                        ),
              inStruct->x_center,
              inStruct->y_center,
              inStruct->radius,
              inStruct->filled
             );

    gdm_debug_circle_quadrant(inStruct->quadrants);

    gdm_debug_active_state(inStruct->activeState);
}

/**
 * Prints the contents of a bar graph struct
 */
void gdm_debug_bar_graph_struct(gdm_bar_graph *inStruct)
{
    fprintf_P(&usart_serial_stream, PSTR("Bar Graph Struct:\r\n"\
                                         "Coord X: %u\r\n"\
                                         "Coord Y: %u\r\n"\
                                         "Min: %f\r\n"\
                                         "Max: %f\r\n"\
                                         "Drawn?(*): %u\r\n"\
                                         "Border: %u\r\n"\
                                         "Last Pix(*): %u\r\n"\
                                         "Length: %u\r\n"\
                                         "Thick: %u\r\n"\
                                         "Orient: %u\r\n"\
                                         "Value: %f\r\n"\
                                        ),
              inStruct->coordX,
              inStruct->coordY,
              inStruct->min,
              inStruct->max,
              inStruct->alreadyDrawn,
              inStruct->drawBorder,
              inStruct->lastPixel,
              inStruct->length,
              inStruct->thickness,
              inStruct->orientation,
              inStruct->value);

    gdm_debug_active_state(inStruct->activeState);
}

/**
 * Prints the contents of a circle struct
 */
void gdm_debug_dial_struct(gdm_analog_dial *inStruct)
{
    fprintf_P(&usart_serial_stream, PSTR("Dial Struct:\r\n"\
                                         "Center X: %u\r\n"\
                                         "Center Y: %u\r\n"\
                                         "Min: %f\r\n"\
                                         "Max: %f\r\n"\
                                         "Radius: %u\r\n"\
                                         "Type: %i\r\n"\
                                         "Title: %s\r\n"\
                                         "Display Title: %u\r\n"\
                                         "Coarse Int: %f\r\n"\
                                         "Fine Int: %f\r\n"\
                                         "Display Coarse Values: %i\r\n"\
                                         "Value: %f\r\n"\
                                         "Last Value(*): %li\r\n"\
                                         "Drawn?(*): %u\r\n"\
                                         "Angle Start(*): %f\r\n"\
                                         "Angle Range(*): %f\r\n"\
                                        ),
              inStruct->centerX,
              inStruct->centerY,
              inStruct->min,
              inStruct->max,
              inStruct->radius,
              inStruct->dialType,
              inStruct->title,
              inStruct->displayTitle,
              inStruct->coarseInterval,
              inStruct->fineInterval,
              inStruct->displayCoarseValues,
              inStruct->value,
              inStruct->lastValue,
              inStruct->alreadyDrawn,
              inStruct->angleStart,
              inStruct->angleRange

             );

    gdm_debug_active_state(inStruct->activeState);
}

/**
 * Prints the display type as a string
 */
void gdm_debug_display_type(display_types inType)
{
    fputs_P(PSTR("Display type: "), &usart_serial_stream);
    switch (inType) {
    case display_type_text:
        fputs_P(PSTR("Text"), &usart_serial_stream);
        break;

    case display_type_line:
        fputs_P(PSTR("Line"), &usart_serial_stream);
        break;

    case display_type_rectangle:
        fputs_P(PSTR("Rectangle"), &usart_serial_stream);
        break;

    case display_type_arc:
        fputs_P(PSTR("Arc"), &usart_serial_stream);
        break;

    case display_type_circle:
        fputs_P(PSTR("Circle"), &usart_serial_stream);
        break;

    case display_type_gauge:
        fputs_P(PSTR("Analog Gauge"), &usart_serial_stream);
        break;

    case display_type_bar_graph:
        fputs_P(PSTR("Bar Graph"), &usart_serial_stream);
        break;

    default:
        fputs_P(PSTR("ERROR"), &usart_serial_stream);
        break;
    }
    fputs_P(PSTR("\r\n"), &usart_serial_stream);
}

/**
 * Calls the correct debug struct for the GDM item in the given display_struct
 *
 * @param inStruct  Struct whose contents are to be printed
 */
void gdm_debug_display_struct(display_struct *inStruct)
{

    gdm_debug_display_type(inStruct->type);

    fputs_P(PSTR("Display struct: "), &usart_serial_stream);
    switch (inStruct->type) {
    case display_type_text:
        gdm_debug_text_struct(&(inStruct->text));
        break;

    case display_type_line:
        gdm_debug_line_struct(&(inStruct->line));
        break;

    case display_type_rectangle:
        gdm_debug_rectangle_struct(&(inStruct->rectangle));
        break;

    case display_type_arc:
        gdm_debug_arc_struct(&(inStruct->arc));
        break;

    case display_type_circle:
        gdm_debug_circle_struct(&(inStruct->circle));
        break;

    case display_type_gauge:
        gdm_debug_dial_struct(&(inStruct->dial));
        break;

    case display_type_bar_graph:
        gdm_debug_bar_graph_struct(&(inStruct->graph));
        break;

    default:
        fputs_P(PSTR("ERROR"), &usart_serial_stream);
        break;
    }

    fputs_P(PSTR("\r\n"), &usart_serial_stream);

}