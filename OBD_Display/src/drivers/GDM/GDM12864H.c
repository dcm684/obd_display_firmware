/*
 * GDM12864H.c
 *
 * Based on Chris Kern's AVR code for GDM12864H
 *
 * Created: 7/30/2012 9:25:15 PM
 *  Author: Meyer
 */
#include <avr/io.h>
#include <ioport.h>

#include "Pin_Defines.h"
#include "GDM12864H.h"

/**
 * Initializes and turns on the LCD display ready to output
 */
void gdm_init(void)
{
    ioport_configure_pin(LCD_ENABLE, IOPORT_DIR_OUTPUT | IOPORT_INIT_HIGH);
    ioport_configure_pin(LCD_DATA_INST, IOPORT_DIR_OUTPUT | IOPORT_INIT_LOW);
    ioport_configure_pin(LCD_RW, IOPORT_DIR_OUTPUT | IOPORT_INIT_LOW);
    ioport_configure_pin(LCD_RESET, IOPORT_DIR_OUTPUT | IOPORT_INIT_LOW);
    ioport_configure_pin(LCD_CS_1, IOPORT_DIR_OUTPUT | IOPORT_INIT_LOW);
    ioport_configure_pin(LCD_CS_2, IOPORT_DIR_OUTPUT | IOPORT_INIT_LOW);
    ioport_configure_pin(LCD_TRANSLATE_DIR, IOPORT_DIR_OUTPUT | IOPORT_INIT_LOW);

    ioport_configure_group(LCD_DATA_PORT, 0xFF,
                           IOPORT_DIR_OUTPUT | IOPORT_INIT_LOW);

    /* Turn the display on */
    gdm_display_onoff(GDM_CHIP_1, true);
    gdm_display_onoff(GDM_CHIP_2, true);

    /* Set the first line to display on to the top row */
    gdm_set_display_start_line(GDM_CHIP_1, 0);
    gdm_set_display_start_line(GDM_CHIP_2, 0);

    gdm_clear_display();
}

/**
 * Turns off the LCD display
 *
 * /param turnOn Set if you want to turn on the display, clear if you want to
 *  turn it off
 */
void gdm_display_onoff(gdm_chip chip, bool turnOn)
{
    if(turnOn) {
        gdm_write_data(chip, GDM_LCD_INST, GDM_DISPLAY_ON);

    } else {
        gdm_write_data(chip, GDM_LCD_INST, GDM_DISPLAY_OFF);
    }
}

/**
 * Sets the line in RAM to display at the top of the screen
 *
 * /param chip Which of the two chips to set the value for
 * /param address Line to display at the top. Valid numbers are 0-63.
 *
 * /returns True if the set was successful. Only returns false when an
 *  invalid address is given
 */
bool gdm_set_display_start_line(gdm_chip chip, uint8_t address)
{
    if(address > 63) {
        return false;
    }

    gdm_write_data(chip, GDM_LCD_INST, (0b11000000 | address));
    return true;
}

/**
 * Clears the entire LCD
 */
void gdm_clear_display(void)
{
    uint8_t x;
    uint8_t y;
    uint8_t i;
    uint8_t chip;

    for(i = 0; i < 2; i++) {
        if(i == 0) {
            chip = GDM_CHIP_1;

        } else {
            chip = GDM_CHIP_2;
        }

        /* In example code it was ++x and ++y */
        for(x = 0; x < 8; x++) {
            gdm_write_data(chip, GDM_LCD_INST, GDM_ADDR_Y(0));
            gdm_write_data(chip, GDM_LCD_INST, GDM_ADDR_X(x));

            for(y = 0; y < GDM_WIDTH_SINGLE; y++) {
                gdm_write_data(chip, GDM_LCD_DATA, 0);
            }
        }
    }

    gdm_active_cache.chip = GDM_CACHE_EMPTY;
}

/**
 * Sends the cached data to the LCD
 */
void gdm_flush_cache(void)
{
    if(gdm_active_cache.chip == GDM_CACHE_EMPTY) {
        /* Nothing to send here, move along */
        return;
    }

    gdm_write_data(gdm_active_cache.chip, GDM_LCD_INST, GDM_ADDR_X(gdm_active_cache.page));
    gdm_write_data(gdm_active_cache.chip, GDM_LCD_INST, GDM_ADDR_Y(gdm_active_cache.column));
    gdm_write_data(gdm_active_cache.chip, GDM_LCD_DATA, gdm_active_cache.data);

    gdm_active_cache.chip = GDM_CACHE_EMPTY;
}

/**
 * Loads the current data for the given page on from the LCD.
 *
 * If the same page is in memory, the function returns, otherwise the
 * cache will be sent to the display and then the desired page will be
 * loaded to the cache.
 *
 * Because the memory is organized in vertical 8-bit pages, it is more
 * efficient to put the column loop on the outside and the row loop inside
 *
 * /def chip Chip whose data is to be read
 * /def page Byte sized page the data to be read
 * /def clmn Column number of the data to be read
 * /def flush Send data to be displayed?
 */
void gdm_load_page(gdm_chip chip, uint8_t page, uint8_t clmn, bool flush)
{
    /* If the most recently read data in the cache is for the same
    chip and address, why read it again? Don't then ! */
    if((chip == gdm_active_cache.chip) &&
            (page == gdm_active_cache.page) &&
            (clmn == gdm_active_cache.column)) {

        return;
    }

    /* Send out the data currently in the cache */
    if(flush) {
        gdm_flush_cache();
    }

    gdm_active_cache.page = page;
    gdm_active_cache.column = clmn;
    gdm_active_cache.chip = chip;

    gdm_write_data(chip, GDM_LCD_INST, GDM_ADDR_X(page));
    gdm_write_data(chip, GDM_LCD_INST, GDM_ADDR_Y(clmn));

    /* You need to read it twice since there's a pipeline and you get
    old data the first read */
    gdm_read_data(chip, GDM_LCD_DATA);
    gdm_active_cache.data = gdm_read_data(chip, GDM_LCD_DATA);
}

/**
 * Set the given pixel to the given level.
 *
 * Because of how gdm_load_page works, the last time that this is called
 * gdm_flush_cache must be called afterwards to get the data on to
 * the screen.
 *
 * The fastest way to display data is to work vertically first and slowly
 * move horizontally.
 *
 * Coordinates are based off of the entire screen.
 *
 * /param x X-coordinates of pixel to set
 * /param y Y-coordinates of pixel to set
 * /param level Level to set the pixel to
 */
void gdm_set_pixel_level(uint8_t x, uint8_t y, pixel_state state)
{
    uint8_t outChip;
    uint8_t page;
    uint8_t bitToSwitch;

    /* Since this deals with the entire pixel range, the chip that contains
    the pixel must be determined and the actual x-coordinate must be
    calculated. Any pixels out side of the x-range will not be adjusted */
    if(x < GDM_WIDTH_SINGLE) {
        outChip = GDM_CHIP_1;

    } else if(x < GDM_WIDTH_FULL) {
        outChip = GDM_CHIP_2;

    } else {
        return;
    }

    /* Pixels out side of the y-range won't be tolerated either */
    if(y >= GDM_HEIGHT_FULL) {
        return;
    }

    /* Calculate the 8-bit page and bit to modify. The axis on the module
     * are swapped. */
    page = (y & 0b00111111) >> 3;
    bitToSwitch = (y & 0b00000111);

    gdm_load_page(outChip, page, (x & 0x3F), true);

    if(state == pixel_on) {
        gdm_active_cache.data |= _BV(bitToSwitch);

    } else if(state == pixel_off) {
        gdm_active_cache.data &= ~_BV(bitToSwitch);
    }

    if(state == pixel_invert) {
        gdm_active_cache.data ^= _BV(bitToSwitch);
    }
}

/**
 * Sets the given page to the given value
 *
 * /param page
 */

/**
 * Waits for the display to not be busy
 */
void gdm_wait(gdm_chip chip)
{
    /* Just keep looping while its busy */
    while((gdm_read_data(chip, GDM_LCD_INST) &
            (_BV(GDM_STATUS_READY) | _BV(GDM_STATUS_RESET))) != 0) {
    }
}

/**
 * Reads a byte from the given register
 *
 * /param address Address of register to read
 *
 * /returns Data that was read
 */
uint8_t gdm_read_data(gdm_chip chip, uint8_t type)
{
    uint8_t retVal;

    if(chip == GDM_CHIP_1) {
        ioport_set_pin_low(LCD_CS_2);
        ioport_set_pin_high(LCD_CS_1);

    } else if(chip == GDM_CHIP_2) {
        ioport_set_pin_low(LCD_CS_1);
        ioport_set_pin_high(LCD_CS_2);

    } else {
        return 0x00;
    }

    ioport_configure_group(LCD_DATA_PORT, 0xFF, IOPORT_DIR_INPUT);

    /* Data from LCD to uC */
    ioport_set_pin_level(LCD_TRANSLATE_DIR, GDM_TRANSLATE_FROM_LCD);
    ioport_set_pin_level(LCD_RW, GDM_RW_READ);
    ioport_set_pin_level(LCD_DATA_INST, type);

    GDM_DELAY;
    ioport_set_pin_high(LCD_ENABLE);
    GDM_DELAY;
    retVal = ioport_get_port_level(LCD_DATA_PORT, 0xFF);
    ioport_set_pin_low(LCD_ENABLE);

    /* Data from uC to LCD*/
    ioport_configure_group(LCD_DATA_PORT, 0xFF, IOPORT_DIR_OUTPUT);
    ioport_set_pin_level(LCD_TRANSLATE_DIR, GDM_TRANSLATE_TO_LCD);

    return retVal;
}

/**
 * Sends a byte of data to the display and waits for the function to be
 * completed before returning.
 *
 * /param outByte Data to be written
 */
void gdm_write_data(gdm_chip chip, uint8_t type, uint8_t outByte)
{
    if(chip == GDM_CHIP_1) {
        ioport_set_pin_low(LCD_CS_2);
        ioport_set_pin_high(LCD_CS_1);

    } else if(chip == GDM_CHIP_2) {
        ioport_set_pin_low(LCD_CS_1);
        ioport_set_pin_high(LCD_CS_2);

    } else {
        return;
    }

    ioport_configure_group(LCD_DATA_PORT, 0xFF, IOPORT_DIR_OUTPUT);

    /* Data from uC to LCD*/
    ioport_set_pin_level(LCD_TRANSLATE_DIR, GDM_TRANSLATE_TO_LCD);
    ioport_set_pin_level(LCD_RW, GDM_RW_WRITE);
    ioport_set_pin_level(LCD_DATA_INST, type);

    ioport_set_port_level(LCD_DATA_PORT, 0xFF, outByte);

    GDM_DELAY;
    ioport_set_pin_high(LCD_ENABLE);
    GDM_DELAY;
    ioport_set_pin_low(LCD_ENABLE);

    gdm_wait(chip);
}

/**
 * Scrolls the current display up by the given number of pixel rows
 *
 * /param rows Number of rows to scroll up
 */
void gdm_scroll_screen(uint8_t rows)
{
    uint8_t x, y;
    gdm_cache readCache;
    gdm_chip currChip;
    uint8_t currPage;
    uint8_t currCol;
    pixel_state outState;

    /* Check if the number of rows to scroll up is greater than the number
    if rows available. If it is clear the display. */
    if(rows >= GDM_HEIGHT_FULL) {
        gdm_clear_display();
        return;
    }

    /* Force the GDM cache to be read initially */
    readCache.chip = GDM_CACHE_EMPTY;
    readCache.page = 0xFF;
    readCache.column = 0xFF;

    /* Starting from the top, read the pixels the given number of rows
    down. Two pages, one for in and one for out, are needed because the
    pages used might not be lined up */
    for(x = 0; x < GDM_WIDTH_FULL; x++) {
        if(x < GDM_WIDTH_SINGLE) {
            currChip = GDM_CHIP_1;

        } else {
            currChip = GDM_CHIP_2;
        }

        currCol = x & 0x3F;

        for(y = 0; y < GDM_HEIGHT_FULL; y++) {
            /* Once all of the pixels have been scrolled, clear the
            remaining rows */
            if(y >= GDM_HEIGHT_FULL - rows) {
                gdm_set_pixel_level(x, y, pixel_off);

            } else {
                currPage =
                    ((y + (rows)) & 0b00111111) >> 3;


                /* Load the current page from the display if it is a new
                page or it is the first page of the display (holds pixel
                (0,0))*/
                if(((x == 0) && (y == 0)) ||
                        (currChip != readCache.chip) ||
                        (currCol != readCache.column) ||
                        (currPage != readCache.page)) {

                    gdm_write_data(currChip, GDM_LCD_INST, GDM_ADDR_X(currPage));
                    gdm_write_data(currChip, GDM_LCD_INST, GDM_ADDR_Y(currCol));

                    /* You need to read it twice since there's a pipeline
                    and you get old data the first read */
                    gdm_read_data(currChip, GDM_LCD_DATA);
                    readCache.data = gdm_read_data(currChip, GDM_LCD_DATA);

                    readCache.chip = currChip;
                    readCache.column = currCol;
                    readCache.page = currPage;
                }

                if(readCache.data & _BV((y + (rows)) & (0b00000111))) {
                    outState = pixel_on;

                } else {
                    outState = pixel_off;
                }

                gdm_set_pixel_level(x, y, outState);

                /* If the row shift is small, both caches might be of the
                same page. */
                /*if((readCache.chip == gdm_active_cache.chip) &
                	(readCache.column == gdm_active_cache.column) &
                	(readCache.page == gdm_active_cache.page)) {

                }*/
            }
        }
    }
}

/**
 * Returns the opposite pixel state for the given pixel state,
 * if the given pixel state is pixel_on then pixel_off is returned,
 * if pixel_invert is given pixel_nochange is returned
 *
 * /param inState State whose opposite is desired
 *
 * /returns Opposite state of the given state
 */
pixel_state gdm_opposite_pixel_state(pixel_state inState)
{
    switch(inState) {
    case pixel_off:
        return pixel_on;

    case pixel_on:
        return pixel_off;

    case pixel_invert:
        return pixel_nochange;

    case pixel_nochange:
        return pixel_invert;

    default:
        return pixel_off;
    }
}