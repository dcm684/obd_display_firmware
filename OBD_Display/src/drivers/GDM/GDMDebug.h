/*
 * GDMDebug.h
 *
 * Created: 5/8/2014 9:49:15 PM
 *  Author: Meyer
 */


#ifndef GDMDEBUG_H_
#define GDMDEBUG_H_

#include "GDM12864H.h"
#include "GDM12864H_Draw.h"
#include "Display.h"

void gdm_debug_active_state(pixel_state inState);
void gdm_debug_text_struct(gdm_text *inStruct);
void gdm_debug_line_struct(gdm_line *inStruct);
void gdm_debug_rectangle_struct(gdm_rectangle *inStruct);
void gdm_debug_arc_struct(gdm_arc *inStruct);
void gdm_debug_circle_struct(gdm_circle *inStruct);
void gdm_debug_bar_graph_struct(gdm_bar_graph *inStruct);
void gdm_debug_dial_struct(gdm_analog_dial *inStruct);
void gdm_debug_circle_quadrant(gdm_circle_quadrant inEnum);

void gdm_debug_display_type(display_types inType);
void gdm_debug_display_struct(display_struct *inStruct);
#endif /* GDMDEBUG_H_ */