/*
 * GDM12864H_Text.c
 *
 * Based on Chris Kern's code for GDM12864H
 *
 * Created: 8/1/2012
 *  Author: Meyer
 */
#include <progmem.h>
#include <stdio.h>

#include "GDM12864H.h"
#include "GDM12864H_Text.h"

#include "fonts/font_5x7.h"
#include <stdlib.h>
#include "../USART_Handler.h"

FILE gdm_serial_stream =
    FDEV_SETUP_STREAM(gdm_putchar, NULL, _FDEV_SETUP_WRITE);

/**
 * Initializes the default font
 */
void gdm_font_init()
{
    activeFont = malloc(sizeof(gdm_font));
    gdm_load_font(font_5x7_char_height, font_5x7_char_width,
                  font_5x7_charArray, font_5x7_char_spacing,
                  font_5x7_line_spacing, pixel_on);
}

/**
 * Loads the given font as the current font
 *
 * /param font_height Font's height in pixels
 * /param font_width Font's width in pixels
 * /param fontData Array of bitmaps for each character
 * /param char_spacing Pixels per space between characters in the same row
 * /param line_spacing Pixels between lines of text
 * /param active_state State of the text's pixels when active
 */
void gdm_load_font( uint8_t font_height,
                    uint8_t font_width,
                    const uint8_t fontData[],
                    uint8_t char_spacing,
                    uint8_t line_spacing,
                    pixel_state active_state )
{

    activeFont->char_height = font_height;
    activeFont->char_width = font_width;
    activeFont->charArray = fontData;

    activeFont->char_spacing = char_spacing;
    activeFont->line_spacing = line_spacing;

    activeFont->activeState = active_state;
}

/**
 * Moves the cursor to the given position
 *
 * /param row Row to place the cursor in
 * /param col Column to place the cursor in
 */
void gdm_move_cursor(uint8_t row, uint8_t col)
{
    gdm_cursor_x = col * (activeFont -> char_width +
                          activeFont->char_spacing);
    gdm_cursor_y = row * (activeFont -> char_height +
                          activeFont->line_spacing);
}

/**
 * Prints the given character and increments the cursor
 *
 * /param inChar Character to print on display
 */
int gdm_putchar(char inChar, void *unused)
{
    uint8_t x;
    uint8_t y;
    uint8_t currCharPixelRow;
    const uint8_t *pInChar;

    if(inChar == '\r') {
        gdm_cursor_x = 0;

    } else if(inChar == '\n') {
        if(GDM_HEIGHT_FULL < (gdm_cursor_y + 2 * activeFont->char_height)) {
            GDM_text_scroll_rows(1, activeFont->char_height + activeFont->line_spacing);

        } else {
            gdm_cursor_y += (activeFont -> char_height +
                             activeFont->line_spacing);
        }

    } else if(inChar == '\t') {
        /* Move to the cursor to the next tab space. One space is
        guaranteed. */
        gdm_putchar(' ', unused);

        while((gdm_cursor_x /
                (activeFont->char_width + activeFont->char_spacing)) %
                GDM_TAB_SIZE != 0) {

            gdm_putchar(' ', unused);
        }

    } else if((inChar >= 32) && (inChar < 127)) {
        /* If it is a printable character... */
        if(GDM_WIDTH_FULL < gdm_cursor_x + activeFont->char_width) {
            /* If this character cannot fit on the screen wrap around to
            the start of the next line */
            gdm_cursor_x = 0;

            /* If the new line is out of the viewable region scroll the
            view */
            if(GDM_HEIGHT_FULL < (gdm_cursor_y + 2 * activeFont->char_height)) {
                GDM_text_scroll_rows(1, activeFont->char_height + activeFont->line_spacing);

            } else {
                gdm_cursor_y += (activeFont -> char_height +
                                 activeFont->line_spacing);
            }
        }

        /* Calculate the pointer to what character to print */
        pInChar = (activeFont->charArray) +
                  (activeFont -> char_width) * (inChar - 32);

        pixel_state outState;
        pixel_state oppositeState;

        oppositeState = gdm_opposite_pixel_state(activeFont->activeState);

        /* Set and clear the appropriate pixels for the requested character */
        for(x = 0; x < (activeFont->char_width) + (activeFont->char_spacing); x++) {
            currCharPixelRow = pgm_read_byte(pInChar + x);

            for(y = 0; y < (activeFont->char_height) + 1; y++) {

                if((x < (activeFont->char_width)) &&
                        (y < (activeFont->char_height))) {

                    if(currCharPixelRow & _BV(y)) {
                        outState = activeFont->activeState;

                    } else {
                        outState = oppositeState;
                    }

                    gdm_set_pixel_level(
                        gdm_cursor_x + x,
                        gdm_cursor_y + y,
                        outState);

                } else {
                    gdm_set_pixel_level(
                        gdm_cursor_x + x,
                        gdm_cursor_y + y,
                        oppositeState);
                }
            }
        }
        /* Advance the cursor */
        gdm_cursor_x += (activeFont->char_width + activeFont->char_spacing);
        gdm_flush_cache();
    }

    return 0; /* Done for printf */
}

/**
 * Prints a string of characters
 *
 * /param inStr
 */
void gdm_puts(const char *inStr)
{
    while(*inStr != '\0') {
        gdm_putchar(*inStr, 0);
        inStr++;
    }
}

/**
 * Prints a string of characters that are stored in program memory
 *
 * /param inStr
 */
void gdm_puts_P(const char *inStr)
{
    while(pgm_read_byte(inStr) != '\0') {
        gdm_putchar(pgm_read_byte(inStr), 0);
        inStr++;
    }
}

/**
 * Sets all of the pixels in a single row of text to the same state
 *
 * /param row Row to clear of text
 * /param value State to set the text row pixels to
 */
void gdm_text_set_row_value(uint8_t row, pixel_state value)
{
    uint8_t x;
    uint8_t y;

    for(x = 0; x < GDM_WIDTH_FULL; x++) {
        for(y = gdm_cursor_y; y < gdm_cursor_y + activeFont->char_height; y++) {
            gdm_set_pixel_level(x, y, value);
        }
    }

    gdm_flush_cache();
}

/**
 * Clears the current row of text where the cursor resides
 *
 * Does not move the cursor
 */
void gdm_text_set_current_row_value(pixel_state value)
{
    gdm_text_set_row_value(gdm_cursor_y /
                           (activeFont->char_height + activeFont->char_spacing), value);
}

/**
 * Sets the active font state for all characters printed after call
 *
 * /param state Sets the font to be on, off, or inverted from the background
 */
void gdm_text_set_state(pixel_state inState)
{
    activeFont->activeState = inState;
}

/**
 * Prints the contents of the given struct on the GDM where the struct says
 *
 * /param	inStruct	Structure containing the location data and text
 */
void gdm_text_print_struct(gdm_text *inStruct)
{
    uint8_t charsToPrint;
    uint8_t initCharsToPrint;
    uint8_t i;
    char *string;
    uint8_t stringPixelLength;

    gdm_move_cursor(inStruct->row, inStruct->column);

    charsToPrint = inStruct->length;

    /* If the number of characters to print, search for a null character.
     * Assume that the most number of characters that can be display is the
     * number of pixels on the display */
    if(charsToPrint == 0) {
        string = inStruct->string;
        while((*string != '\0') && (charsToPrint < GDM_HEIGHT_FULL * GDM_WIDTH_FULL)) {
            string++;
            charsToPrint++;
        }
        charsToPrint++;
    }

    /* If word wrap is not enabled determine how many characters to print
     * before hitting the end point */
    if(!inStruct->wordWrap) {
        stringPixelLength = (inStruct->font->char_width + inStruct->font->char_spacing) * (charsToPrint - 1);
        /*
        fprintf(&usart_serial_stream, "WW: %u %u (%u %u)", inStruct->wordWrap, charsToPrint,
        	stringPixelLength,
        	GDM_WIDTH_FULL - gdm_cursor_x);
        */
    }
    if(!(inStruct->wordWrap)) {
        /* If all of the characters cannot fit on the current line
         * (excluding the space after the final character), determine
         * the number of characters that will fit */
        if(GDM_WIDTH_FULL < gdm_cursor_x + stringPixelLength - inStruct->font->char_spacing) {
            initCharsToPrint = charsToPrint;
            /* Determine how many characters can be printed before hitting
             * the end of the line The space at the after the last character
             * can be ignored in the calculation */
            charsToPrint = (GDM_WIDTH_FULL - gdm_cursor_x + inStruct->font->char_spacing)
                           / (inStruct->font->char_width + inStruct->font->char_spacing);

            //fprintf(&usart_serial_stream, " %u\r\n", charsToPrint);

            if(charsToPrint > initCharsToPrint) {
                charsToPrint = initCharsToPrint;
            }
        }
    }

    /* Load the font */
    activeFont = inStruct->font;

    /* Print the string on the display stopping when the string ends or
    when there is no more space for it. */
    string = inStruct->string;

    for(i = 0; i < charsToPrint; i++) {
        if(*string == '\0') {
            break;
        }
        gdm_putchar(*string, 0);
        string++;
    }

}