/*
 * GDM12864H.h
 *
 * Created: 7/30/2012 9:25:05 PM
 *  Author: Meyer
 */


#ifndef GDM12864H_H_
#define GDM12864H_H_

#include <inttypes.h>
#include <stdbool.h>
//#include <avr/pgmspace.h>

/**
 * GDM_RW_READ It is a read operation
 */
#define GDM_RW_READ true
/**
 * GDM_RW_WRITE It is a write operation
 */
#define GDM_RW_WRITE false

/**
 * GDM_TRANSLATE_FROM_LCD Data is coming from the LCD to the microcontroller
 */
#define GDM_TRANSLATE_FROM_LCD 1
/**
 * GDM_TRANSLATE_TO_LCD Data is going to the LCD from the microcontroller
 */
#define GDM_TRANSLATE_TO_LCD 0

/**
 * /define GDM_LCD_DATA The operation deals with data
 */
#define GDM_LCD_DATA true
/**
 * /define GDM_LCD_DATA The operation is an instruction
 */
#define GDM_LCD_INST false

/**
 * /define GDM_ADDR_X Set the page or x address
 */
#define GDM_ADDR_X(X) ((X & 0b00000111) | 0b10111000)
/**
 * /define GDM_ADDR_Y Set the y address
 */
#define GDM_ADDR_Y(Y) ((Y & 0b00111111) | 0b01000000)

/**
 * /define GDM_DELAY Delay to allow data to be clocked through LCD
 */
#define GDM_DELAY asm volatile("nop\n\t" \
							   "nop\n\t" \
							   "nop\n\t" \
							   "nop\n\t")//delay_us(0.450)(4 NOPS)

/**
 * /def GDM_DISPLAY_ON Value of data byte for turning on display
 */
#define GDM_DISPLAY_ON 0x3F

/**
 * /def GDM_DISPLAY_OFF Value of data byte for turning on display
 */
#define GDM_DISPLAY_OFF 0x3E

/**
 * /define GDM_STATUS_READY Bit in the status byte that indicates whether the
 *	LCD is ready for input. Active low
 */
#define GDM_STATUS_READY 7
/**
 * /define GDM_STATUS_ON Bit in the status byte that indicates whether the
 *	LCD is on. Active low.
 */
#define GDM_STATUS_ON 5
/**
 * /define GDM_STATUS_RESET Bit in the status byte that indicates whether the
 *	LCD was reset. Active high.
 */
#define GDM_STATUS_RESET 4

/**
 * /def GDM_WIDTH_SINGLE Width in number of pixels for a single chip
 */
#define GDM_WIDTH_SINGLE 64

/**
 * /def GDM_WIDTH_FULL Width in number of pixels for the entire display
 */
#define GDM_WIDTH_FULL 128

/**
 * /def GDM_HEIGHT Height in number of pixels for the whole display
 */
#define GDM_HEIGHT_FULL 64

typedef enum {
    GDM_CHIP_1,
    GDM_CHIP_2,
    GDM_CACHE_EMPTY = 0xFF	/*! Indicates that there is no pending data to
								be sent for any chip and the cache is
								empty. */
} gdm_chip;

typedef struct {
    gdm_chip chip;	/*<The chip with data pending in the cache */
    uint8_t page;	/*<Page of data in the cache*/
    uint8_t column; /*<Column on the page in cache */
    uint8_t data;	/*<Currently cached data */
} gdm_cache;

typedef enum {
    pixel_off		= 0,
    pixel_on		= 1,
    pixel_invert	= 2,
    pixel_nochange	= 3
} pixel_state;

gdm_cache gdm_active_cache;

void gdm_init(void);
void gdm_display_onoff(gdm_chip chip, bool turnOn);
bool gdm_set_display_start_line(gdm_chip chip, uint8_t address);
void gdm_clear_display(void);
void gdm_flush_cache(void);
void gdm_load_page(gdm_chip chip, uint8_t page, uint8_t clmn, bool flush);
void gdm_set_pixel_level(uint8_t x, uint8_t y, pixel_state state);
void gdm_wait(gdm_chip chip);
uint8_t gdm_read_data(gdm_chip chip, uint8_t type);
void gdm_write_data(gdm_chip chip, uint8_t type, uint8_t outByte);
void gdm_scroll_screen(uint8_t rows);
pixel_state gdm_opposite_pixel_state(pixel_state inState);
#endif /* GDM12864H_H_ */