/*
 * GDM12864H_Draw.c
 *
 * Created: 8/3/2012 2:50:53 PM
 *  Author: Meyer
 */
#include <math.h>

#include <stdbool.h>

#include <math.h>

#include "GDM12864H.h"
#include "GDM12864H_Draw.h"

//#include "..\USART_Handler.h"

/**
 * Draw a line between the two given points.
 *
 * While negative values are accepted for the points, all
 * negative and other points outside of the displays dimensions
 * will not be drawn.
 *
 * /param x1 X-coordinate of the first point
 * /param y1 Y-coordinate of the first point
 * /param x2 X-coordinate of the second point
 * /param y2 Y-coordinate of the second point
 * /param activeState	Active pixel state
 *
 */
void gdm_draw_line(int16_t x1, int16_t y1, int16_t x2, int16_t y2,
                   pixel_state activeState)
{
    int16_t i, j;
    int16_t calcY;
    int16_t lastY;

    int16_t lowX;
    int16_t highX;
    int16_t lowXY;
    int16_t highXY;

    /* Draw one page for all y addresses before changing pages since
    y addresses automatically increment */
    if(x1 < x2) {
        lowX = x1;
        lowXY = y1;
        highX = x2;
        highXY = y2;

    } else {
        lowX = x2;
        lowXY = y2;
        highX = x1;
        highXY = y1;
    }

    lastY = lowXY;

    /* Draws a vertical line */
    if(x1 == x2) {
        if(y1 < y2) {
            for(i = y1; i <= y2; i++) {
                gdm_set_pixel_level(x1, i, activeState);
            }

        } else {
            for(i = y2; i <= y1; i++) {
                gdm_set_pixel_level(x1, i, activeState);
            }
        }

    } else {
        /* Draw horizontal or diagonal lines */
        for(i = lowX; i <= highX; i++) {
            /* Calculate the corresponding Y-value for the given X */
            calcY = (highXY - lowXY) * (i - lowX) / (highX - lowX) + lowXY;


            /* Don't waste your time with points outside of the display's
            range */
            if(i >= GDM_WIDTH_FULL) {
                break;

            } else if(calcY < GDM_HEIGHT_FULL) {
                gdm_set_pixel_level(i, calcY, activeState);

                /* Draw all of the points between the last calculated Y
                and the just calculated Y */
                if(lowXY < highXY) {
                    for(j = lastY; j < calcY; j++) {
                        gdm_set_pixel_level(i - 1, j, activeState);
                    }

                } else {
                    for(j = lastY; j > calcY; j--) {
                        gdm_set_pixel_level(i - 1, j, activeState);
                    }
                }
            }

            lastY = calcY;
        }
    }

    gdm_flush_cache();
}

/**
 * Draw a line between the two points in the given struct
 *
 * While negative values are accepted for the points, all
 * negative and other points outside of the displays dimensions
 * will not be drawn.
 *
 * /param inStruct		Struct containing the coordinates of the start and
 *						endpoints
 */
void gdm_draw_line_struct(gdm_line *inStruct)
{
    gdm_draw_line(inStruct->x1, inStruct->y1,
                  inStruct->x2, inStruct->y2,
                  inStruct->activeState);
}

/**
 * Draw a rectangle with two corners at the given points.
 *
 * While negative values are accepted for the corners, all
 * negative and other points outside of the displays dimensions
 * will not be drawn.
 *
 * /param x1 X-coordinate of corner one of the rectangle
 * /param y1 Y-coordinate of corner one of the rectangle
 * /param x2 X-coordinate of corner two of the rectangle
 * /param y2 Y-coordinate of corner two of the rectangle
 * /param activeState	Active pixel state
 *
 */
void gdm_draw_rectangle(int16_t x1, int16_t y1, int16_t x2, int16_t y2,
                        bool filled, pixel_state activeState)
{
    /* If it is not to be filled just draw 4 lines */
    if(!filled) {
        gdm_draw_line(x1, y1, x1, y2, activeState);
        gdm_draw_line(x1, y2, x2, y2, activeState);
        gdm_draw_line(x2, y2, x2, y1, activeState);
        gdm_draw_line(x2, y1, x1, y1, activeState);
        return;
    }

    int16_t i;
    int16_t j;

    int16_t lowX;
    int16_t highX;
    int16_t lowXY;
    int16_t highXY;

    if(x1 < x2) {
        lowX = x1;
        lowXY = y1;
        highX = x2;
        highXY = y2;

    } else {
        lowX = x2;
        lowXY = y2;
        highX = x1;
        highXY = y1;
    }

    for(i = lowX; i <= highX; i++) {
        for(j = lowXY; j <= highXY; j++) {
            gdm_set_pixel_level(
                i,
                j,
                activeState);
        }
    }
}

/**
 * Draw a rectangle with two corners at the given points.
 *
 * While negative values are accepted for the corners, all
 * negative and other points outside of the displays dimensions
 * will not be drawn.
 *
 * /param inStruct	Struct containing the two corners of the rectangle
 *
 */
void gdm_draw_rectangle_struct(	gdm_rectangle *inStruct)
{
    gdm_draw_rectangle(inStruct->x1, inStruct->y1,
                       inStruct->x2, inStruct->y2,
                       inStruct->filled, inStruct->activeState);
}

/**
 * Go thru Y-Values (radius to 0) and calc angles from Ys and radius
 * If angle is in range calc X. Pythag
 *
 * /param center_x		X-value of center point
 * /param center_y		Y-value of center point
 * /param radius		If the arc was a circle this would be the radius
 * /param startAngle	Angle (degrees) where the arc starts.
						0 degrees is 3 o'clock
 * /param stopAngle		Angle (degrees) where the arc stops.
						0 degrees is 3 o'clock
 * /param activeState	The pixel state of the arc
 */
void gdm_draw_arc(	int16_t center_x, int16_t center_y,
                    uint8_t radius,
                    int16_t startAngle, int16_t stopAngle,
                    pixel_state activeState)
{
    float x;
    float y;

    float last_x;
    float last_y;
    double resolution;


    double angle;
    double startAngleFloat;
    double stopAngleFloat;

    /* Don't try to draw a circle that has no radius */
    if(radius == 0) {
        return;
    }

    /* If there's more than 360 deg difference, draw a full circle */
    if((stopAngle - startAngle > 360) ||
            (startAngle - stopAngle > 360)) {
        startAngle = 0;
        stopAngle = 360;
    }

#if UNUSED
    bool lineRightOfStart;

    if(stopAngle > startAngle) {
        lineRightOfStart = true;

    } else if(stopAngle < startAngle) {
        lineRightOfStart = false;

    } else {
        /* There's no arc to draw */
        return;
    }

    /* Make sure the angles are between 0 and 360 degrees */
    while(startAngle < 0) {
        startAngle = startAngle + 360;
    }

    while(startAngle > 360) {
        startAngle = startAngle - 360;
    }

    while(stopAngle < 0) {
        stopAngle = stopAngle + 360;
    }

    while(stopAngle > 360) {
        stopAngle = stopAngle - 360;
    }

#endif

    /* Convert to Radians */
    startAngleFloat = startAngle * M_PI / 180;
    stopAngleFloat = stopAngle * M_PI / 180;

    /*	resolution = totalAngle / arcLength
    	resolution = (totalAngle) / (2 * radius * pi * (totalAngle) / 360)
    	resolution = 1 / (2 * radius * pi / 360);
    	resolution = 360 / (2 * radius * pi);
    	resolution = 180 / (radius * pi);
    	Convert to radians
    	resolution = 180 / (radius * pi) * (pi / 180);
    	resolution = 1 / radius
    */
    resolution = radius;
    resolution = 1 / resolution;

    last_x = -1;
    last_y = -1;

    /*if(!lineRightOfStart) {
    	stopAngleFloat += 2 * M_PI;
    }	*/
    while(stopAngleFloat < startAngleFloat) {
        stopAngleFloat += 2 * M_PI;
    }

    for(angle = startAngleFloat; angle <= stopAngleFloat; angle += resolution) {
        x = center_x + radius * cos(angle);
        y = center_y + radius * sin(angle);

        x = round(x);
        y = round(y);

        if((x != last_x) || (y != last_y)) {
            //fprintf_P(&usart_serial_stream, PSTR("%u %u %f\r\n"), (int) x, (int) y, resolution);
            gdm_set_pixel_level((int) x, (int) y, activeState);

            last_x = x;
            last_y = y;
        }
    }

    /* Flush otherwise the last page won't be displayed */
    gdm_flush_cache();
}

/**
 * Go thru Y-Values (radius to 0) and calc angles from Ys and radius
 * If angle is in range calc X. Pythag
 *
 * /param inStruct	Struct containing center point, radius, and start
 *					and stop angles
 */
void gdm_draw_arc_struct(gdm_arc *inStruct)
{
    gdm_draw_arc(	inStruct->x_center, inStruct->y_center,
                    inStruct->radius,
                    inStruct->start_angle, inStruct->stop_angle,
                    inStruct->activeState);
}

/**
 * Draw a circle with a center at the given point and of the given
 * radius
 *
 * While negative values are accepted for the center point, all
 * negative and other points outside of the displays dimensions
 * will not be drawn.
 *
 * /param center_x X-coordinate of the circle's center
 * /param center_y Y-coordinate of the circle's center
 * /param radius Radius, in pixels of the circle
 * /param quadrant Quadrants of the circle to be drawn
 *
 */
void gdm_draw_circle(int16_t center_x, int16_t center_y, uint8_t radius,
                     gdm_circle_quadrant quadrants, bool filled, pixel_state activeState)
{
    uint8_t i;
    uint16_t j;
    int16_t fillCounter;
    uint32_t yValue;
    int16_t lastY;

    //uint16_t iSquared;
    uint16_t radiusSquared;
    //uint32_t ySquared;

    int16_t fillEndPoint;


    /* Only a quarter of the circle needs to be calculated since the
    other 3/4 of the circle will based off of the first. Calculate the
    top-right quadrant since all of the values will be positive. After
    each of the X-Y pairs, the points between the previous X-Y pair and
    the just calculated pair need to be filled, otherwise there will
    not be a solid line */

    lastY = radius;
    yValue = radius;

    /**
    * Explanation of the match, basically it is the Pythagorean
    * theorem
    * r**2 = x**2 + y**2 =>
    * r**2 - x**2 = y**2 =>
    * y = sqrt(r**2 - x**2)
    */
    radiusSquared = radius;
    radiusSquared = radiusSquared * radius;


    /* Start at the 12 o'clock position */
    for(i = 0; i <= radius; i++) {

        if(i != 0) {
            yValue = sqrt(radiusSquared - i * i);
        }

        /* Top Right */
        if(quadrants & gdm_quadrant_ne) {
            gdm_set_pixel_level(center_x + i, center_y - (int16_t) yValue, activeState);

            if(filled) {
                for(fillCounter = 1; fillCounter <= yValue; fillCounter++) {
                    gdm_set_pixel_level(
                        center_x + i,
                        center_y - (int16_t) yValue + fillCounter,
                        activeState);
                }

            } else {
                for(j = lastY - 1; j > yValue; j--) {
                    gdm_set_pixel_level(
                        center_x + i - 1,
                        center_y - j,
                        activeState);
                }
            }
        }

        /* Bottom Right */
        if(quadrants & gdm_quadrant_se) {

            /* Set the calculated point */
            if(!((quadrants & gdm_quadrant_ne) && i == radius)) {
                gdm_set_pixel_level(center_x + i, center_y + (int16_t) yValue, activeState);
            }

            /*Fill the quadrant, one column at a time, starting with
            the pixel closest to the last one drawn in order to keep
            calls to the LCD to a minimum */
            if(filled) {
                if(quadrants & gdm_quadrant_ne) {
                    fillEndPoint = (int16_t) yValue - 1;

                } else {
                    fillEndPoint = (int16_t) yValue;
                }

                for(fillCounter = 1; fillCounter <= fillEndPoint; fillCounter++) {
                    gdm_set_pixel_level(
                        center_x + i,
                        center_y + (int16_t) yValue - fillCounter,
                        activeState);
                }

            } else {
                /* Because the next vertical point may be more than 1 pixel
                less, the vertical pixels between this point and the last
                must be filled */
                for(j = lastY - 1; j > yValue; j--) {
                    gdm_set_pixel_level(
                        center_x + i - 1,
                        center_y + j,
                        activeState);
                }
            }
        }

        /* Lower left */
        if(quadrants & gdm_quadrant_sw) {

            if(!((quadrants & gdm_quadrant_se) && i == 0)) {
                gdm_set_pixel_level(center_x - i, center_y + (int16_t) yValue, activeState);
            }

            if(filled) {
                for(fillCounter = 1; fillCounter <= yValue; fillCounter++) {
                    gdm_set_pixel_level(
                        center_x - i,
                        center_y + (int16_t) yValue - fillCounter,
                        activeState);
                }

            } else {
                for(j = lastY - 1; j > yValue; j--) {
                    gdm_set_pixel_level(
                        center_x - i + 1,
                        center_y + j,
                        activeState);
                }
            }
        }

        /* Upper left */
        if(quadrants & gdm_quadrant_nw) {

            if((!((quadrants & gdm_quadrant_sw) && i == radius)) &&
                    (!((quadrants & gdm_quadrant_ne) && i == 0))) {

                gdm_set_pixel_level(center_x - i, center_y - (int16_t) yValue, activeState);
            }

            if(filled) {
                if(quadrants & gdm_quadrant_sw) {
                    fillEndPoint = (int16_t) yValue - 1;

                } else {
                    fillEndPoint = (int16_t) yValue;
                }

                for(fillCounter = 1; fillCounter <= fillEndPoint; fillCounter++) {
                    gdm_set_pixel_level(
                        center_x - i,
                        center_y - (int16_t) yValue + fillCounter,
                        activeState);
                }

            } else {
                for(j = lastY - 1; j > yValue; j--) {
                    gdm_set_pixel_level(
                        center_x - i + 1,
                        center_y - j,
                        activeState);
                }
            }
        }

        lastY = yValue;
    }

    /* When only inverting opposite quadrants the center pixel gets
    inverted twice and needs to be flipped again */
    if((quadrants == (gdm_quadrant_ne | gdm_quadrant_sw)) ||
            (quadrants == (gdm_quadrant_se | gdm_quadrant_nw))) {
        gdm_set_pixel_level(center_x, center_y, activeState);
    }

    /* Flush otherwise the last page won't be displayed */
    gdm_flush_cache();
}

/**
 * Draw a circle with a center at the given point and of the given
 * radius
 *
 * While negative values are accepted for the center point, all
 * negative and other points outside of the displays dimensions
 * will not be drawn.
 *
 * /param inStruct	Struct containing circle center point, radius,
 *					quadrants to draw, and fill
 *
 */
void gdm_draw_circle_struct (gdm_circle *inStruct)
{
    gdm_draw_circle(	inStruct->x_center, inStruct->y_center,
                        inStruct->radius, inStruct->quadrants,
                        inStruct->filled, inStruct->activeState);
}

/**
 * Draws a bar graph
 */
void gdm_draw_bar_graph(gdm_bar_graph *graph)
{
    uint8_t i;
    uint8_t j;

    //uint64_t pixelValue;

    uint8_t startPixel;
    uint8_t stopPixel;
    uint64_t targetPixel;

    bool drawBorder;
    bool activePixel;

    uint8_t firstThickPixel;
    uint8_t lastThickPixel;

    /* There needs to be at least three pixels thickness for a boarder
    to be drawn */
    if((graph->thickness > 2) && graph->drawBorder) {
        if(!graph->alreadyDrawn) {
            switch(graph->orientation) {
            case horizontal_right:
            case horizontal_left:
                gdm_draw_rectangle(
                    graph->coordX,
                    graph->coordY,
                    graph->coordX + graph->length - 1,
                    graph->coordY + graph->thickness - 1,
                    false,
                    graph->activeState);
                break;

            case vertical_top:
            case vertical_bottom:
                gdm_draw_rectangle(
                    graph->coordX,
                    graph->coordY,
                    graph->coordX + graph->thickness - 1,
                    graph->coordY + graph->length - 1,
                    false,
                    graph->activeState);
                break;
            }
        }

        drawBorder = true;
        firstThickPixel = 1;
        lastThickPixel = graph->thickness - 1;

    } else {
        drawBorder = false;
        firstThickPixel = 0;
        lastThickPixel = graph->thickness;
    }

    /* Calculate the pixel that represents the target value */

    /**
     * If you can avoid redrawing pixels that are unchanged, things can be
     * sped up some
     */
    /*if(graph->value > graph->max) {
    	if(graph->drawBorder) {
    		targetPixel = graph->length - 1;
    	} else {
    		targetPixel = graph->length;
    	}
    } else if(graph->value < graph->max) {
    	if(graph->drawBorder) {
    		targetPixel = 1;
    	} else {
    		targetPixel = 0;
    	}
    }*/

    /* Determine where the row / column representing the in value is */
    if(graph->drawBorder) {
        /* When a border is drawn, the pixels for it must be taken into
        account */
        targetPixel = graph->value * (graph->length - 2) /
                      (graph->max - graph->min) + 1;

    } else {
        targetPixel = graph->value * (graph->length - 1) / (graph->max - graph->min) + 1;
    }

    if((graph->orientation == horizontal_right) ||
            (graph->orientation == vertical_bottom)) {

        targetPixel = graph->length - targetPixel;
    }

    if(graph->alreadyDrawn) {
        if(targetPixel > graph->lastPixel) {
            stopPixel = (uint8_t) targetPixel;
            startPixel = graph->lastPixel;

        } else if(targetPixel < graph->lastPixel) {
            stopPixel = graph->lastPixel;
            startPixel = (uint8_t) targetPixel;

        } else {
            /* Nothing will change will the graph so leave now */
            return;
        }

    } else {
        startPixel = 0;
        stopPixel = graph->length - 1;
    }

    /* Store the destination for the next time that this is called so
    things can go more quickly */
    graph->lastPixel = (uint8_t) targetPixel;

    if((graph->orientation == horizontal_left) ||
            (graph->orientation == horizontal_right)) {
        //@TODO: Handle values outside of range

        for(i = startPixel; i <= stopPixel; i++) {
            activePixel = false;

            if(drawBorder) {
                if(i == 0) {
                    /* When there's a border the first column will
                    always be on and is already on so skip it */
                    i = 1;

                } else if(i == graph->length - 1) {
                    /* Same with the last column */
                    break;
                }

                if((i <= targetPixel)) {
                    activePixel = true;
                    //graph->lastPixel = i;
                }

            } else {
                /* In non-border its pretty simple, if its less than
                or equal to the target column, its going to be drawn
                active */
                if(i <= targetPixel) {
                    activePixel = true;
                    //graph->lastPixel = i;
                }
            }

            /* When drawing from the right, the pixels need inverted */
            if((graph->orientation == horizontal_right) &&
                    (i != targetPixel)) {
                activePixel = !activePixel;
            }

            /* Do the actual drawing */
            for(j = firstThickPixel; j < lastThickPixel; j++) {
                gdm_set_pixel_level(
                    graph->coordX + i,
                    graph->coordY + j,
                    !((activePixel) ^
                      (graph->activeState)));
            }
        }

    } else if((graph->orientation == vertical_bottom) ||
              (graph->orientation == vertical_top)) {
        /*  */
        for(i = firstThickPixel; i < lastThickPixel; i++) {

            /* Do the actual drawing */
            for(j = startPixel; j <= stopPixel; j++) {

                activePixel = false;

                if(drawBorder) {
                    if(j == 0) {
                        /* When there's a border the first row will
                        always be on and is already on so skip it */
                        j = 1;

                    } else if(j == graph->length - 1) {
                        /* Same with the last row */
                        break;
                    }

                    if(j <= targetPixel) {
                        activePixel = true;
                        //graph->lastPixel = i;
                    }

                } else {
                    /* In non-border its pretty simple, if its less than
                    or equal to the target column, its going to be drawn
                    active */
                    if(j <= targetPixel) {
                        activePixel = true;
                        //graph->lastPixel = i;
                    }
                }

                /* When drawing from the right, the pixels need inverted */
                if((graph->orientation == vertical_bottom) &&
                        (j != targetPixel)) {
                    activePixel = !activePixel;
                }

                gdm_set_pixel_level(
                    graph->coordX + i,
                    graph->coordY + j,
                    !((activePixel) ^
                      (graph->activeState)));
            }

        }

    }

    //graph->lastPixel = graph->value;
    graph->alreadyDrawn = true;

    gdm_flush_cache();
}

/**
 * Draws an analog dial
 *
 * /param inDial	The dial to draw the value on
 * /param inValue	Value to draw on the dial
 */
void gdm_draw_dial(gdm_analog_dial *inDial)
{
    int32_t valueToDraw;

    /* Gauges have a hard limit those outside of those limits are
    only displayed as the closest allowed value */
    valueToDraw = inDial->value;

    if(valueToDraw > inDial->max) {
        valueToDraw = inDial->max;

    } else if(valueToDraw < inDial->min) {
        valueToDraw = inDial->min;
    }

    /* Draw the gauge if it has not already been drawn */
    if(!inDial->alreadyDrawn) {
        /* Draw the dial frame */
        /* @TODO: Circle drawing needs way to have an fill that is the
        opposite of the surrounding line */
        if((inDial->dialType == circle_3_4) ||
                (inDial->dialType == circle_full)) {

            if(inDial->dialType == circle_full) {
                inDial->angleRange = 2 * M_PI;
                inDial->angleStart = M_PI_2;

            } else {
                inDial->angleRange = 3 * M_PI / 2;
                inDial->angleStart = 3 * M_PI / 4;
            }

            /*gdm_draw_circle(inDial->centerX,
            				inDial->centerY,
            				inDial->radius,
            				GDM_CIRCLE_FULL,
            				false,
            				inDial->activeState);*/
            gdm_draw_arc(inDial->centerX,
                         inDial->centerY,
                         inDial->radius,
                         0,
                         360,
                         inDial->activeState);




        } else {
            inDial->angleRange = M_PI;

            if(inDial->dialType == semicircle_left) {

                inDial->angleStart = M_PI_2;

                gdm_draw_circle(inDial->centerX,
                                inDial->centerY,
                                inDial->radius,
                                gdm_quadrant_nw | gdm_quadrant_sw,
                                false,
                                inDial->activeState);

            } else if(inDial->dialType == semicircle_right) {

                inDial->angleStart = - M_PI_2;

                gdm_draw_circle(inDial->centerX,
                                inDial->centerY,
                                inDial->radius,
                                gdm_quadrant_ne | gdm_quadrant_se,
                                false,
                                inDial->activeState);

            } else if(inDial->dialType == semicircle_up) {

                inDial->angleStart = M_PI;

                gdm_draw_circle(inDial->centerX,
                                inDial->centerY,
                                inDial->radius,
                                gdm_quadrant_ne | gdm_quadrant_nw,
                                false,
                                inDial->activeState);

            } else if(inDial->dialType == semicircle_down) {

                inDial->angleStart = 0;

                gdm_draw_circle(inDial->centerX,
                                inDial->centerY,
                                inDial->radius,
                                gdm_quadrant_se | gdm_quadrant_sw,
                                false,
                                inDial->activeState);

            }
        }

        /* Add the interval ticks */
        if(inDial->coarseInterval > 0) {

            if(inDial->displayCoarseValues) {

            }

        }

        if(inDial->fineInterval > 0) {

        }

        inDial->alreadyDrawn = true;

    } else {
        /* If the dial has already been drawn, undraw the last
        position of the hand. (Draw it again but with an inverting
        pixel type */

        gdm_draw_dial_needle(inDial, &(inDial->lastValue), true);

        /* Redraw any numbers that might have been covered by the
        hand */

    }

    /* If this is a semi-circle, draw a line to close the semicircle.
    Even if the circle was already drawn, redraw the lines on the edge
    since they might have gotten over written by the needle. */
    if((inDial->dialType == semicircle_up) ||
            (inDial->dialType == semicircle_down)) {

        gdm_draw_line(inDial->centerX - inDial->radius,
                      inDial->centerY,
                      inDial->centerX + inDial->radius,
                      inDial->centerY,
                      inDial->activeState);

    } else if((inDial->dialType == semicircle_left) ||
              (inDial->dialType == semicircle_right)) {

        gdm_draw_line(inDial->centerX,
                      inDial->centerY - inDial->radius,
                      inDial->centerX,
                      inDial->centerY + inDial->radius,
                      inDial->activeState);
    }

    /* Draw the needle */
    gdm_draw_dial_needle(inDial, &valueToDraw, false);

    inDial->lastValue = inDial->value;
}

/**
 * Draws the needle for a gauge.
 *
 * Using the values from the given dial a needle is drawn on that gauge
 * at the given value.
 *
 * Because a needle will need redrawn several times it's faster to undraw
 * the previous needle instead of erasing and redrawing the entire gauge.
 * The undraw functionality redraws the given value, but with the
 * pixel opposite state of the dial's active state.
 *
 * /param inDial The dials whose needle is to be drawn.
 * /param inValue Value to draw the needle at
 * /param undraw Use the opposite pixel state as what's given by inDial
 *
 */
void gdm_draw_dial_needle(gdm_analog_dial *inDial, int32_t *inValue, bool undraw)
{
    float needle_x;
    float needle_y;

    float side_x;
    float side_y;

    //uint8_t needle_length;
    float	needle_angle;

    pixel_state usedState;

    /* If undrawing, find the opposite state */
    if(undraw) {
        if(inDial->activeState == pixel_off) {
            usedState = pixel_on;

        } else {
            usedState = pixel_off;
        }

    } else {
        usedState = inDial->activeState;
    }

    /* Create a small gap between the needle and the gauge edge. This
    makes it look cleaner than running to the edge, but it also allows
    you to avoid dealing with redrawing the circle where the needle
    draws over the edge. */
    /*
    if(inDial->radius >= 2) {
    	needle_length = inDial->radius - 2;

    } else {
    	needle_length = inDial->radius;;
    }*/

    /* Find the needle tip's position */
    needle_angle =	(inDial->angleRange) *
                    (*inValue) / (inDial->max - inDial->min) +
                    (inDial->angleStart);

    needle_x = inDial->centerX + (inDial->radius - 2) * cos(needle_angle);
    needle_y = inDial->centerY + (inDial->radius - 2) * sin(needle_angle);

    needle_x = round(needle_x);
    needle_y = round(needle_y);

#if 0
    /* Draw a single line needle */
    gdm_draw_line(inDial->centerX,
                  inDial->centerY,
                  needle_x,
                  needle_y,
                  usedState);
#else

    /* Draw a 2-sided needle. To keep memory size down, the same x and y
    variables are used for both sides. */
    side_x = inDial->centerX + 2 * cos(needle_angle + M_PI_2);
    side_y = inDial->centerY + 2 * sin(needle_angle + M_PI_2);

    side_x = round(side_x);
    side_y = round(side_y);

    gdm_draw_line(side_x,
                  side_y,
                  needle_x,
                  needle_y,
                  usedState);

    side_x = inDial->centerX + 2 * cos(needle_angle - M_PI_2);
    side_y = inDial->centerY + 2 * sin(needle_angle - M_PI_2);

    side_x = round(side_x);
    side_y = round(side_y);

    gdm_draw_line(side_x,
                  side_y,
                  needle_x,
                  needle_y,
                  usedState);

    /* Draw an arc to close the base of the needle */
    gdm_draw_arc(inDial->centerX,
                 inDial->centerY,
                 2,
                 needle_angle * 180 / M_PI + 90,
                 needle_angle * 180 / M_PI - 90,
                 inDial->activeState);
#endif

#ifdef fill_circle_gap

    if(undraw) {
        /* When undrawing, the circle will have a pixel cleared where
        the needle intersected. Re draw it */

        gdm_set_pixel_level(needle_x, needle_y, inDial->activeState);
    }

#endif

    /*fprintf_P(&usart_serial_stream, PSTR("%u %li\tAngle: %f\tX: %f\tY: %f %u\r\n"),
    	undraw, *inValue, needle_angle, needle_x, needle_y, usedState);*/

    gdm_flush_cache();
}