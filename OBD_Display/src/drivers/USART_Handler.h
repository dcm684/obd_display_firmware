/*
 * USART_Handler.h
 *
 * Created: 8/6/2012 2:54:34 PM
 *  Author: Meyer
 */


#ifndef USART_HANDLER_H_
#define USART_HANDLER_H_

#include <stdio.h>

#include "usart_serial.h"

#include "config/conf_usart_serial.h"

//static usart_serial_options_t usart_options;
FILE usart_serial_stream;

void usart_init(void);
enum status_code usart_local_put(char inChar, FILE ignore);
uint8_t usart_local_get(FILE ignore);
#endif /* USART_HANDLER_H_ */