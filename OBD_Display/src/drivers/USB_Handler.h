/*
 * USB_Handler.h
 *
 * Created: 7/23/2012 10:40:46 AM
 *  Author: Meyer
 */


#ifndef USB_HANDLER_H_
#define USB_HANDLER_H_

#include <asf.h>
#include <stdio.h>
#include "Pin_Defines.h"

// Configuration based on internal RC:
// USB clock need of 48Mhz
//#define CONFIG_USBCLK_SOURCE        USBCLK_SRC_RCOSC
//#define CONFIG_OSC_RC32_CAL         48000000UL
//#define CONFIG_OSC_AUTOCAL_RC32MHZ_REF_OSC  OSC_ID_USBSOF

#define USB_ATTACHED() ioport_get_pin_level(USB_SENSE)

//FILE usb_serial_stream;

void usb_init(void);

#endif /* USB_HANDLER_H_ */