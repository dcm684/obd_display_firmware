#include "OBD\OBD.h"
/*
 * LED_Bar.h
 *
 * Created: 7/19/2012 2:06:00 PM
 *  Author: Meyer
 */


#ifndef LED_BAR_H_
#define LED_BAR_H_

/* /def LED_PATTERN_TYPE_MASK Masks out all non-pattern bits */
#define LED_PATTERN_TYPE_MASK 0x03
/* /def LED_PATTERN_RADIATE Puts the lowest center and highest on
both ends. LEDs on both sides illuminate for each value. Resolution
is hald that of other patterns */
#define LED_PATTERN_RADIATE 2
/* /def LED_PATTERN_CENTERED Inverts the levels of LEDs 0-7. When
used with LED_PATTERN_FILL when a value is less than the center,
the bar is filled from the center point to the LED matching the
value. Does not provide any value when not used with LED_PATTERN_FILL */
#define LED_PATTERN_CENTERED 1
/* /def LED_PATTERN_NORMAL Lowest value is assigned to the left most
LED and highest to the right most */
#define LED_PATTERN_NORMAL 0

/* /def LED_PATTERN_INVERT Invert the LED levels */
#define LED_PATTERN_INVERT 0x4
/* /def LED_PATTERN_FILL Illuminate all LEDs up to and including the
LED that matches the value */
#define LED_PATTERN_FILL	0x8

/* /def LED_SCALAR Amount to scale the led value and the ranges */
#define LED_SCALAR 16

int16_t bar_points[16];
bool bar_invert;
bool bar_centered;
bool bar_fill;
bool bar_radiate;
uint8_t globalLEDPID;
bool bar_rangesScaled;	/* Ranges of the LED are multiplied by a scalar and therefore the output value must be too */

void led_bar_init(void);
void led_bar_set_leds(uint16_t led_pattern);
void led_bar_set_pattern(int low_end, int high_end, uint8_t pattern);
void led_bar_set_value(int16_t *value);
void led_bar_interrupt_handler(void);
void led_bar_set_up_interrupt( volatile obd_pid_list *inList, uint16_t inPid, int min, int max, uint8_t pattern );
void led_bar_set_up_interrupt_float( volatile obd_pid_list *inList, uint16_t inPid, float min, float max, uint8_t pattern );
#endif /* LED_BAR_H_ */