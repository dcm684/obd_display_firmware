/*
 * MCP2515.h
 *
 * Created: 7/20/2012 3:13:13 PM
 *  Author: Meyer
 */


#ifndef MCP2515_H_
#define MCP2515_H_

#include "SPILocal.h"

#define OPEN_CAN_SPI ioport_set_pin_low(CAN_SELECT)
#define CLOSE_CAN_SPI ioport_set_pin_high(CAN_SELECT)
#define SEND_CAN_SPI(A) spi_naked_send_byte(&spi_can_device, A) //spi_write_single(spi_can_device.spi, A)
//#define READ_CAN_SPI(A) A = spi_naked_read_byte(&spi_can_device)//spi_read_single(spi_can_device.spi, A)
#define READ_CAN_SPI spi_naked_read_byte(&spi_can_device)

typedef struct {
    uint16_t adrsValue;
    bool isExtendedAdrs;
    uint32_t extendedAdrsValue;
    bool rtr;
    uint8_t dataLength;
    uint8_t data[8];
}  CANMSG;

/* Only handles Standard IDs
 * Mask - Mask of the bits that are to be filtered, 1 - Check, 0 - Ignore
 * Filters - What values the unmasked bits should be. When a message matches,
 *              it will be passed onto RX buffer 0.
 * There are 2 filters for RX 0.
 */
typedef struct {
    uint16_t mask;
    uint16_t filters[2];
} FILTER0;

/* Only handles Standard IDs
 * Mask - Mask of the bits that are to be filtered, 1 - Check, 0 - Ignore
 * Filters - What values the unmasked bits should be. When a message matches,
 *              it will be passed onto RX buffer 1.
 * There are 4 filters for RX 1.
 */
typedef struct {
    uint16_t mask;
    uint16_t filters[4];
} FILTER1;


//Filter Buffer Constants
enum can_buffer {
    /*< CAN Buffer 0 */
    can_buffer_0,
    /*< CAN Buffer 1 */
    can_buffer_1,
    /*< Both CAN Buffers */
    can_buffer_all,
};

//Data rate selection constants
enum can_baud_rates {
    /*< CAN baud rate of 10kBps */
    can_baud_10k,
    /*< CAN baud rate of 50kBps */
    can_baud_50k,
    /*< CAN baud rate of 100kBps */
    can_baud_100k,
    /*< CAN baud rate of 125kBps */
    can_baud_125k,
    /*< CAN baud rate of 250kBps */
    can_baud_250k,
    /*< CAN baud rate of 500kBps */
    can_baud_500k,
};

enum can_modes {
    /*< Read / Write mode */
    can_mode_normal		= 0x00,
    /*< MCP2515 will be put to sleep */
    can_mode_sleep		= 0x20,
    /*< loop-back mode */
    can_mode_loopback	= 0x40,
    /*< Read-only mode */
    can_mode_listen		= 0x60,
    /*< Configuration mode */
    can_mode_config		= 0x80,
};

//#define SLAVESELECT 10

//MCP2515 Registers
#define RXF0SIDH 0x00
#define RXF0SIDL 0x01
#define RXF0EID8 0x02
#define RXF0EID0 0x03
#define RXF1SIDH 0x04
#define RXF1SIDL 0x05
#define RXF1EID8 0x06
#define RXF1EID0 0x07
#define RXF2SIDH 0x08
#define RXF2SIDL 0x09
#define RXF2EID8 0x0A
#define RXF2EID0 0x0B
#define BFPCTRL 0x0C
#define TXRTSCTRL 0x0D
#define CANSTAT 0x0E
#define CANCTRL 0x0F
#define RXF3SIDH 0x10
#define RXF3SIDL 0x11
#define RXF3EID8 0x12
#define RXF3EID0 0x13
#define RXF4SIDH 0x14
#define RXF4SIDL 0x15
#define RXF4EID8 0x16
#define RXF4EID0 0x17
#define RXF5SIDH 0x18
#define RXF5SIDL 0x19
#define RXF5EID8 0x1A
#define RXF5EID0 0x1B
#define TEC 0x1C
#define REC 0x1D
#define RXM0SIDH 0x20
#define RXM0SIDL 0x21
#define RXM0EID8 0x22
#define RXM0EID0 0x23
#define RXM1SIDH 0x24
#define RXM1SIDL 0x25
#define RXM1EID8 0x26
#define RXM1EID0 0x27
#define CNF3 0x28
#define CNF2 0x29
#define CNF1 0x2A
#define CANINTE 0x2B
#define MERRE 7
#define WAKIE 6
#define ERRIE 5
#define TX2IE 4
#define TX1IE 3
#define TX0IE 2
#define RX1IE 1
#define RX0IE 0

#define CANINTF 0x2C
#define MERRF 7
#define WAKIF 6
#define ERRIF 5
#define TX2IF 4
#define TX1IF 3
#define TX0IF 2
#define RX1IF 1
#define RX0IF 0

#define EFLG 0x2D
#define RX1OVR	7
#define RX0OVR	6
#define TXB0	5
#define TXEP	4
#define RXEP	3
#define TXWAR	2
#define RXWAR	1
#define EWARN	0

#define TXB0CTRL 0x30
#define ABTF	6
#define MLOA	5
#define TXERR	4
#define TXREQ	3
#define TXPRIORITYM 0x03

#define TXB0SIDH 0x31
#define TXB0SIDL 0x32
#define EXIDE 3
#define TXB0EID8 0x33
#define TXB0EID0 0x34
#define TXB0DLC 0x35
#define TXRTR 7
#define TXB0D0 0x36

#define RXB0CTRL 0x60
#define RXM1 6
#define RXM0 5
#define RXRTR 3
#define BUKT 2
// Bits 2:0 FILHIT2:0
#define RXB0SIDH 0x61
#define RXB0SIDL 0x62
#define RXB0EID8 0x63
#define RXB0EID0 0x64
#define RXB0DLC 0x65
#define RXB0D0 0x66

#define RXB1CTRL 0x70
#define RXB1SIDH 0x71
#define RXB1SIDL 0x72
#define RXB1EID8 0x73
#define RXB1EID0 0x74
#define RXB1DLC 0x75
#define RXB1D0 0x76

#define FILHIT2 2
#define FILHIT1 1
#define FILHIT0 0


//MCP2515 Command uint8_ts
#define RESET 0xC0
#define READ 0x03
#define READ_RX_BUFFER 0x90
#define WRITE 0x02
#define LOAD_TX_BUFFER 0x40
#define RTS 0x80
#define READ_STATUS 0xA0
#define RX_STATUS 0xB0
#define BIT_MODIFY 0x05

#define READ_RX_0_SIDH 0x00
#define READ_RX_0_DATA 0x02
#define READ_RX_1_SIDH 0x04
#define READ_RX_1_DATA 0x06

#define CAN_SPI_BAUD 100000000UL

/* List of transmit buffers with values based on their offset from
transmit buffer 0 */
typedef enum {
    can_tx_buffer_0		= 0,
    can_tx_buffer_1		= 0x10,
    can_tx_buffer_2		= 0x20,
    can_tx_buffer_next	= 0xff
} can_tx_buffer;

/* Indicates where data is stored */
typedef enum {
    can_rx_buffer_none,	/*!< No data has been received */
    can_rx_buffer_0,	/*!< Data is in rx buffer 0 */
    can_rx_buffer_1,	/*!< Data is in rx buffer 1 */
    can_rx_buffer_both,	/*!< Data is in both rx buffers */
} can_rx_buffer;

spi_entire_device spi_can_device;

bool can_init(enum can_baud_rates baudConst);
bool can_set_baud_rate(enum can_baud_rates baudConst);
bool can_set_receive_filters(enum can_buffer buffer, FILTER0 bufferFilter0, FILTER1 bufferFilter1);
bool can_set_mode(enum can_modes mode);
bool can_receive_message(CANMSG *msg, uint16_t timeout);
bool can_transmit_message(can_tx_buffer buffer, CANMSG *msg, uint16_t timeout);
uint8_t can_get_tx_err_cnt(void);
uint8_t can_get_rx_err_cnt(void);
void can_write_register(uint8_t regno, uint8_t val);
void can_write_register_bit(uint8_t regno, uint8_t bitno, bool val);
uint8_t can_read_register(uint8_t regno);
bool can_was_data_received(void);
can_rx_buffer can_read_rx_buffer(can_rx_buffer inBuffer, CANMSG *msg);
can_rx_buffer can_poll_rx_buffer(void);
#endif /* MCP2515_H_ */