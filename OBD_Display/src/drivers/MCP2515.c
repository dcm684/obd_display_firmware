/*
 * MCP2515.c
 *
 * Created: 7/20/2012 3:13:01 PM
 *  Author: Meyer
 */
#include <ioport.h>
#include <delay.h>

#include "Pin_Defines.h"
#include "MCP2515.h"
#include "SPILocal.h"

/**
 * /brief Sets the baud rate and puts the MCP2515 in CONFIG mode
 *
 * /param baudConst A constant indicating the desired baud rate
 *
 * /returns Was the baud rate set and the chip put in CONFIG successfully
 */
bool can_init(enum can_baud_rates baudConst)
{
    ioport_configure_pin(CAN_INTERRUPT, IOPORT_DIR_INPUT);

    ioport_set_pin_dir(CAN_SELECT, IOPORT_DIR_OUTPUT);
    ioport_set_pin_high(CAN_SELECT);

    spi_can_device.clock = CAN_SCK;
    spi_can_device.cs = CAN_SELECT;
    spi_can_device.miso = CAN_MISO;
    spi_can_device.mosi = CAN_MOSI;
    spi_can_device.spi = &CAN_SPI;

    init_master_spi(&spi_can_device);

    OPEN_CAN_SPI;
    spi_naked_send_byte(&spi_can_device, RESET);
    CLOSE_CAN_SPI;

    delay_ms(100);

    /* Verify that the device was reset by confirming it is config mode */
    //if(can_mode_config != (can_read_register(CANSTAT) & 0xEF)) {
    if(can_mode_config != (can_read_register(CANSTAT) & can_mode_config)) {
        return false;
    }

    /* Set the baud rate */
    return(can_set_baud_rate(baudConst));

}

/**
 * Sets the baud rate
 *
 * /param baudConst A constant indicating the desired baud rate.
 *
 * /returns Baud rate was successfully set
 */
bool can_set_baud_rate(enum can_baud_rates baudConst)
{
    uint8_t brp;
    uint8_t outData[3];

    /*
    BRP<5:0> = 00h, so divisor (0+1)*2 for 125ns per quantum at 16MHz for 500K
    SJW<1:0> = 00h, Sync jump width = 1
    */
    switch(baudConst) 	{
    case can_baud_500k:
        brp = 0;
        break;

    case can_baud_250k:
        brp = 1;
        break;

    case can_baud_125k:
        brp = 3;
        break;

    case can_baud_100k:
        brp = 4;
        break;

    default:
        return false;
    }

    outData[0] = WRITE;
    outData[1] = CNF1;
    outData[2] = brp & 0b00111111;
    OPEN_CAN_SPI;
    spi_simple_send_packet(&spi_can_device, outData, 3);
    CLOSE_CAN_SPI;

    if(can_read_register(CNF1) != outData[2]) {
        return false;
    }

    /*
    PRSEG<2:0> = 0x01, 2 time quantum for prop
    PHSEG<2:0> = 0x06, 7 time constants to PS1 sample
    SAM = 0, just 1 sampling
    BTLMODE = 1, PS2 determined by CNF3
    */
    outData[1] = CNF2;
    outData[2] = 0b10110001;
    OPEN_CAN_SPI;
    spi_simple_send_packet(&spi_can_device, outData, 3);
    CLOSE_CAN_SPI;

    if(can_read_register(CNF2) != outData[2]) {
        return false;
    }

    /*
    PHSEG2<2:0> = 5 for 6 time constants after sample
    */
    outData[1] = CNF3;
    outData[2] = 0x05;
    OPEN_CAN_SPI;
    spi_simple_send_packet(&spi_can_device, outData, 3);
    CLOSE_CAN_SPI;

    if(can_read_register(CNF3) != outData[2]) {
        return false;
    }

    //SyncSeg + PropSeg + PS1 + PS2 = 1 + 2 + 7 + 6 = 16
    return true;
}

/**
 * Set the mode of the chip and verify it was successfully set
 *
 * /param mode The mode to put the chip in
 *
 * /returns Was the mode successfully set
 */
bool can_set_mode(enum can_modes mode)
{
    uint8_t temp;
    temp = can_read_register(CANCTRL);
    temp = temp & (0x1F);
    temp = temp | (mode & 0xE0);
    can_write_register(CANCTRL, temp);

    /* Verify it was set as per the data sheet */
    temp = can_read_register(CANSTAT);
    temp = temp & (0xE0);

    return(temp == (mode & 0xE0));
}

/**
 * Set receive filters and mask for both buffers
 *
 * Only handles standard IDs. It can potentially handle all ID types
 * relatively changes but I don't need that support.
 *
 * /param buffer			Which receive buffer's filters should be set?
 * /param bufferFilter0 FILTER0 item that contains the mask and filters
 *							for receive buffer 0. If only buffer 1 is
 *							being set, this is ignored.
 * /param bufferFilter1 FILTER1 item that contains the mask and filters
 *							for receive buffer 1. If only buffer 0 is
 *							being set, this is ignored.
 *
 * /returns The given filters were successfully set
 *
 * Added by Christopher Meyer July, 2011
 *
 */
bool can_set_receive_filters( enum can_buffer buffer, FILTER0 bufferFilter0, FILTER1 bufferFilter1 )
{
    uint8_t maskAddr;
    uint8_t mask;
    uint8_t filterAddr;
    uint8_t filterCnt;
    uint8_t activeFilters[4];
    uint8_t bufferCtl;
    uint8_t addressOffset;
    uint8_t i;
    uint8_t initialMode;

    /* Verify that the system is in config mode */
    /*
    if(can_mode_config != (can_read_register(CANSTAT) & can_mode_config)) {
    	return false;
    }
    */
    /* Enter config mode if not already there */
    initialMode = can_read_register(CANSTAT) & 0xE0;
    if(initialMode != can_mode_config) {
        if(!can_set_mode(can_mode_config)) {
            return false;
        }
    }

    /* Call the function recursively for filter 0, if both filters are
    to be set. Then continue and set filter 1. */
    if(buffer == can_buffer_all) {
        if(!can_set_receive_filters(can_buffer_0, bufferFilter0,
                                    bufferFilter1)) {
            return false;
        }

        buffer = can_buffer_1;
    }

    if(buffer == can_buffer_0) {
        maskAddr = RXM0SIDH;
        mask = bufferFilter0.mask;
        filterAddr = RXF0SIDH;
        filterCnt = 2;
        activeFilters[0] = bufferFilter0.filters[0];
        activeFilters[1] = bufferFilter0.filters[1];
        bufferCtl = RXB0CTRL;

    } else {
        maskAddr = RXM1SIDH;
        mask = bufferFilter1.mask;
        filterAddr = RXF2SIDH;
        filterCnt = 4;
        activeFilters[0] = bufferFilter1.filters[0];
        activeFilters[1] = bufferFilter1.filters[1];
        activeFilters[2] = bufferFilter1.filters[2];
        activeFilters[3] = bufferFilter1.filters[3];
        bufferCtl = RXB1CTRL;
    }

    /* Set the mask */
    OPEN_CAN_SPI;
    SEND_CAN_SPI(WRITE);
    SEND_CAN_SPI(maskAddr);
    SEND_CAN_SPI(mask >> 3);
    SEND_CAN_SPI((mask & 0xFF) << 5);
    CLOSE_CAN_SPI;

    /* Verify mask was set */
    if(can_read_register(maskAddr) != (mask >> 3)) {
        return false;
    }

    /* Set filters */
    OPEN_CAN_SPI;
    SEND_CAN_SPI(WRITE);
    SEND_CAN_SPI(filterAddr);

    for(i = 0; i < filterCnt; i++) {
        if((buffer == can_buffer_1) && (i == 1)) {
            /* The filters for buffer 1 are not contiguous */
            CLOSE_CAN_SPI;
            OPEN_CAN_SPI;
            SEND_CAN_SPI(WRITE);
            SEND_CAN_SPI(RXF3SIDH);
        }

        SEND_CAN_SPI(activeFilters[i] >> 3);
        SEND_CAN_SPI((activeFilters[i] & 0xFF) << 5);
        /* The EID filters are unused so just set them to zero */
        SEND_CAN_SPI(0);
        SEND_CAN_SPI(0);
    }

    CLOSE_CAN_SPI;

    /* Verify the filters were set */
    addressOffset = 0;

    for(i = 0; i < filterCnt; i++) {
        if((buffer == can_buffer_1) && i == 1) {
            /* Add an offset for filters RXF3 and up
            (buffer 1 filters 1 and up) */
            addressOffset = 4;
        }

        if(can_read_register(filterAddr + 4 * i + addressOffset) !=
                (activeFilters[i] >> 3)) {
            return false;
        }
    }

    /* Set receive operating mode to SID filter mode */
    can_write_register_bit(bufferCtl, RXM1, 0);
    can_write_register_bit(bufferCtl, RXM0, 1);

    /* Verify that RXM in the buffer control register was set to
    filter SIDs */
    if((can_read_register(bufferCtl) & (_BV(RXM0) | _BV(RXM1))) !=
            _BV(RXM0)) {
        return false;
    }

    /* Return to the initial mode */
    if(initialMode != can_mode_config) {
        can_set_mode(initialMode);
    }

    return true;
}

/**
 * Returns what RX buffer contains data
 *
 * /returns Receive buffer(s) that contains data
 */
can_rx_buffer can_poll_rx_buffer(void)
{
    uint8_t readVal;
    can_rx_buffer retVal;

    retVal = can_rx_buffer_none;

    readVal = can_read_register(CANINTF);

    if(bit_is_set(readVal, ERRIF)) {
        readVal = can_read_register(EFLG);

        /* Clear flags indicating that the RX buffers have overflowed */
        if(bit_is_set(readVal, RX0OVR)) {
            can_write_register_bit(EFLG, RX0OVR, false);
        }

        if(bit_is_set(readVal, RX1OVR)) {
            can_write_register_bit(EFLG, RX1OVR, false);
        }

        /* Reload the buffer indicating which buffers have data in them */
        readVal = can_read_register(CANINTF);
    }

    /* Check if data is waiting in the buffers */
    if(bit_is_set(readVal, RX0IF)) {
        retVal = can_rx_buffer_0;
    }

    if(bit_is_set(readVal, RX1IF)) {
        if (retVal == can_rx_buffer_0) {
            retVal = can_rx_buffer_both;
        } else {
            retVal = can_rx_buffer_1;
        }
    }

    return retVal;
}

/**
 * Reads the given buffer and returns the value stored in that buffer
 *
 * If the variable indicating where the buffer to check indicates that
 * a value is in both receive buffers, buffer 0 will be returned
 *
 * Prior to reading, it will verify if the buffer does actually contain
 * data. If it doesn't the return value will indicate that.
 *
 * /param inBuffer	Buffer to check
 * /param outData	Where to store the data read
 *
 * /return			Where the data was read from
 */
can_rx_buffer can_read_rx_buffer(can_rx_buffer inBuffer, CANMSG *msg)
{
    can_rx_buffer bufferReadFrom;
    uint8_t readVal;
    uint8_t byteIndex;

    if(inBuffer == can_rx_buffer_none) {
        return can_rx_buffer_none;
    }

    bufferReadFrom = can_poll_rx_buffer();

    /* Confirm that the requested buffer has data in it and then
    	check if this data was requested, is Remote Transmission
    	Request (RTR) set? */
    if(bufferReadFrom != can_rx_buffer_none) {

        if((inBuffer == can_rx_buffer_0) ||
                (inBuffer == can_rx_buffer_both)) {

            if ((bufferReadFrom == can_rx_buffer_0) ||
                    (bufferReadFrom == can_rx_buffer_both))	{

                readVal = can_read_register(RXB0CTRL);
                bufferReadFrom = can_rx_buffer_0;
            } else {
                bufferReadFrom = can_rx_buffer_none;
            }

        } else if(inBuffer == can_rx_buffer_1) {

            if(bufferReadFrom == can_rx_buffer_1) {
                readVal = can_read_register(RXB1CTRL);
                bufferReadFrom = can_rx_buffer_1;
            } else {
                bufferReadFrom = can_rx_buffer_none;
            }
        } else {
            bufferReadFrom = can_rx_buffer_none;
        }

        if(bufferReadFrom == can_rx_buffer_none) {
            return bufferReadFrom;
        }

        msg->rtr = bit_is_set(readVal, 3);

        /* Start receiving buffer 0 from the SID high uint8_t */
        OPEN_CAN_SPI;

        if(inBuffer == can_rx_buffer_0) {
            SEND_CAN_SPI(READ_RX_BUFFER | READ_RX_0_SIDH);

        } else {
            SEND_CAN_SPI(READ_RX_BUFFER | READ_RX_1_SIDH);
        }

        /* Get the SID */
        msg->adrsValue = 0;
        SEND_CAN_SPI(0);
        readVal = READ_CAN_SPI;
        msg->adrsValue = (readVal << 3);
        SEND_CAN_SPI(0);
        readVal = READ_CAN_SPI;
        msg->adrsValue |= (readVal >> 5);

        /* Get EID if it exists */
        msg->isExtendedAdrs = bit_is_set(readVal, EXIDE);
        msg->extendedAdrsValue = 0;

        if(msg->isExtendedAdrs) {
            msg->extendedAdrsValue = (readVal & 0x03);
            msg->extendedAdrsValue = (msg->extendedAdrsValue) << 16;
            SEND_CAN_SPI(0);
            readVal = READ_CAN_SPI;
            msg->extendedAdrsValue |= (readVal << 8);
            SEND_CAN_SPI(0);
            readVal = READ_CAN_SPI;
            msg->extendedAdrsValue |= readVal;

        } else {
            SEND_CAN_SPI(0);
            SEND_CAN_SPI(0);
        }

        /* Get the number of bytes received */
        SEND_CAN_SPI(0);
        readVal = READ_CAN_SPI;
        msg->dataLength = (readVal & 0x0F);

        if(msg->dataLength > 8) {
            msg->dataLength = 8;
        }

        /* Read the actual data */
        for(byteIndex = 0; byteIndex < msg->dataLength; byteIndex++) {
            SEND_CAN_SPI(0);
            readVal = READ_CAN_SPI;
            msg->data[byteIndex] = readVal;
        }

        /* End the communication with the chip.
        No need to reset the RXIF. */
        CLOSE_CAN_SPI;
    }

    return bufferReadFrom;
}

/**
 * Checks to see if a message exists in either of the receive buffers and
 * returns the value message in msg
 *
 * When calling provide a CANMSG that will be overwritten if data exists in
 * either buffer. If not data is in the buffer or received in timeout
 * seconds, the function will return false. Otherwise true will be returned.
 *
 * Data in Buffer 0 has priority.
 *
 * /param msg		Pointer to the CANMSG item where the CAN message will
 *						be stored
 * /param timeout	The number of milliseconds the function will wait
 *						before returning a false
 *
 * /returns			Data was successfully received.
 *
 * Added by Christopher Meyer July, 2011. Based on Frank Kienast's
 * can_receive_message, but now uses the Read RX Buffer SPI instructions.
 * Also, both RX buffers are checked.
 *
 */
bool can_receive_message(CANMSG *msg, uint16_t timeout)
{
    uint16_t timer;

    can_rx_buffer bufferWithData;

    bufferWithData = can_rx_buffer_none;

    /* Wait for data to be received in either buffer */
    for(timer = 0; timer < timeout; timer++) {

        bufferWithData = can_poll_rx_buffer();

        if(bufferWithData != can_rx_buffer_none) {
            break;
        }

        delay_ms(1);
    }

    if(bufferWithData != can_rx_buffer_none) {
        bufferWithData = can_read_rx_buffer(bufferWithData, msg);
    }

    /* Return whether data is in the buffer */
    return (bufferWithData != can_rx_buffer_none);

}

/**
 * Sends a message over than CAN bus
 *
 * /param msg		Message to transmit
 * /param timeout	Number of milliseconds to wait for a message before
 *						giving up and going home
 * /param buffer	Transmit buffer to use to send the message
 *
 * /returns			The message was successfully sent
 */
bool can_transmit_message(can_tx_buffer buffer, CANMSG *msg, uint16_t timeout)
{
    uint16_t timer;
    bool sentMessage;
    uint8_t txIF;
    uint8_t val;
    uint8_t i;

    txIF = TX0IF; /* Default to TX0IF to guarantee that it is set and to shut the comppiler up */

    /* If no specific buffer is requested find an empty transmit buffer */
    if(buffer == can_tx_buffer_next) {
        /* When TXBnCTRL.TXREQ is clear it means the buffer is empty */
        if(!(can_read_register(TXB0CTRL) & _BV(TXREQ))) {
            buffer = can_tx_buffer_0;
            txIF = TX0IF;

        } else if(!(can_read_register(TXB0CTRL + can_tx_buffer_1) & _BV(TXREQ))) {
            buffer = can_tx_buffer_1;
            txIF = TX1IF;

        } else if(!(can_read_register(TXB0CTRL + can_tx_buffer_2) & _BV(TXREQ))) {
            buffer = can_tx_buffer_2;
            txIF = TX2IF;

        } else {
            /* No empty buffer could be found, so go home and cry */
            return false;
        }
    }

    sentMessage = false;

    val = msg->adrsValue >> 3;
    can_write_register(buffer + TXB0SIDH, val);

    val = msg->adrsValue << 5;

    if(msg->isExtendedAdrs) {
        val |= _BV(EXIDE);
    }

    can_write_register(buffer + TXB0SIDL, val);

    if(msg->isExtendedAdrs) {
        val = msg->extendedAdrsValue >> 8;
        can_write_register(buffer + TXB0EID8, val);
        val = msg->extendedAdrsValue;
        can_write_register(buffer + TXB0EID0, val);
    }

    val = msg->dataLength & 0x0f;

    if(msg->rtr) {
        val |= _BV(TXRTR);
    }

    can_write_register(buffer + TXB0DLC, val);

    /* Message uint8_ts */
    OPEN_CAN_SPI;
    SEND_CAN_SPI(WRITE);
    SEND_CAN_SPI(buffer + TXB0D0);

    for(i = 0; i < msg->dataLength; i++) {
        SEND_CAN_SPI(msg->data[i]);
        /*delay_us(5); / * Needs a delay or the SPI interrupt flag won't
        				get set causing a delay * /*/
    }

    CLOSE_CAN_SPI;
    //delay_us(20);

    /* Transmit the message */
    can_write_register_bit(buffer + TXB0CTRL, TXREQ, true);

    sentMessage = false;

    for(timer = 0; timer < timeout; timer++) {
        val = can_read_register(CANINTF);

        if(bit_is_set(val, txIF)) {
            sentMessage = true;
            break;
        }

        delay_ms(1);
    }

    if(!sentMessage) {
        /* Abort the send if failed */
        can_write_register_bit(buffer + TXB0CTRL, TXREQ, false);
    } else {
        /* Clear message transmitted interrupt  */
        can_write_register_bit(CANINTF, txIF, false);
    }

    return sentMessage;

}

/**
 * Gets the number of transmit errors
 *
 * /returns Number of transmit errors
 */
uint8_t can_get_tx_err_cnt(void)
{
    return(can_read_register(TEC));
}

/**
 * Gets the number of receive errors
 *
 * /returns	Number of receive errors
 */
uint8_t can_get_rx_err_cnt(void)
{
    return(can_read_register(REC));
}

/**
 * Writes the given value to the given register
 *
 * /param regno Register to write to
 * /param val	Value to write
 */
void can_write_register(uint8_t regno, uint8_t val)
{
    uint8_t outPacket[3] = {WRITE, regno, val};

    OPEN_CAN_SPI;
    spi_simple_send_packet(&spi_can_device, outPacket, 3);
    CLOSE_CAN_SPI;
}

/**
 * Sets the value to the given bit in the given register
 *
 * /param regno	Register than contains the bit to be set
 * /param bitno	Bit number to set (7-0)
 * /param val	Value to set the bit to
 */
void can_write_register_bit(uint8_t regno, uint8_t bitno, bool val)
{
    OPEN_CAN_SPI;
    SEND_CAN_SPI(BIT_MODIFY);
    SEND_CAN_SPI(regno);
    SEND_CAN_SPI(_BV(bitno));

    if(val) {
        SEND_CAN_SPI(0xff);

    } else {
        SEND_CAN_SPI(0x00);
    }

    CLOSE_CAN_SPI;
}

/**
 * Returns the current value of the given register
 *
 * /param regno	Register to read
 *
 * /returns		The value in the register
 */
uint8_t can_read_register(uint8_t regno)
{
    uint8_t retVal;
    uint8_t outPacket[3] = {READ, regno, 0};

    OPEN_CAN_SPI;
    spi_simple_send_packet(&spi_can_device, outPacket, 3);
    retVal = spi_naked_read_byte(&spi_can_device);
    CLOSE_CAN_SPI;

    return retVal;
}

/**
 * Checks if any receive buffer has data in it
 *
 * /returns		Is any data available?
 */
bool can_was_data_received(void)
{
    return can_read_register(CANINTF) & (_BV(RX0IF) | (_BV(RX0IF)));
}


