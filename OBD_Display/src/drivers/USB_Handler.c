/*
 * USB_Handler.c
 *
 * Created: 7/23/2012 10:51:05 AM
 *  Author: Meyer
 */
#include "conf_usb.h"
#include "USB_Handler.h"

/* fprintf doesn't work, but straight printf does
FILE usb_serial_stream =
	FDEV_SETUP_STREAM(stdio_usb_putchar, stdio_usb_getchar, _FDEV_SETUP_RW);
*/

void usb_init(void)
{
    irq_initialize_vectors();
    cpu_irq_enable();

    if(!udc_include_vbus_monitoring()) {
        stdio_usb_vbus_event(USB_ATTACHED());
    }

    ioport_configure_pin(USB_SENSE, IOPORT_DIR_INPUT);

    stdio_usb_init();
}
