/*
 * Nav_Switch.h
 *
 * Created: 7/23/2012 11:29:53 AM
 *  Author: Meyer
 */

#ifndef NAV_SWITCH_H_
#define NAV_SWITCH_H_

#include <adc.h>
#include <stdbool.h>

/* /def NAV_ADC_UP ADC value for nav switch being pressed up */
#define NAV_ADC_UP 200
/* /def NAV_ADC_LEFT ADC value for nav switch being pressed left */
#define NAV_ADC_LEFT 1400
/* /def NAV_ADC_DOWN ADC value for nav switch being pressed down */
#define NAV_ADC_DOWN 1000
/* /def NAV_ADC_RIGHT ADC value for nav switch being pressed right */
#define NAV_ADC_RIGHT 600

/* /def NAV_PRESS_UP Nav switch was pressed up */
#define NAV_PRESS_UP 1
/* /def NAV_PRESS_LEFT Nav switch was pressed left */
#define NAV_PRESS_LEFT 2
/* /def NAV_PRESS_DOWN Nav switch was pressed down */
#define NAV_PRESS_DOWN 4
/* /def NAV_PRESS_RIGHT Nav switch was pressed left */
#define NAV_PRESS_RIGHT 8
/* /def NAV_PRESS_SELECT Nav switch select was pressed */
#define NAV_PRESS_SELECT 16
/* /def NAV_PRESS_NONE Nothing on the nav switch was pressed */
#define NAV_PRESS_NONE 0

typedef enum {
    NAV_PRESSED,
    NAV_HELD,
    NAV_RELEASED,
    NAV_OFF
} nav_button_state;

typedef struct {
    nav_button_state up;
    nav_button_state right;
    nav_button_state down;
    nav_button_state left;
    nav_button_state select;
} nav_switch_states;

nav_switch_states nav_all_switches;

ADC_t nac_adc_port;



void nav_init(void);
uint8_t nav_check(void);
bool nav_button_update(nav_button_state *inState, bool pressed);
#endif