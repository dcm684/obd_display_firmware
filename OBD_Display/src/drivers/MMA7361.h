/*
 * MMA7361.h
 *
 * Created: 7/20/2012 10:10:34 AM
 *  Author: Meyer
 */


#ifndef MMA7361_H_
#define MMA7361_H_

#include <avr/io.h>
#include <asf.h>

#include "Pin_Defines.h"

#define ACCEL_GS_15 15
#define ACCEL_GS_60 90

/*
#define ACCEL_X_AXIS 0
#define ACCEL_Y_AXIS 1
#define ACCEL_Z_AXIS 2
*/

/* /def ACCEL_IIR_SHIFT
 * Power of 2 to use as a fraction filter for the Infinite Impulse response
 * filter used to calculate the average accelerometer value.
 */
#define ACCEL_IIR_SHIFT 2

/* /def ACCEL_X_CHAN Channel on accelerometer ADC port attached to X-Axis */
#define ACCEL_X_CHAN ADCCH_POS_PIN0
/* /def ACCEL_Y_CHAN Channel on accelerometer ADC port attached to Y-Axis */
#define ACCEL_Y_CHAN ADCCH_POS_PIN1
/* /def ACCEL_Z_CHAN Channel on accelerometer ADC port attached to Z-Axis */
#define ACCEL_Z_CHAN ADCCH_POS_PIN2

#define ACCEL_INTERREAD_DELAY_MS 10
#define ACCEL_BAD_AXIS_VAL 0xFFFFFFFF

//Scale values by 16 to increase precision while still using ints
#define ACCEL_VALUE_SCALE 16

#if 1
/* Even though the chip has a 12-bit ADC, the MSB is a sign bit. The conversion is signed because
 * I need to use the differential ADC which requires signed results. This results in an 11-bit result */
#define ACCEL_MID_POINT_33 1024 /* 2048 / 2 */
#define ACCEL_XY_BITS_ZERO_33 16384//1024 * 16

//Resolution of 1 bit at 3.3V and 1.5g, 1g/0.8V * 3.3V/2048 = 0.00201 g/b
//
//Scaled by 4 to lose less precision, 4 because right shift 2 is easier than div
//
//Voltage of  1g: 1g * Voltage resolution = 1g * 800mV/g = 800mV;
//Bits in 1g: 1g voltage resolution * bits per volt = 0.8V * 2048 / 3.3V =  496.48;
//+1g value in bits: Bits in 1g + +0g bits = 496.48 + 2048 = 2544.48
//Scaled +1g value in bits: +1g value in bits * ACCEL_VALUE_SCALE = 2544.48 * 16 = 40711.76
#define ACCEL_Z_BITS_ZERO_33_15 40711

//Bits per g: (b/V)*(V/g) = 4096 / 3.3V * 0.8V / 1g = 496.48 b/g;
//Scaled bits per g: Bits per g * ACCEL_VALUE_SCALE = 496.48 * 16 = 7943.68
#define ACCEL_G_CONVERSION_33_15 7944

//Resolution of 1 bit at 3.3V and 1.5g, 1g/0.206V * 3.3V/2048 = 0.00782 g/b
//
//Bits in 1g: 1g voltage resolution * bits per volt = 0.206V * 2048 / 3.3V =  127.84 bits
//+1g value in bits: Bits in 1g + +0g bits = 127.84 + 2048 = 2175.84
//Scaled +1g value in bits: +1g value in bits * ACCEL_VALUE_SCALE = 2175.84 * 16 = 34813.52
#define ACCEL_Z_BITS_ZERO_33_60 34814

//Bits per g: (b/V)*(V/g) = 2048 / 3.3V * 0.206V / 1g = 127.84 bits/g;
//Scaled bits per g: Bits per g * ACCEL_VALUE_SCALE = 127.84 bits/g * 16 = 2045.52
#define ACCEL_G_CONVERSION_33_60 2046

#else /* Full 12-bit range */
/* 12-bit ADC resolution */
#define ACCEL_MID_POINT_33 2048
#define ACCEL_XY_BITS_ZERO_33 66536	//Real ADC value (4096) Multiplied by ACCEL_VALUE_SCALE

//Resolution of 1 bit at 3.3V and 1.5g, 1g/0.8V * 3.3V/4096 = 0.00101 g/b
//Scaled by 4 to lose less precision, 4 because right shift 2 is easier than div
//Voltage of  1g: 1g * Voltage resolution = 1g * 800mV/g = 800mV;
//Bits in 1g: 1g voltage resolution * bits per volt = 0.8V * 4096 / 3.3V =  992.97;
//+1g value in bits: Bits in 1g + +0g bits = 992.97 + 2048 = 3040.97
//Scaled +1g value in bits: +1g value in bits * ACCEL_VALUE_SCALE = 3040.97 * 16 = 48655.52
#define ACCEL_Z_BITS_ZERO_33_15 48656
//Bits per g: (b/V)*(V/g) = 4096 / 3.3V * 0.8V / 1g = 992.97 b/g;
//Scaled bits per g: Bits per g * ACCEL_VALUE_SCALE = 992.97 * 16 = 15887.52
#define ACCEL_G_CONVERSION_33_15 7944

//Voltage of  1g: 1g * Voltage resolution = 1g * 206mV/g = 206mV;
//Bits in 1g: 1g voltage resolution * bits per volt = 0.206V * 4096 / 3.3V =  255.69;
//+1g value in bits: Bits in 1g + +0g bits = 255.69 + 2048 = 2303.69
//Scaled +1g value in bits: +1g value in bits * ACCEL_VALUE_SCALE = 2303.69 * 16 = 36859.03
#define ACCEL_Z_BITS_ZERO_33_60 36859
//Bits per g: (b/V)*(V/g) = 4096 / 3.3V * 0.206V / 1g = 255.69 b/g;
//Scaled bits per g: Bits per g * ACCEL_VALUE_SCALE = 255.69 * 16 = 4091
#define ACCEL_G_CONVERSION_33_60 4091
#endif

typedef enum {
    accel_axis_type_x,	/*< X-axis for the accelerometer */
    accel_axis_type_y,	/*< Y-axis for the accelerometer */
    accel_axis_type_z,	/*< Z-axis for the accelerometer */
    accel_axis_type_none	/*< No axis */
} accel_axis_type;

typedef struct {
    ADC_t *port;									/*! ADC struct for port */
    uint8_t channel_mask;							/*! Mask for channel on port */
    enum adcch_positive_input channel_pos_input;	/*! Reference for positive input */
    uint8_t pin_number;								/*! Pin number to check */
    int16_t offset;									/*! Offset used for zeroing */
    volatile uint16_t value;	/*! Most recent filtered value */
    volatile uint16_t rawValue; /*! Most recent value read (unfiltered) */
} adcchan;

typedef struct {
    adcchan axis_x;	/* Accelerometer's x-axis ADC */
    adcchan axis_y;	/* Accelerometer's y-axis ADC  */
    adcchan axis_z;	/* Accelerometer's z-axis ADC  */
    volatile accel_axis_type activeAxis; /* Axis to read / or being read */
} accel_all_chans;	/* Collection of all ADC axes */

int _z_bits_zero;
float _g_conversion;
accel_all_chans *activeAllChans; /**< accel_all_chans currently being read */
accel_all_chans accelChans; /**< All 3 accelerometer axes */

void accel_init(accel_all_chans *inAllChans,
                int gRange, unsigned long points);
void accel_single_axis_init(adcchan *);

void accel_start_read(accel_all_chans *inAxes);
//static void accel_finish_read(ADC_t *adc, uint8_t ch_mask, adc_result_t result);

void accel_calibrate(adcchan *, adcchan *, adcchan *);
uint16_t accel_get_raw(adcchan *axis);
int16_t accel_get_raw_with_offset(adcchan *);
int16_t accel_get_in_g(adcchan *axis);
int16_t accel_calc_in_g(adcchan *axis);
uint16_t accel_apply_iir(uint16_t toAdd, uint16_t runningAverage);
#endif /* MMA7361_H_ */