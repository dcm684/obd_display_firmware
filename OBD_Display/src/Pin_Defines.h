/*
 * PinDefines.h
 *
 * Created: 7/18/2012 10:37:24 PM
 *  Author: Meyer
 */

#include <avr\io.h>

#ifndef PIN_DEFINES_H_
#define PIN_DEFINES_H_

/* Accelerometer related defines */
/* /def ADC port that the accelerometer analog outputs are connected to */
#define ACCEL_ADC_PORT ADCA
/* /def ACCEL_MASK_X Mask for ADC port channel connected to the accel. X axis */
#define ACCEL_MASK_X ADC_CH2
/* /def ACCEL_MASK_Y Mask for ADC port channel connected to the accel. Y axis */
#define ACCEL_MASK_Y ADC_CH1
/* /def ACCEL_MASK_Z Mask for ADC port channel connected to the accel. Z axis */
#define ACCEL_MASK_Z ADC_CH0
/* /def ACCEL_POSITIVE_X Reference to positive ADC input pin for X axis */
#define ACCEL_POSITIVE_X ADCCH_POS_PIN2
/* /def ACCEL_POSITIVE_Y Reference to positive ADC input pin for Y axis */
#define ACCEL_POSITIVE_Y ADCCH_POS_PIN1
/* /def ACCEL_POSITIVE_Z Reference to positive ADC input pin for Z axis */
#define ACCEL_POSITIVE_Z ADCCH_POS_PIN0
/* /def ACCEL_PIN_X Pin number of the port that the X axis is attached */
#define ACCEL_PIN_X 2
/* /def ACCEL_PIN_Y Pin number of the port that the Y axis is attached */
#define ACCEL_PIN_Y 1
/* /def ACCEL_PIN_Z Pin number of the port that the Z axis is attached */
#define ACCEL_PIN_Z 0
/* /def ACCEL_G_SEL Pin on accelerometer to select sensitivity */
#define ACCEL_G_SEL IOPORT_CREATE_PIN(PORTA, 5)

/* Nav switch related defines */
/* /def NAV_SELECT Digital pin connected to nav switch button press */
#define NAV_SELECT IOPORT_CREATE_PIN(PORTA, 4)
/* /def NAV_ADC_PORT ADC port that nav switch is attached to */
#define NAV_ADC_PORT ADCA
/* /dev NAV_ADC_CHANNEL_MASK ADC channel attached to the nav switch */
#define NAV_ADC_CHANNEL_MASK ADC_CH3
/* /def NAV_ADC_POSITIVE Reference to positive ADC input pin for nav switch */
#define NAV_ADC_POSITIVE ADCCH_POS_PIN3
/* /def NAV_ADC_PIN Pin number of the port that the NAV switch is attached */
#define NAV_ADC_PIN 3

/* LED bar related defines */
/* /def LED_BAR_CLOCK Clock pin for the LED bar shift register */
#define LED_BAR_CLOCK IOPORT_CREATE_PIN(PORTA, 6)
/* /def LED_BAR_DATA Data pin for the LED bar shift register */
#define LED_BAR_DATA IOPORT_CREATE_PIN(PORTA, 7)
/* /def LED_BAR_OE Output enable for the LED bar shift register */
#define LED_BAR_OE IOPORT_CREATE_PIN(PORTB, 0)
/* /def LED_BAR_LATCH Output latch for the LED bar shift register */
#define LED_BAR_LATCH IOPORT_CREATE_PIN(PORTB, 1)

/* CAN controller defines */
/* /def CAN_INTERRUPT Event occurred on CAN controller */
#define CAN_INTERRUPT IOPORT_CREATE_PIN(PORTC, 3)
/* /def CAN_SELECT Chip select for CAN controller */
#define CAN_SELECT IOPORT_CREATE_PIN(PORTC, 4)
/* /def CAN_MOSI MOSI for CAN SPI connection */
#define CAN_MOSI IOPORT_CREATE_PIN(PORTC, 5)
/* /def CAN_MISO MOSI for CAN SPI connection */
#define CAN_MISO IOPORT_CREATE_PIN(PORTC, 6)
/* /def CAN_SCK Clock for CAN SPI connection */
#define CAN_SCK IOPORT_CREATE_PIN(PORTC, 7)
/* /def CAN_SPI SPI used for CAN controller */
#define CAN_SPI SPIC


/* SPI2 and SD card defines */
/* /def SD_DETECT_PORT Port where the SD card detection pin is */
#define SD_DETECT_PORT PORTC
/* /def SD_DETECT Pin to reflect whether a SD card in inserted */
#define SD_DETECT PIN2_bm
/* /def SD_SPI SPI connected to the SD card */
#define SD_SPI SPIE
/* /def SD_SPI SPI connected to the SD card */
#define SD_PORT PORTE
/* /def SD_SELECT Bit mask for Chip select for the SD card */
#define SD_SELECT PIN4_bm
/* /def SD_MOSI Bit mask for MOSI pin connected to the SD card */
#define SD_MOSI PIN5_bm
/* /def SD_MISO Bit mask for MISO pin connected to the SD card */
#define SD_MISO PIN6_bm
/* /def SD_SCK Bit mask for SPI clock pin connected to the SD card */
#define SD_SCK PIN7_bm

/* /def SPI_SELECT_SPARE_1 Chip select of spare SPI #1 (*/
#define SPI_SELECT_SPARE_1 IOPORT_CREATE_PIN(PORTB, 3)
/* /def SPI_SELECT_SPARE_2 Chip select of spare SPI #2 */
#define SPI_SELECT_SPARE_2 IOPORT_CREATE_PIN(PORTB, 2)

/* LCD screen defines */
/* /def LCD_ENABLE Enable output on the LCD screen */
#define LCD_ENABLE IOPORT_CREATE_PIN(PORTD, 0)
/* /def LCD_DATA_INST Is data being given or is it an instruction */
#define LCD_DATA_INST IOPORT_CREATE_PIN(PORTD, 1)
/* /def LCD_RW Controls direction of communication with LCD */
#define LCD_RW IOPORT_CREATE_PIN(PORTD, 2)
/* /def LCD_RESET Reset pin for the LCD */
#define LCD_RESET IOPORT_CREATE_PIN(PORTD, 3)
/* /def LCD_CS_1 Chip select for the LCD chip 1 */
#define LCD_CS_1 IOPORT_CREATE_PIN(PORTE, 0)
/* /def LCD_CS_2 Chip select for the LCD chip 2 */
#define LCD_CS_2 IOPORT_CREATE_PIN(PORTD, 4)
/* /def LCD_TRANSLATE_DIR Used to control the direction of data going through
	the level shifter attached to the LCD data pins. */
#define LCD_TRANSLATE_DIR IOPORT_CREATE_PIN(PORTE, 1)

/* /def LCD_DATA_PORT Port that the LCD data pins are attached to */
#define LCD_DATA_PORT IOPORT_PORTF
/* /def LCD_DATA_BIT_0 LCD data bit 0 */
#define LCD_DATA_BIT_0 IOPORT_CREATE_PIN(PORTF, 0)
/* /def LCD_DATA_BIT_1 LCD data bit 1 */
#define LCD_DATA_BIT_1 IOPORT_CREATE_PIN(PORTF, 1)
/* /def LCD_DATA_BIT_2 LCD data bit 2 */
#define LCD_DATA_BIT_2 IOPORT_CREATE_PIN(PORTF, 2)
/* /def LCD_DATA_BIT_3 LCD data bit 3 */
#define LCD_DATA_BIT_3 IOPORT_CREATE_PIN(PORTF, 3)
/* /def LCD_DATA_BIT_4 LCD data bit 4 */
#define LCD_DATA_BIT_4 IOPORT_CREATE_PIN(PORTF, 4)
/* /def LCD_DATA_BIT_5 LCD data bit 5 */
#define LCD_DATA_BIT_5 IOPORT_CREATE_PIN(PORTF, 5)
/* /def LCD_DATA_BIT_6 LCD data bit 6 */
#define LCD_DATA_BIT_6 IOPORT_CREATE_PIN(PORTF, 6)
/* /def LCD_DATA_BIT_7 LCD data bit 7 */
#define LCD_DATA_BIT_7 IOPORT_CREATE_PIN(PORTF, 7)

/* /def USB_SENSE Pin that indicates if a USB cable is attached */
#define USB_SENSE IOPORT_CREATE_PIN(PORTD, 5)

/* /def USART_TX Pin used to transmit USART data */
#define USART_TX (IOPORT_CREATE_PIN(PORTE, 3))

/* /def USART_RX Pin used to received USART data */
#define USART_RX (IOPORT_CREATE_PIN(PORTE, 2))

/* /def RTC_TWI_MODULE TwoWire (I2C) module for the attached real-time clock */
#define RTC_TWI_MODULE TWIC

#endif /* PINDEFINES_H_ */