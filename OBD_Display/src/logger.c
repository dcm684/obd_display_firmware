/*
 * logger.c
 *
 * Created: 12/19/2012 10:53:05 PM
 *  Author: Meyer
 */

#include "drivers/USART_Handler.h"

#include <string.h>
#include "logger.h"
#include "drivers/MCP7940.h"
#include "drivers/SDFAT/SDHandler.h"
#include "drivers/SDFAT/diskio.h"

#include "OBD/OBD.h"
#include "OBD/OBDPidFunctions.h"
#include "OBD/OBDPidStrings.h"

uint8_t loggerPids[LOG_MAX_PIDS];

/**
 * Creates a logger to store at the given file name the given PIDs.
 *
 * A new file will be created. If a file already exists with the same
 * name the new entries will be appended to the existing file.
 *
 * /param outLogger		The logger that will be created by this function
 * /param inPids		A list of PIDs to record to the log file
 * /param numberOfPids	How many entries are in the inPids
 * /param frequency		Number of 100ms intervals between entries
 * /param inQueryList	PID list to attach to
 *
 * /returns Was the header line written successfully
 */
FRESULT logger_init(logger *inLogger, uint8_t inPids[], uint8_t numberOfPids, uint8_t frequency, volatile obd_pid_list *inQueryList)
{

    rtc_time time;
    rtc_date date;
    uint8_t i;
    uint8_t pidsWritten;


    inLogger->logFileName = malloc(LOG_FILE_NAME_LENGTH);

    /* If the are no PIDs to log, return before doing any thing */
    if(numberOfPids == 0) {
        return 0;
    }

    inLogger->frequency = frequency;
    inLogger->queryList = inQueryList;

    /* Set the file name based on the date and time */
    rtc_get_date(&date);
    rtc_get_time(&time);

    sprintf_P(inLogger->logFileName, PSTR("/%04u%02u%02u_%02u%02u%02u.log"),
              date.year,
              date.month,
              date.date,
              time.hour,
              time.minute,
              time.second);

#if !defined(WRITE_TO_SD) || defined(LOGGER_PRINT)
    /* Print the string out on USART */
    fputs_P(PSTR("Log file name: "), &usart_serial_stream);
    fputs(inLogger->logFileName, &usart_serial_stream);
    fputs_P(PSTR("\r\n"), &usart_serial_stream);
#endif /* !WRITE_TO_SD LOGGER_PRINT */

    /* If there are more PIDs to add than space is available, don't
    write those after the max */
    if(numberOfPids >= LOG_MAX_PIDS) {
        numberOfPids = LOG_MAX_PIDS;
    }

    /* Add the PIDs to the logger */
    pidsWritten = 0;

    for(i = 0; i < numberOfPids; i++) {
        if(logger_add_pid(inLogger, inPids[i])) {
            pidsWritten++;
        }
    }

    /* Write the header to the log file */
    return logger_write_line(inLogger, LOGGER_LINE_HEADER);
}

/**
 * Writes the logged PIDs to the log and initializes another PID request
 * cycle.
 *
 * /returns Success of the log write. If no PIDs are in logger FR_OK
 *			will be returned
 */
FRESULT logger_interrupt_handler(void)
{
    FRESULT retVal;

    /* Write the PIDs */
    if(globalLogger.numberOfPids > 0) {
        retVal = logger_write_line(&globalLogger, LOGGER_LINE_DATA);

    } else {
        retVal = FR_OK;
    }

    return retVal;
}

/**
 * Add a PID to the logger PID list if it doesn't already exist
 *
 * PIDs are also added to the global PID list that is used to request
 * PID values from vehicle
 *
 * /param inLogger	The logger that the PID will be added to
 * /param inPid		PID to be added
 */
bool logger_add_pid(logger *inLogger, uint8_t inPid)
{
    uint8_t i;
    bool addPid = true;

    /* Confirm that space exists in the logger */
    if(inLogger->numberOfPids >= LOG_MAX_PIDS) {
        return false;
    }

    /* Confirm that PID doesn't already exist in the logger */
    for(i = 0; i < inLogger->numberOfPids; i++) {
        if(inPid == inLogger->pids[i]) {
            addPid = false;
            break;
        }
    }

    if(addPid) {
        inLogger->pids[inLogger->numberOfPids] = inPid;
        (inLogger->numberOfPids)++;

        /* Add the PID to the OBD poll list */
        obd_add_real_pids_to_list(inLogger->queryList, inPid);
    }

    return addPid;
}

/**
 * Remove a PID from the logger PID list
 *
 * /param inLogger	Logger to remove the PID from
 * /param inPid		PID to remove from the logger
 *
 * /return			The PID existed and was successfully removed
 */
bool logger_remove_pid(logger *inLogger, uint8_t inPid)
{
    uint8_t i;
    bool matchFound;

    matchFound = false;

    /* Go through the PID list looking for a match. When the match is
    found, move all succeeding PIDs one position earlier */
    for(i = 0; i < inLogger->numberOfPids; i++) {
        if(matchFound) {
            /* Move this PID to the previous index */
            //(inLogger->pids + i - 1) = *(inLogger->pids + i);
            inLogger->pids[i - 1] = inLogger->pids[i];

        } else if(inPid == inLogger->pids[i]) {
            matchFound = true;
        }
    }

    if(matchFound) {
        /* Decrement the numberOfPIDs */
        (inLogger->numberOfPids)--;

        /* Shrink the PID list memory to use only what is needed */
        //inLogger->pids = realloc(inLogger->pids, inLogger->numberOfPids);

        /* Remove the PID from the OBD poll list */
        if(inPid < PID_NON_STD_START) {
            obd_remove_pid_from_list(&pidsToQuery, inPid);
        }
    }

    return matchFound;
}

/**
 * Will go through the given logger and appends the current values of
 * each of the PIDs to the file and closes
 *
 * /param inLogger Logger whose PIDs will be written to its file
 */
FRESULT logger_write_line(logger *inLogger, loggerLineType lineType)
{
    FRESULT result;

    obd_pid tempPID; /* Used with combo PIDs */

    uint8_t i;

    obd_value_with_unit obdValue;

    /* Create the char array that will contain all of the entries for
    the line. Since you cannot create an array dynamically make it large
    enough to fit all values of the larger of the values or header string

    A values entry will include a fixed length float.
    The header has the form "(XX) unit"

    Both contain delimiters between values and a carriage return,
    line feed, and \0 at the end of the lines.
    */
    char *outString;

#if (LOG_ENTRY_LENGTH) > (OBD_UNIT_MAX_LENGTH + 5)
    //char outString[LOG_MAX_PIDS * LOG_ENTRY_LENGTH + 3];
    outString = malloc(LOG_MAX_PIDS * LOG_ENTRY_LENGTH + 3);
#else
    //char outString[LOG_MAX_PIDS * (OBD_UNIT_MAX_LENGTH + 5) + 3];
    outString = malloc(LOG_MAX_PIDS * (OBD_UNIT_MAX_LENGTH + 5) + 3);
#endif

    /* Loop the write here */
    /* Add each entry separately. Generate the string for each PID, then
    add it to the line. After each entry add a tab or end line depending
    on whether it was the last item in the line */
    outString[0] = '\0';

    for(i = 0; i < inLogger->numberOfPids; i++) {

        /* Translate the PID value to something meaningful to the user */
        if(inLogger->pids[i] < PID_NON_STD_START) {
            //obd_translate_pid(&(inLogger->queryList->listOfPIDs[i]), &obdValue);

            obd_get_pid(&pidsToQuery, inLogger->pids[i], &tempPID);
            tempPID.unit_type = obd_unit_english;
            obd_translate_pid(&tempPID, &obdValue);

        } else if(!obd_check_and_process_accel_pid(inLogger->pids[i], &obdValue)) {
            /* Non-standard PID that isn't an accelerometer value */
            tempPID.pid = inLogger->pids[i];
            tempPID.unit_type = obd_unit_english; /* @TODO: This needs a better method
													of choosing combo unit types */

            obd_translate_nonstd_pid(&tempPID, &obdValue);
        }

        if(lineType == LOGGER_LINE_DATA) {
            /* Write the translated value of the received value from the
            vehicle */
            sprintf_P(outString, PSTR("%s%f%c"),
                      outString,
                      obdValue.value,
                      LOG_SEPARATOR_CHAR);

        } else {
            /* Write the headers using the unit from the translated OBD
            value */

            sprintf_P(outString, PSTR("%s(%02X) %S%c"),
                      outString,
                      inLogger->pids[i],
                      //(inLogger->queryList->listOfPIDs[i].pid),
                      (wchar_t *)(obdUnitStrings[obdValue.unit]),
                      LOG_SEPARATOR_CHAR);
        }
    }

    /* Strip the last delimiter and and a CRLF */
    outString[strlen(outString) - 1] = 0;
    sprintf_P(outString, PSTR("%s\r\n"), outString);


#ifdef WRITE_TO_SD
    result = sd_append_file(inLogger->logFileName, outString);

    if(result != FR_OK) {
        put_rc(&usart_serial_stream, result);
    }

#else
    result = FR_OK;
#endif /* WRITE_TO_SD */

#if !defined(WRITE_TO_SD) || defined(LOGGER_PRINT)
    /* Print the string out on USART */
    fputs(outString, &usart_serial_stream);
#endif /* !WRITE_TO_SD LOGGER_PRINT */

    free(outString);

    return result;
}

/**
 * Free the memory for all parts of the logger
 *
 * /param inLogger	Logger whose memory is to be freed
 */

/*
void logger_free(logger *inLogger) {
	free(inLogger->pids);
	free(inLogger->logFileName);
	free(inLogger);
}
*/

/**
 * Clears all values from the given logger
 *
 * /param inLogger Logger to clear
 */
void logger_clear(logger *inLogger)
{
    inLogger->frequency = 0;
    inLogger->logFileName[0] = 0;
    //free(inLogger->logFileName);
    inLogger->numberOfPids = 0;
}
