/*
 * Display.c
 *
 * Handles the drawing on the display
 *
 * Created: 4/5/2013 11:50:36 AM
 *  Author: cmeyer
 */
#include <avr/pgmspace.h>

#include "Display.h"

#include "drivers/GDM/GDM12864H_Text.h"
#include "OBD/OBDPidFunctions.h"
#include "drivers/USART_Handler.h"
#include "OBD/OBDPidStrings.h"

/**
 * Draws the given object on the display
 */
void display_draw_object(display_struct *toDraw)
{
    switch (toDraw->type) {
    case display_type_text:
        gdm_text_print_struct(&(toDraw->text));
        break;
    case display_type_line:
        gdm_draw_line_struct(&(toDraw->line));
        break;
    case display_type_rectangle:
        gdm_draw_rectangle_struct(&(toDraw->rectangle));
        break;
    case display_type_arc:
        gdm_draw_arc_struct(&(toDraw->arc));
        break;
    case display_type_circle:
        gdm_draw_circle_struct(&(toDraw->circle));
        break;
    case display_type_gauge:
        gdm_draw_dial(&(toDraw->dial));
        break;
    case display_type_bar_graph:
        gdm_draw_bar_graph(&(toDraw->graph));
        break;
    default:
        break;
    }
}

/**
 * Refreshes the items on the display with the most recent information
 */
void display_refresh(void)
{

    obd_pid tempPID;
    obd_value_with_unit rpm, miPerHr, fuelEcon, voltage;

    obd_get_pid(&pidsToQuery, pid_engine_rpm, &tempPID);
    tempPID.unit_type = obd_unit_english;
    obd_translate_pid(&tempPID, &rpm);

    obd_get_pid(&pidsToQuery, pid_vehicle_speed, &tempPID);
    tempPID.unit_type = obd_unit_english;
    obd_translate_pid(&tempPID, &miPerHr);

    tempPID.pid = pid_fuel_economy;
    tempPID.unit_type = obd_unit_english;
    obd_translate_nonstd_pid(&tempPID, &fuelEcon);

    obd_get_pid(&pidsToQuery, pid_control_module_volt, &tempPID);
    tempPID.unit_type = obd_unit_english;
    obd_translate_pid(&tempPID, &voltage);

    //gdm_clear_display();

    gdm_move_cursor(0, 0);
    fprintf_P(&gdm_serial_stream,
              PSTR("RPM:\t%4.0f %S\r\nMpH:\t%4.1f %S\r\nMPG:\t%4.1f %S\r\nVBat:\t%2.1f %S\r\n\r\n"),
              (double) rpm.value,
              obdUnitStrings[rpm.unit],
              (double) miPerHr.value,
              obdUnitStrings[miPerHr.unit],
              (double) fuelEcon.value,
              obdUnitStrings[fuelEcon.unit],
              (double) voltage.value,
              obdUnitStrings[voltage.unit]);

}