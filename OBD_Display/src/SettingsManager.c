/*
 * SettingsManager.c
 *
 * Created: 4/12/2014 11:36:06 PM
 *  Author: Meyer
 *
 * Version 1 Format
 * All items are comma delimited
 * PID values are in hexadecimal, e.g. 0A,A,10
 * Lines are ended with \r\n
 * All section headers must exist. If a section is empty, the line after
 *	the header would be blank
 * All PID output values and LED ranges will be floats.
 *
 * The first item for all display items are their enum value followed by values
 * specific to them
 *
 * arc
 * 4,x,y,radius,start_angle,stop_angle,pixel_state
 *
 * circle
 * 5,x,y,radius,quadrants,fill,pixel_state
 *
 * line
 * 2,x1,y1,x2,y2,pixel_state
 *
 * rectangle
 * 3,x1,y1,x2,y2,fill,pixel_state
 *
 * text
 * 1,column,row,word_wrap,length,string
 *
 * bar graph
 * 7,x,y,min,max,length,thickness,orientation,border,pixel_state,pid
 *
 * gauge
 * 6,x,y,min,max,radius,style,title,coarse_increment,fine_increment,print_coarse_values,pixel_state,pid
 *
 * OBD_Settings
 * 1 (This is the version number)
 * Unit (I or i or M or m)
 * logger
 * pid,...
 * led_bar
 * pid,low_end,high_end,pattern
 * display
 * display_item_entry1
 * display_item_entry2
 * display_item_entryN
 *
 */
#include <inttypes.h>
#include <stdbool.h>
#include "SettingsManager.h"
#include "drivers/SDFAT/ff.h"
#include <avr/pgmspace.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "drivers/USART_Handler.h"
#include "logger.h"
#include "drivers/LED_Bar.h"
#include "Display.h"
#include "drivers/GDM/GDMDebug.h"

/**
 * Burns through the log file until it hits a new line
 *
 * @param inFile    File to read
 *
 * @return Result of the read
 */
settings_load_result setting_load_new_line(FIL *inFile)
{
    FRESULT result;
    char currChar;
    UINT bytesRead;

    do {
        /* Read the next character */
        result = f_read(inFile, &currChar, 1, &bytesRead);
    } while ((result == FR_OK) && (currChar != '\n') && (bytesRead > 0));

    if(currChar != '\n') {
        if(result != FR_OK) {
            return settings_load_file_error;
        } else if (bytesRead == 0) {
            return settings_load_eof;
        } else {
            return settings_load_corrupt;
        }
    } else {
        return settings_load_ok;
    }
}

/**
 * Starting with the character in buffer the inFile is read to get a single
 * int delimited by anything non-numeric
 *
 * If the first character of the input buffer is null, the function will
 * read in a variable
 *
 * @param inFile        File to read
 * @param buffer        Where the first char read is and the remaining will be stored
 * @param allowHex      Are hex values allowed?
 * @param allowNegative Are negative values permitted? Unused when hex values are allowed
 * @param outValue      Where to store the found number if everything went as expected
 *
 * @return  The result of the read
 */
//#define DEBUG_READ_INT
FRESULT settings_read_int( FIL *inFile, char *buffer, settings_allow_hex allowHex, settings_allow_negative allowNegative, int *outValue )
{

    UINT bytesRead = 0xCC; /* Default to some non-zero value */
    FRESULT result;
    int valueHolder;
    char *initBuffer;

    result = FR_OK;
    initBuffer = buffer;

    if (*buffer == '\0') {
        result = f_read(inFile, buffer, 1, &bytesRead);
        if (result != FR_OK) {
            return result;
        }
    }

    /* Determine if value is hex. Hex values start with 0x. Can safely dispose of the leading
     * 0 to get to the x */
    if(allowHex == settings_hex_yes) {
        /* Negatives aren't allow when hex values are allowed */
        allowNegative = settings_negative_no;

        if(*buffer == '0') {
            f_read(inFile, buffer, 1, &bytesRead);

            if((*buffer = 'x') || (*buffer = 'X')) {
                f_read(inFile, buffer, 1, &bytesRead);
                /* Move the start of the buffer to the first non-hex indicating char */
                initBuffer = buffer;
            } else {
                allowHex = settings_hex_no;
            }
        } else {
            allowHex = settings_hex_no;
        }
    }

#ifdef DEBUG_READ_INT
    fprintf_P(&usart_serial_stream, PSTR("( %s -> "), buffer);
#endif /* DEBUG_READ_INT */

    do {
        if((bytesRead > 0)
                && (((*buffer >= '0') && (*buffer <= '9'))
                    || ((allowHex == settings_hex_yes)
                        && (((*buffer >= 'a') && (*buffer <= 'f'))
                            || ((*buffer >= 'A') && (*buffer <= 'F'))))
                    || ((allowNegative == settings_negative_yes)
                        && (*buffer == '-')))
          ) {

            /* Add the char to the temp number holder */
            buffer++;
        } else {
            /* Remove the bad char by null terminating then reset the
            pointer to where it was at the start */
            *buffer = '\0';
            buffer = initBuffer;

#ifdef DEBUG_READ_INT
            fprintf_P(&usart_serial_stream, PSTR(" %s ->"), buffer);
#endif /* DEBUG_READ_INT */

            /* Convert the hex to a 16-bit value */
            if (allowHex == settings_hex_yes) {
                valueHolder = strtol(buffer, NULL, 16);
                //fprintf_P(&usart_serial_stream, PSTR("Hex: %s -> %X\r\n"), buffer, valueHolder);
            } else {
                valueHolder = atol(buffer);
            }
            *outValue = valueHolder;
            break;
        }
        /* Read the next character */
        result = f_read(inFile, buffer, 1, &bytesRead);

    } while (result == FR_OK);

#ifdef DEBUG_READ_INT
    fprintf_P(&usart_serial_stream, PSTR(" %s -> %i)\r\n"), buffer, *outValue);
#endif /* DEBUG_READ_INT */

    *buffer = '\0';

    return result;
}

/**
 * Starting with the character in buffer the inFile is read to get a single
 * float delimited by anything non-numeric
 *
 * If the first character of the input buffer is null, the function will
 * read in a variable
 *
 * @param inFile        File to read
 * @param buffer        Where the first char read is and the remaining will be stored
 * @param allowNegative Are negative values permitted?
 * @param outValue      Where to store the found number if everything went as expected
 *
 * @return  The result of the read
 */
//#define DEBUG_READ_FLOAT
FRESULT settings_read_float( FIL *inFile, char *buffer, settings_allow_negative allowNegative, float *outValue )
{

    UINT bytesRead = 0xCC;
    FRESULT result;
    float valueHolder;
    char *initBuffer;

    result = FR_OK;
    initBuffer = buffer;

    if (*buffer == '\0') {
        result = f_read(inFile, buffer, 1, &bytesRead);
        if (result != FR_OK) {
            return result;
        }
    }

#ifdef DEBUG_READ_FLOAT
    fprintf_P(&usart_serial_stream, PSTR("\r\n(%s -> "), buffer);
#endif /* DEBUG_READ_FLOAT */

    do {

        if((bytesRead > 0)
                && (((*buffer >= '0') && (*buffer <= '9'))
                    || (*buffer == '.')
                    || ((allowNegative == settings_negative_yes)
                        && (*buffer == '-')))
          ) {

            /* Add the char to the temp number holder */
            buffer++;
        } else {
            /* Remove the bad char by null terminating then reset the
            pointer to where it was at the start */
            *buffer = '\0';
            buffer = initBuffer;

#ifdef DEBUG_READ_FLOAT
            fprintf_P(&usart_serial_stream, PSTR(" %s ->"), buffer);
#endif /* DEBUG_READ_FLOAT */

            /* Convert the hex to a 16-bit value */
            valueHolder = atof(buffer);
            *outValue = valueHolder;
            break;
        }
        /* Read the next character */
        result = f_read(inFile, buffer, 1, &bytesRead);

    } while (result == FR_OK);

#ifdef DEBUG_READ_FLOAT
    fprintf_P(&usart_serial_stream, PSTR(" %s -> %f)"), buffer, *outValue);
#endif /* DEBUG_READ_FLOAT */

    *buffer = '\0';

    return result;
}

/**
 * Starting with the character in buffer the inFile is read to get a single
 * string delimited by commas and non-text
 *
 * If the first character of the input buffer is null, the function will
 * read in a variable
 *
 * @param inFile    File to read
 * @param inBuffer  Where the first char read is and the remaining will be stored
 * @param outString	Where to store the string number if everything went as expected
 * @param maxStringLength   Longest the read string can be. Anything longer will
 *                          be truncated. Length includes the null terminator.
 *                          No string can be longer than
 *                          SETTING_MAX_STRING_LENGTH regardless of the value
 *                          received for this variable
 *
 * @return The result of the read
 */
//#define DEBUG_READ_STRING
#define PRINT_READ_STRING_TEXT
FRESULT settings_read_string( FIL *inFile,
                              char *inBuffer,
                              char *outString,
                              uint8_t maxStringLength )
{

    UINT bytesRead = 0xCC;
    FRESULT result;
    char *initBuffer;
    char tempString[SETTING_MAX_STRING_LENGTH];
    int8_t charsRead = 0;

    result = FR_OK;
    initBuffer = inBuffer;

    if (*inBuffer == '\0') {
        result = f_read(inFile, inBuffer, 1, &bytesRead);
        if (result != FR_OK) {
            return result;
        }
    }

    /* Ensure that maxStringLength is less than or equal to
     * SETTING_MAX_STRING_LENGTH, the length of the tempString[] */
    if (maxStringLength > SETTING_MAX_STRING_LENGTH) {
        maxStringLength = SETTING_MAX_STRING_LENGTH;
    }

#ifdef DEBUG_READ_STRING
    fprintf_P(&usart_serial_stream, PSTR("\r\n(%s -> "), inBuffer);
#endif

#ifdef PRINT_READ_STRING_TEXT
    fputs_P(PSTR("Read string: "), &usart_serial_stream);
#endif /* PRINT_READ_STRING_TEXT */

    do {
        /* Accept any valid printable characters except ',' */
        if(	(bytesRead > 0)
                && ((*inBuffer >= 0x20) && (*inBuffer <= 0x7E))
                && (*inBuffer != ',')) {

#ifdef PRINT_READ_STRING_TEXT
            fprintf_P(&usart_serial_stream, PSTR("%c"), *inBuffer);
#endif /* PRINT_READ_STRING_TEXT */

            if (charsRead < (maxStringLength - 1)) {
                /* Add the read char to the holder string */
                tempString[charsRead] = *inBuffer;
                charsRead++;
            }

            /* Add the char to the temp string holder */
            inBuffer++;
        } else {
            /* Remove the bad char by null terminating then reset the
            pointer to where it was at the start */
            *inBuffer = '\0';
            inBuffer = initBuffer;

#ifdef DEBUG_READ_STRING
            fprintf_P(&usart_serial_stream, PSTR(" %s ->"), inBuffer);
#endif /* DEBUG_READ_STRING */
            /* Null terminate the out string */

            break;
        }
        /* Read the next character */
        result = f_read(inFile, inBuffer, 1, &bytesRead);

    } while (result == FR_OK);

#ifdef PRINT_READ_STRING_TEXT
    fputs_P(PSTR("\r\n"), &usart_serial_stream);
#endif /* PRINT_READ_STRING_TEXT */

#ifdef DEBUG_READ_STRING
    fprintf_P(&usart_serial_stream, PSTR(" %s -> %i)"), inBuffer, *outValue);
#endif /* DEBUG_READ_STRING */

    /* Copy the read string to the output, only if it was successful */
    if (result == FR_OK) {
        tempString[charsRead] = '\0';
        strcpy(outString, tempString);
    }




    *inBuffer = '\0';

    return result;
}

/**
 * Reads the current line of the given file looking for the given string
 * at the beginning of the line
 *
 * @param	inFile      File to be checked
 * @param	inString    String to find
 * @param	outResult   More descriptive result
 *
 * @return Was the given string found at the beginning of the line?
 */
bool settings_look_for_string(FIL *inFile, char *goalString, settings_load_result *outResult)
{
    FRESULT result;
    bool completeMatch;
    UINT bytesRead;
    bool charMatched;
    char currChar;
    completeMatch = false;

    fprintf_P(&usart_serial_stream, PSTR("Looking for %s\r\n"), goalString);

    do {
        charMatched = false;
        /* Read the next character */
        result = f_read(inFile, &currChar, 1, &bytesRead);
        //fprintf_P(&usart_serial_stream, PSTR("%c(%X)"),currChar,currChar);

        /* Ignore comment lines */
#if 0
        while(currChar == '#') {
            setting_load_new_line(inFile);
            result = f_read(inFile, &currChar, 1, &bytesRead);
            fprintf(&usart_serial_stream, "(X %c)", *goalString);
        }
#endif

        //fprintf_P(&usart_serial_stream, PSTR("In: %c Goal:%c Result: %u\r\n"), currChar, *goalString, result);

        /* If the read character matches the desired character continue */
        if(currChar == *goalString) {
            charMatched = true;
            /* Increment the pointer to the next desired character */
            goalString++;

            /* If the next character is a null character the entire string
             * has been matched. Burn through the rest of the line */
            if(*goalString == '\0') {
                completeMatch = true;
            }
        }

        /* If there was an error reading the file, a mismatch, or the entire
         * goal string was matched, break out */
    } while ((result == FR_OK) && (charMatched) && (!completeMatch));

    if(!completeMatch) {
        if(result != FR_OK) {
            *outResult = settings_load_file_error;
        } else {
            *outResult = settings_load_corrupt;
        }
    } else {
        *outResult = settings_load_ok;
    }

    return completeMatch;
}

/**
 * Gets the version number for the log file
 *
 * @param inFile        File to read
 * @param outVersion    Version read from the file
 *
 * @return  Result of the settings read
 */
settings_load_result settings_load_version(FIL *inFile, int *outVersion)
{
    settings_load_result outResult;
    FRESULT result;
    char currChar;
    UINT bytesRead;
    bool numberFound;
    int version;

    version = 0;
    numberFound = false;
    currChar = '\0';

    do {
        /* Read the next character */
        result = f_read(inFile, &currChar, 1, &bytesRead);

        /* Get out if the new character is the end of a line */
        if ((bytesRead == 0) || (currChar == '\r') || (currChar == '\n')) {
            break;
        }

        /* Ignore comment lines */
        while((currChar == '#') && (bytesRead > 0)) {
            setting_load_new_line(inFile);
            result = f_read(inFile, &currChar, 1, &bytesRead);
        }

        /* When bytes read is 0, the end of the file has been reached */
        if(bytesRead == 0) {
            break;
        }

        result = settings_read_int(inFile, &currChar,
                                   settings_hex_no, settings_negative_no, &version);

        if (result == FR_OK) {
            numberFound = true;
        }

    } while ((result == FR_OK) && (numberFound) && (currChar != '\r') && (currChar != '\n'));

    /* Assume carriage returns are all followed by a newline */
    if(numberFound && (currChar == '\r')) {
        result = f_read(inFile, &currChar, 1, &bytesRead);
    }

    if(!numberFound) {
        if(result != FR_OK) {
            outResult = settings_load_file_error;
        } else if (bytesRead == 0) {
            return settings_load_eof;
        } else {
            outResult = settings_load_corrupt;
        }
    } else {
        outResult = settings_load_ok;
        *outVersion = version;
    }

    return outResult;
}

/**
 * Parses a settings file for logger
 *
 * @param inFile    File to read
 * @param currGoal  Pointer to location in memory to store the key
 *                  being looked for
 *
 * @return Result of the settings read
 */
settings_load_result settings_load_logger( FIL *inFile, char *currGoal )
{
    settings_load_result outResult;
    char currChar;
    UINT bytesRead;
    int theNumber;
    bool numberFound = false;
    FRESULT result = FR_OK;

    uint8_t pidCounter = 0;

    /* verify that it is a logger line */
    snprintf_P(currGoal, SETTING_BUFFER_SIZE, PSTR(SETTING_KEY_LOGGER));
    if(settings_look_for_string(inFile, currGoal, &outResult)) {
        setting_load_new_line(inFile);

        /* Read and load until a comma is encountered. Add the PID the
        start the next PID until a new line or non-numerical value is
        encountered */
        numberFound = true;

        do {
            /* Read the next character */
            result = f_read(inFile, &currChar, 1, &bytesRead);

            /* Get out if the new character is the end of a line */
            if ((currChar == '\r') || (currChar == '\n')) {
                break;
            }

            /* Ignore comment lines */
            while(currChar == '#') {
                setting_load_new_line(inFile);
                result = f_read(inFile, &currChar, 1, &bytesRead);
            }

            /* When bytes read is 0, the end of the file has been reached */
            if(bytesRead == 0) {
                numberFound = false;
                break;
            }

            result = settings_read_int(inFile, &currChar,
                                       settings_hex_yes, settings_negative_no, &theNumber);

            if (result == FR_OK) {
                if (pidCounter < LOG_MAX_PIDS) {
                    fprintf_P(&usart_serial_stream, PSTR("PID: %X\r\n"), theNumber);
                    loggerPids[pidCounter]	= (uint8_t) theNumber;
                    pidCounter++;
                } else {
                    break;
                }
            }

        } while ((result == FR_OK)
                 && (numberFound)
                 && (currChar != '\r')
                 && (currChar != '\n'));

    } else {
        fputs_P(PSTR("Log line missing. Assume no PIDs to log\r\n"), &usart_serial_stream);
    }

    if(!numberFound) {
        if(result != FR_OK) {
            outResult = settings_load_file_error;
        } else if (bytesRead == 0) {
            return settings_load_eof;
        } else {
            outResult = settings_load_corrupt;
        }
    } else {
        /* If there were PIDs that were added to be logged, initialize the logger */
        if(pidCounter > 0) {
            if(FR_OK != logger_init(&globalLogger, loggerPids, pidCounter, 10, &pidsToQuery)) {
                fprintf_P(&usart_serial_stream, PSTR("Failed to init logger from settings file\r\n"));
                outResult = settings_load_logger_error;
            } else {
                outResult = settings_load_ok;
            }
        }
    }

    return outResult;
}

/**
 * Parses a settings file for the LED bar settings
 *
 * @param inFile    File to read
 * @param currGoal  Pointer to location in memory to store the key
 *                  being looked for
 *
 * @return Result of the settings read
 */
//#define DEBUG_LOAD_LED_BAR
settings_load_result settings_load_led_bar( FIL *inFile, char *currGoal )
{

    settings_load_result outResult;
    char currChar;
    UINT bytesRead;
    bool fullLEDBar;
    FRESULT result = FR_OK;

    uint8_t readPID = 0;
    float readLowEnd = 0;
    float readHighEnd = 0;
    uint8_t readPattern = 0;

    fullLEDBar = false;

    /* verify that it is a LED bar line */
    snprintf_P(currGoal, SETTING_BUFFER_SIZE, PSTR(SETTING_KEY_LED_BAR));
    if(settings_look_for_string(inFile, currGoal, &outResult)) {
        setting_load_new_line(inFile);

        int16_t tempInt;
        float tempFloat;

        fullLEDBar = false;
        /* Read the next character */
        result = f_read(inFile, &currChar, 1, &bytesRead);

        /* The end of the file has been reached when 0 bytes are read */
        if(bytesRead == 0) {
            return settings_load_eof;
        }

        /* Ignore comment lines */
        while(currChar == '#') {
            setting_load_new_line(inFile);
            result = f_read(inFile, &currChar, 1, &bytesRead);
        }
        *currGoal = currChar;

        /* PID */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_yes, settings_negative_no, &tempInt);

        if (FR_OK == result) {
            readPID = tempInt;

            /* Min value */
            result = settings_read_float(inFile, currGoal, settings_negative_yes, &tempFloat);
        }

        if (FR_OK == result) {
            readLowEnd = tempFloat;

            /* Max value */
            result = settings_read_float(inFile, currGoal, settings_negative_yes, &tempFloat);
        }

        if (FR_OK == result) {
            readHighEnd = tempFloat;

            /* LED pattern type */
            result = settings_read_int(inFile, currGoal,
                                       settings_hex_no, settings_negative_no, &tempInt);
        }

        if (FR_OK == result) {
            readPattern = tempInt;
            fullLEDBar = true;
        }
    } else {
        fputs_P(PSTR("LED bar line missing. Assume LED bar will be unused\r\n"), &usart_serial_stream);
    }

    if(!fullLEDBar) {
        if(result != FR_OK) {
            outResult = settings_load_file_error;
        } else {
            outResult = settings_load_corrupt;
        }
    } else {
        /* Set the pattern */
        led_bar_set_up_interrupt_float(&pidsToQuery, readPID, readLowEnd, readHighEnd, readPattern);
        outResult = settings_load_ok;

#ifdef DEBUG_LOAD_LED_BAR
        fprintf_P(&usart_serial_stream,
                  PSTR("PID: %X\r\nLow: %f\r\nHigh: %f\r\nPat: %u\r\n"),
                  readPID, readLowEnd, readHighEnd, readPattern);
#endif /* DEBUG_LOAD_LED_BAR */
    }

    while ((currChar != '\n') && (result == FR_OK)) {
        result = f_read(inFile, &currChar, 1, &bytesRead);
    }

    return outResult;
}


/**
 * Loads a line that was determined to contain text that is to
 * be displayed and stores it in a display_struct
 *
 * Expected format:
 * 1,column,row,word_wrap,length,string
 *
 * @param outStruct Where the read bar graph data is to be stored
 * @param inFile    File to read from
 * @param currGoal  Buffer to store read text in
 *
 * @return The success of the read and parse
 */
settings_load_result settings_load_text( display_struct *outStruct, FIL *inFile, char *currGoal )
{
    FRESULT result;
    settings_load_result outResult;
    bool shapeFound;
    gdm_text tempText;
    int tempValue;
    char tempString[SETTING_MAX_STRING_LENGTH];

    result = FR_OK;
    shapeFound = false;

    /* First char column */
    result = settings_read_int(inFile, currGoal,
                               settings_hex_no, settings_negative_no, &tempValue);
    if (FR_OK == result) {
        tempText.column = tempValue;

        /* Text row */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_no, &tempValue);
    }

    if (FR_OK == result) {
        tempText.row = tempValue;

        /* Word wrap */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_no, &tempValue);
    }

    if (FR_OK == result) {
        tempText.wordWrap = (tempValue > 0);

        /* String length */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_no, &tempValue);
    }

    if (FR_OK == result) {
        tempText.length = tempValue;

        /* Actual string */
        result = settings_read_string(inFile, currGoal, tempString, SETTING_MAX_STRING_LENGTH);
    }

    if (FR_OK == result) {
        tempText.string = strdup(tempString);
        tempText.length = strlen(tempString);
        tempText.font = activeFont;

        shapeFound = true;
    }

    if(!shapeFound) {
        if(result != FR_OK) {
            outResult = settings_load_file_error;
        } else {
            outResult = settings_load_corrupt;
        }
    } else {
        outResult = settings_load_ok;
        outStruct->type = display_type_text;
        outStruct->text = tempText;
    }

    return outResult;
}

/**
 * Loads a line that was determined to contain a line that is to
 * be displayed and stores it in a display_struct
 *
 * Expected format:
 * 2,x1,y1,x2,y2,pixel_state
 *
 * 2	Character indicating that line contains a line item (Already read)
 * x1	X-coordinate for point 1 of line
 * y1	Y-coordinate for point 1 of line
 * x2	X-coordinate for point 2 of line
 * y2	Y-coordinate for point 2 of line
 * pixel_state	Pixel state for line
 *
 * @param outStruct Where the read bar graph data is to be stored
 * @param inFile    File to read from
 * @param currGoal  Buffer to store read text in
 *
 * @return The success of the read and parse
 */
settings_load_result settings_load_line( display_struct *outStruct, FIL *inFile, char *currGoal )
{
    FRESULT result;
    settings_load_result outResult;
    bool shapeFound;
    gdm_line tempLine;
    int tempValue;

    shapeFound = false;
    *currGoal = '\0';

    result = settings_read_int(inFile, currGoal,
                               settings_hex_no, settings_negative_yes, &tempValue);
    if (FR_OK == result) {
        tempLine.x1 = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempLine.y1 = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempLine.x2 = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempLine.y2 = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempLine.activeState = tempValue;
        shapeFound = true;
    }

    if(!shapeFound) {
        if(result != FR_OK) {
            outResult = settings_load_file_error;
        } else {
            outResult = settings_load_corrupt;
        }
    } else {
        outResult = settings_load_ok;
        outStruct->type = display_type_line;
        outStruct->line = tempLine;

    }

    return outResult;
}

/**
 * Loads a line that was determined to contain a rectangle that is
 * to be displayed and stores it in a display_struct
 *
 * Expected format:
 * 3,x1,y1,x2,y2,fill,pixel_state
 *
 * 3	Character indicating that line contains a rectangle item (Already read)
 * x1	X-coordinate for corner 1 of line
 * y1	Y-coordinate for corner 1 of line
 * x2	X-coordinate for corner 2 of line
 * y2	Y-coordinate for corner 2 of line
 * fill	Fill the rectangle
 * pixel_state	Pixel state for rectangle
 *
 * @param outStruct Where the read bar graph data is to be stored
 * @param inFile    File to read from
 * @param currGoal  Buffer to store read text in
 *
 * @return The success of the read and parse
 */
settings_load_result settings_load_rect( display_struct *outStruct, FIL *inFile, char *currGoal )
{
    FRESULT result;
    settings_load_result outResult;
    bool shapeFound;
    gdm_rectangle tempRectangle;
    int tempValue;

    shapeFound = false;
    result = settings_read_int(inFile, currGoal,
                               settings_hex_no, settings_negative_yes, &tempValue);
    if (FR_OK == result) {
        tempRectangle.x1 = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempRectangle.y1 = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempRectangle.x2 = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempRectangle.y2 = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempRectangle.filled = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempRectangle.activeState = tempValue;
        shapeFound = true;
    }

    if(!shapeFound) {
        if(result != FR_OK) {
            outResult = settings_load_file_error;
        } else {
            outResult = settings_load_corrupt;
        }
    } else {
        outResult = settings_load_ok;
        outStruct->type = display_type_rectangle;
        outStruct->rectangle = tempRectangle;
    }

    return outResult;
}

/**
 * Loads a line that was determined to contain an arc that is to
 * be displayed and stores it in a display_struct
 *
 * Expected format:
 * 4,x,y,radius,start_angle,stop_angle,pixel_state
 *
 * 4	Character indicating that line contains a arc item (Already read)
 * x	X-coordinate for center of arc
 * y	Y-coordinate for center of arc
 * radius	Radius of the arc
 * start_angle	Start of the arc
 * stop_angle	End of the arc
 * pixel_state	Pixel state for line
 *
 * @param outStruct Where the read bar graph data is to be stored
 * @param inFile    File to read from
 * @param currGoal  Buffer to store read text in
 *
 * @return The success of the read and parse
 */
settings_load_result settings_load_arc( display_struct *outStruct, FIL *inFile, char *currGoal )
{
    FRESULT result;
    settings_load_result outResult;
    bool shapeFound;
    gdm_arc tempArc;
    int tempValue;

    shapeFound = false;
    result = settings_read_int(inFile, currGoal,
                               settings_hex_no, settings_negative_yes, &tempValue);

    if (FR_OK == result) {
        tempArc.x_center = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempArc.y_center = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempArc.radius = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempArc.start_angle = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempArc.stop_angle = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempArc.activeState = tempValue;
        shapeFound = true;
    }


    if(!shapeFound) {
        if(result != FR_OK) {
            outResult = settings_load_file_error;
        } else {
            outResult = settings_load_corrupt;
        }
    } else {
        outResult = settings_load_ok;
        outStruct->type = display_type_arc;
        outStruct->arc = tempArc;
    }

    return outResult;
}

/**
 * Loads a line that was determined to contain a circle that is to
 * be displayed and stores it in a display_struct
 *
 * Expected format:
 * 5,x,y,radius,quadrants,fill,pixel_state
 *
 * 5	Character indicating that line contains a line item (Already read)
 * x	X-coordinate for center of arc
 * y	Y-coordinate for center of arc
 * radius	Radius of the arc
 * quadrants	Quadrants to draw of the circle
 * fill	Should the circle be filled?
 * pixel_state	Pixel state for line
 *
 * @param outStruct Where the read bar graph data is to be stored
 * @param inFile    File to read from
 * @param currGoal  Buffer to store read text in
 *
 * @return The success of the read and parse
 */
settings_load_result settings_load_circle( display_struct *outStruct, FIL *inFile, char *currGoal )
{
    FRESULT result;
    settings_load_result outResult;
    bool shapeFound;
    gdm_circle tempCircle;
    int tempValue;

    shapeFound = false;
    result = settings_read_int(inFile, currGoal,
                               settings_hex_no, settings_negative_yes, &tempValue);
    if (FR_OK == result) {
        tempCircle.x_center = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempCircle.y_center = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempCircle.radius = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempCircle.quadrants = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempCircle.filled = tempValue;
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempValue);
    }

    if (FR_OK == result) {
        tempCircle.activeState = tempValue;
        shapeFound = true;
    }

    if(!shapeFound) {
        if(result != FR_OK) {
            outResult = settings_load_file_error;
        } else {
            outResult = settings_load_corrupt;
        }
    } else {
        outResult = settings_load_ok;
        outStruct->type = display_type_circle;
        outStruct->circle = tempCircle;
    }

    return outResult;
}

/**
 * Loads a line that was determined to contain a gauge that is to
 * be displayed and stores it in a display_struct
 *
 * 6,x,y,min,max,radius,type,title,coarse_increment,fine_increment,print_coarse_values,pixel_state,pid
 *
 * 6				Character indicating that line contains a
 *						line item (Already read)
 * x				X-coordinate for the center of the gauge
 * y				Y-coordinate for the center of the gauge
 * min				Minimum value represented by the gauge
 * max				Maximum value represented by the gauge
 * radius			Radius of the gauge
 * type				Type of dial
 * title			Title to be displayed on the gauge
 * coarse_increment	Count between coarse increments on the gauge
 * fine_increment	Count between the fine increments on the gauge
 * print_coarse_values	Should the values of the coarse increments be printed?
 * pixel_state		Pixel state for active area gauge
 * pid				PID whose value is tracked by the gauge
 *
 * @param outStruct Where the read bar graph data is to be stored
 * @param inFile    File to read from
 * @param currGoal  Buffer to store read text in
 *
 * @return The success of the read and parse
 */
settings_load_result settings_load_gauge( display_struct *outStruct, FIL *inFile, char *currGoal )
{
    FRESULT result;
    settings_load_result outResult;
    bool shapeFound;
    gdm_analog_dial	tempDial;
    int tempInt;
    float tempFloat;
    char tempString[SETTING_MAX_STRING_LENGTH];

    shapeFound = false;
    /* Center X */
    result = settings_read_int(inFile, currGoal,
                               settings_hex_no, settings_negative_yes, &tempInt);
    if (FR_OK == result) {
        tempDial.centerX = tempInt;

        /* Center Y */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempInt);
    }

    if (FR_OK == result) {
        tempDial.centerY = tempInt;

        /* Minimum */
        result = settings_read_float(inFile, currGoal,
                                     settings_negative_yes, &tempFloat);
    }

    if (FR_OK == result) {
        tempDial.min = tempFloat;

        /* Maximum */
        result = settings_read_float(inFile, currGoal,
                                     settings_negative_yes, &tempFloat);
    }

    if (FR_OK == result) {
        tempDial.max = tempFloat;

        /* Radius */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_no, &tempInt);
    }

    if (FR_OK == result) {
        tempDial.radius = tempInt;

        /* Dial type */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_no, &tempInt);
    }

    if (FR_OK == result) {
        tempDial.dialType = tempInt;

        /* Dial title */
        result = settings_read_string(inFile, currGoal, tempString, GDM_TITLE_LENGTH);
    }

    if (FR_OK == result) {
        //tempDial.title = strdup(tempString);
        strncpy(tempDial.title, tempString, GDM_TITLE_LENGTH);

        /* Interval, Coarse */
        result = settings_read_float(inFile, currGoal,
                                     settings_negative_no, &tempFloat);
    }

    if (FR_OK == result) {
        tempDial.coarseInterval = tempFloat;

        /* Interval, Fine */
        result = settings_read_float(inFile, currGoal,
                                     settings_negative_no, &tempFloat);
    }

    if (FR_OK == result) {
        tempDial.fineInterval = tempFloat;

        /* Display coarse values */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_no, &tempInt);
    }

    if (FR_OK == result) {
        tempDial.displayCoarseValues = tempInt;

        /* Active pixel state */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_no, &tempInt);
    }

    if (FR_OK == result) {
        tempDial.activeState = tempInt;

        /* PID  */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_yes, settings_negative_no, &tempInt);
    }

    if (FR_OK == result) {
        fprintf_P(&usart_serial_stream, PSTR("PID Read, but not processed: %X\r\n"), tempInt);
        //tempDial.pid = tempValue;
        shapeFound = true;
    }

    if(!shapeFound) {
        if(result != FR_OK) {
            outResult = settings_load_file_error;
        } else {
            outResult = settings_load_corrupt;
        }
    } else {
        outResult = settings_load_ok;
        outStruct->type = display_type_gauge;
        outStruct->dial = tempDial;
    }

    return outResult;
}

/**
 * Loads a line that was determined to contain a bar graph that is
 * to be displayed and stores it in a display_struct
 *
 * Expected Format:
 * 7,x,y,min,max,length,thickness,orientation,border,pixel_state,pid
 *
 * 7			Character indicating that line contains a bar
 *					graph item (Already read)
 * x			X-coordinate for bottom left of bar graph
 * y			Y-coordinate for bottom left of bar graph
 * min			Minimum value represented by bar graph
 * max			Maximum value represented by bar graph
 * length		Length of the bar graph
 * thickness	Thickness of the bar graph
 * orientation	Orientation of the bar graph
 * border		Does the bar graph have a border?
 * pixel_state	Pixel state for active area of bar graph
 * pid	PID represented by the bar graph
 *
 * @param outStruct Where the read bar graph data is to be stored
 * @param inFile    File to read from
 * @param currGoal  Buffer to store read text in
 *
 * @return The success of the read and parse
 */
settings_load_result settings_load_bar_graph( display_struct *outStruct, FIL *inFile, char *currGoal )
{
    FRESULT result;
    settings_load_result outResult;
    bool shapeFound;
    gdm_bar_graph tempGraph;
    int tempInt;
    float tempFloat;

    shapeFound = false;

    /* X Coordinate */
    result = settings_read_int(inFile, currGoal,
                               settings_hex_no, settings_negative_yes, &tempInt);
    if (FR_OK == result) {
        tempGraph.coordX = tempInt;

        /* Y Coordinate */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempInt);
    }

    if (FR_OK == result) {
        tempGraph.coordY = tempInt;

        /* Minimum value */
        result = settings_read_float(inFile, currGoal,
                                     settings_negative_yes, &tempFloat);
    }

    if (FR_OK == result) {
        tempGraph.min = tempFloat;

        /* Maximum value */
        result = settings_read_float(inFile, currGoal,
                                     settings_negative_yes, &tempFloat);
    }

    if (FR_OK == result) {
        tempGraph.max = tempFloat;

        /* Bar length */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempInt);
    }

    if (FR_OK == result) {
        tempGraph.length = tempInt;

        /* Bar thickness */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempInt);
    }

    if (FR_OK == result) {
        tempGraph.thickness = tempInt;

        /* Bar orientation */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempInt);
    }

    if (FR_OK == result) {
        tempGraph.orientation = tempInt;

        /* Draw border? */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempInt);
    }

    if (FR_OK == result) {
        tempGraph.drawBorder = tempInt;

        /* Active pixel state */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_no, settings_negative_yes, &tempInt);
    }

    if (FR_OK == result) {
        tempGraph.activeState = tempInt;

        /* PID read */
        result = settings_read_int(inFile, currGoal,
                                   settings_hex_yes, settings_negative_no, &tempInt);
    }

    if (FR_OK == result) {
        //tempDial.pid = tempValue;
        fprintf_P(&usart_serial_stream, PSTR("PID Read, but not processed: %X\r\n"), tempInt);
        shapeFound = true;
    }

    if(!shapeFound) {
        if(result != FR_OK) {
            outResult = settings_load_file_error;
        } else {
            outResult = settings_load_corrupt;
        }
    } else {
        outResult = settings_load_ok;
        outStruct->type = display_type_bar_graph;
        outStruct->graph = tempGraph;
    }

    return outResult;
}

/**
 * Loads the final section of the file, the display
 *
 * @param inFile    File to read
 * @param currGoal  Pointer to location in memory to store the key
 *                  being looked for
 * @return	Result of the settings read
 */
settings_load_result settings_load_display( FIL *inFile, char *currGoal )
{

    settings_load_result outResult;
    char currChar;
    UINT bytesRead;
    FRESULT result;
    int tempInt;
//	display_types currentType;
//	char *numberTemp;
    display_struct readStruct;

    /* verify that it is a display line */
    snprintf_P(currGoal, SETTING_BUFFER_SIZE, PSTR(SETTING_KEY_DISPLAY));
    if(settings_look_for_string(inFile, currGoal, &outResult)) {
        setting_load_new_line(inFile);

        /* The settings display struct array is reset everytime the file is
         * loaded */
        settingsDisplayCount = 0;

//		numberTemp = currGoal;
        do {
            /* Read the next character */
            result = f_read(inFile, &currChar, 1, &bytesRead);

            /* The end of the file has been reached when 0 bytes are read */
            if(bytesRead == 0) {
                break;
            }

            /* Ignore comment lines */
            while(currChar == '#') {
                setting_load_new_line(inFile);
                result = f_read(inFile, &currChar, 1, &bytesRead);
            }

#ifdef LOCAL
            if((currChar >= '0') && (currChar <= '9')) {

                /* Add the char to the temp number holder */
                *numberTemp = currChar;
                numberTemp++;
            } else if ((currChar == ',')) {

                /* Convert the hex to a 16-bit value */
                *numberTemp = '\0';
                numberTemp = currGoal;

                currentType = atoi(numberTemp);

                fprintf_P(&usart_serial_stream, PSTR("Display Type: %u\r\n"), currentType);
                setting_load_new_line(inFile);
            }
#else
            *currGoal = currChar;
            settings_read_int(inFile, currGoal,
                              settings_hex_no, settings_negative_no, &tempInt);

            readStruct.type = display_type_none;
            switch(tempInt) {
            case display_type_text:
                settings_load_text(&readStruct, inFile, currGoal);
                break;

            case display_type_line:
                settings_load_line(&readStruct, inFile, currGoal);
                break;

            case display_type_rectangle:
                settings_load_rect(&readStruct, inFile, currGoal);
                break;

            case display_type_arc:
                settings_load_arc(&readStruct, inFile, currGoal);
                break;

            case display_type_circle:
                settings_load_circle(&readStruct, inFile, currGoal);
                break;

            case display_type_gauge:
                settings_load_gauge(&readStruct, inFile, currGoal);
                break;

            case display_type_bar_graph:
                settings_load_bar_graph(&readStruct, inFile, currGoal);
                break;

            default:
                setting_load_new_line(inFile);
                break;

            }

            /* Store the read display struct in an array for future use */
            if (readStruct.type != display_type_none) {
                if (settingsDisplayCount < SETTINGS_DISPLAY_ITEMS) {
                    memcpy(&settingsDisplayStructs[settingsDisplayCount],
                           &readStruct,
                           sizeof(display_struct));

                    gdm_debug_display_struct(&settingsDisplayStructs[settingsDisplayCount]);
                    display_draw_object(&settingsDisplayStructs[settingsDisplayCount]);

                    settingsDisplayCount++;
                }
            }
            setting_load_new_line(inFile);
#endif

        } while (result == FR_OK);

    } else {
        fputs_P(PSTR("Display string missing, assume nothing will be displayed on screen\r\n"), &usart_serial_stream);
    }

    return outResult;
}

/**
 * Loads a settings file
 *
 * Included in a settings file are the items to log to a file and items to
 * show on the display and how to display it
 */
settings_load_result settings_load_file(char *fileName)
{
    FIL settingsFile;
    FRESULT result;
    char *currGoal;
    settings_load_result outResult;
    int logVersion;

    currGoal = malloc(SETTING_BUFFER_SIZE);

    /* Open the file for reading only if it exists */
    result = f_open(&settingsFile, fileName, FA_READ | FA_OPEN_EXISTING);
    if(result != FR_OK) {
        return settings_load_file_error;
    }

    /* First line needs to indicate that this is a settings file */
    snprintf_P(currGoal, SETTING_BUFFER_SIZE, PSTR(SETTING_KEY_MAIN));
    if(settings_look_for_string(&settingsFile, currGoal, &outResult)) {
        fprintf_P(&usart_serial_stream, PSTR("File loaded successfully\r\n"));
        setting_load_new_line(&settingsFile);

        /* Second line contains the version number */
        if(settings_load_version(&settingsFile, &logVersion) == settings_load_ok) {
            fprintf_P(&usart_serial_stream, PSTR("Log Version: %u\r\n"), logVersion);

            /* The next line starts the logger section */
            settings_load_logger(&settingsFile, currGoal);
            //setting_load_new_line(&settingsFile);

            /* After the logger section is the LED bar section */
            settings_load_led_bar(&settingsFile, currGoal);
            //setting_load_new_line(&settingsFile);

            /* The final section in the file is the display section and will continue to
             * the end of the file */
            settings_load_display(&settingsFile, currGoal);
        } else {
            fprintf_P(&usart_serial_stream, PSTR("Failed to load version\r\n"));
        }
    } else {
        fprintf_P(&usart_serial_stream, PSTR("Failed to load log: %u\r\n"), outResult);
    }

    result = f_close(&settingsFile);
    if(result != FR_OK) {
        return settings_load_file_error;
    }

    return settings_load_ok;
}