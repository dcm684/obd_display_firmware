/*
 * TestRoutines.h
 *
 * Created: 9/7/2012 3:03:13 PM
 *  Author: Meyer
 */


#ifndef TESTROUTINES_H_
#define TESTROUTINES_H_

#include <stdio.h>
#include "OBD/OBD.h"

#include "TestRoutines/Test_CAN_OBD.h"
#include "TestRoutines/Test_GDM.h"

//obd_pid listOfPIDs[5];

void test_led_pattern(void);
void test_print_accel(void);
void test_print_time(void);

void test_nav_switch(void);

void test_sd_read(FILE *outStream);
void test_sd_create_read_delete(FILE *outStream);
void test_nav_accel_led(void);
void test_settings_load_file(void);
#endif /* TESTROUTINES_H_ */
