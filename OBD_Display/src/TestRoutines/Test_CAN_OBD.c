/*
 * Test_CAN_OBD.c
 *
 * Tests the functions related to the CAN and OBD systems
 *
 * Created: 9/22/2013 2:05:25 PM
 *  Author: Meyer
 */

#include "Test_CAN_OBD.h"
#include "../OBD_Display.h"

#include <delay.h>

#include "../drivers/MCP7940.h"
#include "../drivers/MCP2515.h"

#include "../drivers/USART_Handler.h"

#include "../OBD/OBD.h"
#include "../OBD/OBDPidFunctions.h"
#include "../OBD/OBDPidStrings.h"

#include "../drivers/GDM/GDM12864H.h"
#include "../drivers/GDM/GDM12864H_Text.h"
#include "../drivers/GDM/GDM12864H_Draw.h"

#include "../drivers/SDFAT/SDHandler.h"
#include "../drivers/SDFAT/diskio.h"
#include "../drivers/SDFAT/ff.h"
#include "../logger.h"

/**
 * Test the MCP2515 module to verify it is working.
 *
 * This is to be tested in loopback mode. This will also test if the
 * MCP2515 receive filters are working when used with
 * can_set_obd_filters(). Whenever the seconds is more than 8, the
 * value will get received.
 */
void test_can(void)
{
    CANMSG message;
    uint8_t addy;

    rtc_time time;

    rtc_get_time(&time);
    addy = 0x7E0 + time.second;

    message.adrsValue = addy;
    message.isExtendedAdrs = false;
    message.extendedAdrsValue = 0;
    message.rtr = false;
    message.dataLength = 8;

    message.data[0] = time.hour;
    message.data[1] = time.am_pm;
    message.data[2] = time.minute;
    message.data[3] = time.second;
    message.data[4] = 0x55;
    message.data[5] = 0x55;
    message.data[6] = 0x55;
    message.data[7] = 0x55;

    if(!can_transmit_message(can_tx_buffer_next, &message, 250)) {
        fputs_P(PSTR("TX Fail\r\n"), &usart_serial_stream);

    } else {
        fputs_P(PSTR("TX Success\r\n"), &usart_serial_stream);

        if(!can_receive_message(&message, 250)) {
            fputs_P(PSTR("RX Fail\r\n"), &usart_serial_stream);

        } else {
            if(message.adrsValue != addy) {
                fprintf_P(&usart_serial_stream,
                          PSTR("Address Mismatch %X != %X\r\n"),
                          message.adrsValue, addy);

            } else {
                if(time.second == message.data[3]) {
                    fprintf_P(&usart_serial_stream,
                              PSTR("Successful Transmit %u\r\n"),
                              time.second);

                } else {
                    fprintf_P(&usart_serial_stream,
                              PSTR("Message Mismatch %X != %X\r\n"),
                              time.second, message.data[3]);
                }
            }
        }
    }

    delay_ms(1000);
}

/**
 * Prints all information received from the CAN bus on USART
 */
void test_can_dump(void)
{
    uint8_t i;
    CANMSG message;

    if(can_receive_message(&message, 250)) {
        fprintf_P(&usart_serial_stream, PSTR("\t%X ("), message.adrsValue);

        for(i = 0; i < message.dataLength; i++) {
            fprintf_P(&usart_serial_stream, PSTR(" %X"), message.data[i]);
        }

        fputs_P(PSTR(")\r\n"), &usart_serial_stream);
    }
}

/**
 * Initializes the variables for the test that displays various OBD
 * values on both displays including bar graphs on the GDM
 */
void test_display_obd_init(void)
{
    bar_graph_a = malloc(sizeof(gdm_bar_graph));
    bar_graph_b = malloc(sizeof(gdm_bar_graph));

    obd_init_pid_list(&pidsToQuery);
    obd_add_pid_to_list(&pidsToQuery, pid_engine_rpm);
    obd_add_pid_to_list(&pidsToQuery, pid_fuel_level_input);
    obd_add_pid_to_list(&pidsToQuery, pid_vehicle_speed);
    obd_add_pid_to_list(&pidsToQuery, pid_engine_rpm);

    obd_send_multiple_requests(&pidsToQuery);

    bar_graph_a->activeState = pixel_on;
    bar_graph_a->coordX = 0;
    bar_graph_a->coordY = 35;
    bar_graph_a->drawBorder = true;
    bar_graph_a->min = 0;
    bar_graph_a->max = 50;
    bar_graph_a->orientation = horizontal_left;
    bar_graph_a->thickness = 10;
    bar_graph_a->length = 128;
    bar_graph_a->alreadyDrawn = false;

    bar_graph_b->activeState = pixel_on;
    bar_graph_b->coordX = 0;
    bar_graph_b->coordY = 50;
    bar_graph_b->drawBorder = true;
    bar_graph_b->min = 0;
    bar_graph_b->max = 8000;
    bar_graph_b->orientation = horizontal_left;
    bar_graph_b->thickness = 10;
    bar_graph_b->length = 128;
    bar_graph_b->alreadyDrawn = false;
}

/**
 * Reads the OBD port and displays several values including fuel
 * economy on the UART and GDM. On the GDM a bar graph will be
 * shown as well
 */
void test_display_obd(void)
{
    //uint8_t i;
    //uint32_t val;
    obd_value_with_unit fuelEcon;
    float miPerHr;
    float rpm;
    float fuel;
    //float engineFuelRate;

    if(multiple_pids_remaining == 0) {
        obd_send_multiple_requests(&pidsToQuery);
    }

    rpm = ((float) pidsToQuery.listOfPIDs[0].value) / 4;

    fuel = (((float) pidsToQuery.listOfPIDs[1].value) * 100) / 255;

    miPerHr = ((float) pidsToQuery.listOfPIDs[2].value) * 0.6214;

    if(!obd_get_fuel_economy(obd_unit_english, &fuelEcon)) {
        fputs_P(PSTR("FAILED to get mpg\r\n"), &usart_serial_stream);
    }

    bar_graph_a->value = (uint32_t)(fuelEcon.value + 0.5);
    gdm_draw_bar_graph(bar_graph_a);

    rpm = ((float) pidsToQuery.listOfPIDs[3].value) / 4;
    bar_graph_b->value = (uint32_t)(rpm + 0.5);

    gdm_draw_bar_graph(bar_graph_b);

    gdm_move_cursor(0, 0);
    fprintf_P(&gdm_serial_stream,
              PSTR("RPM:\t%4.0f\r\nMpH:\t%4.1f\r\nMPG:\t%4.1f\r\nFuel:\t%4.1f%%\r\n"),
              (double) rpm, (double) miPerHr, (double) fuelEcon.value, (double) fuel);

    fprintf_P(&usart_serial_stream,
              PSTR("RPM:\t%4.0f\r\nMpH:\t%4.1f\r\nMPG:\t%4.1f\r\nFuel:\t%4.1f%%\r\n"),
              (double) rpm, (double) miPerHr, (double) fuelEcon.value, (double) fuel);

    delay_ms(500);
}

/**
 * Tests the various PID logging functions. The SD card must already
 * be initialized prior to calling this function.
 *
 * Creates a logger, logs for 10 seconds, adds a pid, logs for
 * 10 more seconds, removes a pid, and logs for 10 more seconds before
 * returning. Each time this is called a new file will be created.
 */
void test_logger_obd(void)
{

    uint8_t pids[] = {pid_maf_rate, pid_engine_rpm, pid_vehicle_speed, pid_fuel_pressure};
    uint8_t i;
    obd_pid_list test_list;

    FRESULT result;

    allow_obd_int_processing = false;

    fputs_P(PSTR("<------------Start----------->\r\n"), &usart_serial_stream);
#if 1
    /* Disk must be mounted */
    result = f_mount(0, &sdFAT[0]);

    if(result != FR_OK) {
        fputs_P(PSTR("Mount Failed: "), &usart_serial_stream);
        put_rc(&usart_serial_stream, result);
        return;
    }

#endif

    /* Create the logger */
    if(3 != logger_init(&globalLogger, pids, 3, 1, &test_list)) {
        fputs_P(PSTR("Failed to create logger\r\n"), &usart_serial_stream);

    } else {
        fprintf_P(&usart_serial_stream, PSTR("\r\nFile Name: %s\r\n"\
                                             "Measurement Freq: %u00ms\r\n"\
                                             "Number of PIDs: %u\r\n"),
                  globalLogger.logFileName,
                  globalLogger.frequency,
                  globalLogger.numberOfPids);

        /* List all of the PIDs in the logger */
        for(i = 0; i < globalLogger.numberOfPids; i++) {
            fprintf(&usart_serial_stream, "A %2u: %2X\r\n",
                    i, globalLogger.pids[i]);
        }

        /* In retrospect there is no reason to add or remove PIDs from the
        logger once it has been initialized. If PIDs are added or removed
        a new logger file will need to be created since the column order
        will be changed and as a result the header will need reprinted */
#ifdef TEST_LOGGER_ADD_REMOVE
        /* Add a new PID */
        fprintf_P(&usart_serial_stream, PSTR("\r\nTrying to Add New PID %X\r\n"), pids[3]);
        logger_add_pid(&globalLogger, pids[3]);

        for(i = 0; i < globalLogger.numberOfPids; i++) {
            fprintf_P(&usart_serial_stream, PSTR("B %2u: %2X\r\n"),
                      i, globalLogger.pids[i]);
        }

        /* Add a PID that already is in the list to verify it won't be added */
        fprintf_P(&usart_serial_stream, PSTR("\r\nTrying to Add Old PID %X\r\n") pids[0]);
        logger_add_pid(&globalLogger, pids[0]);

        for(i = 0; i < globalLogger.numberOfPids; i++) {
            fprintf_P(&usart_serial_stream, PSTR("C %2u: %2X\r\n"),
                      i, globalLogger.pids[i]);
        }

        /* Remove the first PID from the logger */
        fprintf_P(&usart_serial_stream, PSTR("\r\nTrying to Remove PID %X\r\n"), pids[0]);
        logger_remove_pid(&globalLogger, pids[0]);

        for(i = 0; i < globalLogger.numberOfPids; i++) {
            fprintf_P(&usart_serial_stream, PSTR("D %2u: %2X\r\n"),
                      i, globalLogger.pids[i]);
        }

#endif /* TEST_LOGGER_ADD_REMOVE */

    }

    /* Add some values to the PID list and write them to the log */
    uint8_t j;

    for(i = 0; i < 5; i++) {
        /* Value is (time + index) */
        for(j = 0; j < test_list.length; j++) {
            test_list.listOfPIDs[j].value = i + j;
            test_list.listOfPIDs[j].rx_state = obd_received;
        }

        result = logger_write_line(&globalLogger, LOGGER_LINE_DATA);

        if(result != FR_OK) {
            put_rc(&usart_serial_stream, result);
        }

        //delay_s(1);
    }

    /* Print the contents of the file that was just created and
    populated */
    fprintf_P(&usart_serial_stream, PSTR("Contents of %s:\r\n"), globalLogger.logFileName);
    sd_dump_file_to_display(&usart_serial_stream, (globalLogger.logFileName));

    /* Delete the file */
    result = f_unlink((TCHAR *)(globalLogger.logFileName));

    if(result != FR_OK) {
        fputs_P(PSTR("File Unlink Failed: "), &usart_serial_stream);
        put_rc(&usart_serial_stream, result);

    } else {
        fputs_P(PSTR("File Deleted\r\n"),  &usart_serial_stream);
    }

    /* Delete the logger */
    logger_clear(&globalLogger);

    /* Unmount the drive */
    f_mount(0, NULL);

    fputs_P(PSTR("<------------Stop------------>\r\n"), &usart_serial_stream);

    allow_obd_int_processing = true;
}

/**
 * Tests the various various conversions used by OBD-II PIDs
 *
 * This does not check all cases for each PID, but provides a sanity check
 * for all used PIDs. The maximum, minimum, and an in between case is
 * checked. Due to the nature of floats there may be false negatives
 * because of rounding errors.
 */
void test_pid_get_metric(void)
{
    uint8_t tests;
    uint8_t i;

    obd_pid testPid;
    float outValue;

    typedef struct {
        uint8_t pid;
        uint32_t inValue;
        float	expectedValue;
    } tester;

    tester testerArray[] = {
        (tester) {pid_intake_manifold_pressure, 0, 0},
        (tester) {pid_intake_manifold_pressure, 200, 200},
        (tester) {pid_intake_manifold_pressure, 255, 255},

        (tester) {pid_engine_load, 0, 0},
        (tester) {pid_engine_load, 200, 78.4313725},
        (tester) {pid_engine_load, 255, 100},

        (tester) {pid_bank_1_short_term_trim, 0, -100},
        (tester) {pid_bank_1_short_term_trim, 127, -0.78125},
        (tester) {pid_bank_1_short_term_trim, 255, 99.21875},

        (tester) {pid_fuel_pressure, 0, 0},
        (tester) {pid_fuel_pressure, 10, 30},
        (tester) {pid_fuel_pressure, 255, 765},

        (tester) {pid_engine_rpm, 0, 0},
        (tester) {pid_engine_rpm, 40, 10},
        (tester) {pid_engine_rpm, 65535, 16383.75},

        (tester) {pid_timing_advance, 0, -64},
        (tester) {pid_timing_advance, 198, 35},
        (tester) {pid_timing_advance, 255, 63.5},

        (tester) {pid_intake_air_temp, 0, -40},
        (tester) {pid_intake_air_temp, 40, 0},
        (tester) {pid_intake_air_temp, 255, 215},

        (tester) {pid_maf_rate, 0, 0},
        (tester) {pid_maf_rate, 250, 2.5},
        (tester) {pid_maf_rate, 65535, 655.35},

        (tester) {pid_fuel_rail_pressure_manifold, 0, 0},
        (tester) {pid_fuel_rail_pressure_manifold, 100, 7.9},
        (tester) {pid_fuel_rail_pressure_manifold, 65535, 5177.265},

        (tester) {pid_fuel_rail_pressure_gauge, 0, 0},
        (tester) {pid_fuel_rail_pressure_gauge, 250, 2500},
        (tester) {pid_fuel_rail_pressure_gauge, 65535, 655350},

        (tester) {pid_egr_error, 0, -100},
        (tester) {pid_egr_error, 129, 0.78125},
        (tester) {pid_egr_error, 255, 99.21875},

        (tester) {pid_evap_sys_vapor_pressure_sm, 0x8000, -8192},
        (tester) {pid_evap_sys_vapor_pressure_sm, 202, 50.5},
        (tester) {pid_evap_sys_vapor_pressure_sm, 0x7FFF, 8191.75},

        (tester) {pid_catalyst_temp_b1_s1, 0, -40},
        (tester) {pid_catalyst_temp_b1_s1, 20, -38},
        (tester) {pid_catalyst_temp_b1_s1, 65535, 6513.5},

        (tester) {pid_control_module_volt, 0, 0},
        (tester) {pid_control_module_volt, 32767, 32.767},
        (tester) {pid_control_module_volt, 65535, 65.535},

        (tester) {pid_absolute_load, 0, 0},
        (tester) {pid_absolute_load, 32767, 12849.80392},
        (tester) {pid_absolute_load, 65535, 25700},

        (tester) {pid_cmd_equiv_ratio, 0, 0},
        (tester) {pid_cmd_equiv_ratio, 16384, 0.5},
        (tester) {pid_cmd_equiv_ratio, 65535, 1.999969},

        (tester) {pid_ambient_air_temp, 0, -40},
        (tester) {pid_ambient_air_temp, 20, -20},
        (tester) {pid_ambient_air_temp, 255, 215},

        (tester) {pid_max_maf, 0, 0},
        (tester) {pid_max_maf, 0x80000000, 1280},
        (tester) {pid_max_maf, 0xFF000000, 2550},


        (tester) {pid_evap_sys_vapor_pressure_abs, 0, 0},
        (tester) {pid_evap_sys_vapor_pressure_abs, 300, 1.5},
        (tester) {pid_evap_sys_vapor_pressure_abs, 65535, 327.675},

        (tester) {pid_evap_sys_vapor_pressure_big, 0, -32767},
        (tester) {pid_evap_sys_vapor_pressure_big, 32767, 0},
        (tester) {pid_evap_sys_vapor_pressure_big, 65535, 32768},

        (tester) {pid_fuel_rail_pressure_abs, 0, 0},
        (tester) {pid_fuel_rail_pressure_abs, 32768, 327680},
        (tester) {pid_fuel_rail_pressure_abs, 65535, 655350},

        (tester) {pid_engine_oil_temp, 0, -40},
        (tester) {pid_engine_oil_temp, 200, 160},
        (tester) {pid_engine_oil_temp, 255, 215},

        (tester) {pid_fuel_injection_timing, 0, -210},
        (tester) {pid_fuel_injection_timing, 32767, 45.9921875},
        (tester) {pid_fuel_injection_timing, 65535, 301.9921875},

        (tester) {pid_engine_fuel_rate, 0, 0},
        (tester) {pid_engine_fuel_rate, 32767, 1638.35},
        (tester) {pid_engine_fuel_rate, 65535, 3276.75},

        (tester) {pid_engine_torque_driver, 0, -125},
        (tester) {pid_engine_torque_driver, 120, -5},
        (tester) {pid_engine_torque_driver, 255, 130},

        (tester) {pid_engine_torque_actual, 0, -125},
        (tester) {pid_engine_torque_actual, 120, -5},
        (tester) {pid_engine_torque_actual, 255, 130},

        /* This is supposed to be bad. Done to verify code is actually
        finding bad values */
        (tester) {pid_engine_torque_actual, 0, 130},
        /* Verifies when an invalid PID is sent, false is returned */
        (tester)
        {
            255, 0, 130
        }
    };

    /* Matches the number of indices in the testerArray array */
    tests = 80;

    fputs_P(PSTR("\r\n\n\n\n-----Start-----\r\n"), &usart_serial_stream);

    for(i = 0; i < tests; i ++) {

        if(i % 3 == 0) {
            fputs_P(PSTR("\r\n"), &usart_serial_stream);
        }

        testPid.pid = testerArray[i].pid;
        testPid.value = testerArray[i].inValue;

        if(obd_get_metric_value(&testPid, &outValue)) {
            if(outValue == testerArray[i].expectedValue) {
                fputs_P(PSTR("Match"), &usart_serial_stream);

            } else {
                fputs_P(PSTR("\tNo Match"), &usart_serial_stream);
            }

            fprintf_P(&usart_serial_stream, PSTR("(%X %lu) %f => %f\r\n"),
                      testerArray[i].pid,
                      testerArray[i].inValue,
                      (double) testerArray[i].expectedValue,
                      (double) outValue);

        } else {
            fputs_P(PSTR("No value received\r\n"), &usart_serial_stream);
        }
    }

    fputs_P(PSTR("-----Completed-----\r\n"), &usart_serial_stream);
}

/**
 * Verifies that the correct value and unit are returned when translating
 * a PID.
 *
 * Checks both the English and Metric values. Only needs to check one
 * value for each unit type. Some units may be checked twice because
 * of how English and metric units are used
 *
 * Due to the nature of conversions and the width of a float, not all
 * values may match exactly with their expected values. Percent error
 * may be in the millionths. Since the values from the OBD are at most
 * 65535 the error are out of the significant figure region.
 */
void test_pid_translate(void)
{
    uint8_t i, j;
    obd_pid testPid;
    uint8_t numberOfTests;

    float calcError;

    obd_value_with_unit calcValue;

    typedef struct {
        uint8_t pid;
        uint32_t inValue;
        float outMetric;
        obd_units metricUnit;
        float outEnglish;
        obd_units englishUnit;
    } tester;

    tester testerArray[] = {
        (tester) {pid_supported_01_20,				200,	0,			obd_units_bitwise,		0,			obd_units_bitwise},
        (tester) {pid_o2_primary_bank_1_sensor_1,	200,	0,			obd_units_multiple,		0,			obd_units_multiple},
        (tester) {pid_warm_up_since_dtc_clear,		200,	200,		obd_units_none,			200,		obd_units_none},
        (tester) {pid_engine_load,					200,	78.4313725, obd_units_percent,		78.4313725, obd_units_percent},
        (tester) {pid_catalyst_temp_b1_s1,			20,		-38,		obd_units_metric_deg_c,	-36.4,		obd_units_eng_deg_f},
        (tester) {pid_fuel_rail_pressure_manifold,	100,	7.9,		obd_units_metric_kPa,	1.14579813,	obd_units_eng_psi},
        (tester) {pid_intake_manifold_pressure,		200,	200,		obd_units_metric_kPa,	59.06,		obd_units_eng_inHg},
        (tester) {pid_evap_sys_vapor_pressure_abs,	300,	1.5,		obd_units_metric_kPa,	6.02194618,	obd_units_eng_inH20},
        (tester) {pid_evap_sys_vapor_pressure_sm,	202,	50.5,		obd_units_metric_Pa,	0.20273885,	obd_units_eng_inH20},
        (tester) {pid_engine_rpm,					200,	50,			obd_units_rpm,			50,			obd_units_rpm},
        (tester) {pid_vehicle_speed,				200,	200,		obd_units_metric_km_h,	124.274238,	obd_units_eng_mph},
        (tester) {pid_timing_advance,				198,	35,			obd_units_degrees,		35,			obd_units_degrees},
        (tester) {pid_maf_rate,						250,	2.5,		obd_units_metric_g_s,	 0.3306934,	obd_units_eng_lb_min},
        (tester) {pid_runtime_since_engine_start,	200,	200,		obd_units_seconds,		200,		obd_units_seconds},
        (tester) {pid_time_run_with_mil,			200,	200,		obd_units_minutes,		200,		obd_units_minutes},
        (tester) {pid_dist_with_mil_on,				200,	200,		obd_units_metric_km,	124.274238,	obd_units_eng_miles},
        (tester) {pid_control_module_volt,			200,	0.2,		obd_units_volts,		0.2,		obd_units_volts},
        (tester) {pid_engine_fuel_rate,				200,	10,			obd_units_metric_L_h,	2.64172052,	obd_units_eng_ga_h},
        (tester) {pid_engine_torque_ref,			200,	200,		obd_units_metric_Nm,	147.512430,	obd_units_eng_ftlb},
        /* Remaining values are to ensuring that bad values are rejected */
        (tester) {0xFF,								200,	0,			obd_units_invalid,		0,			obd_units_invalid},
        (tester) {pid_engine_torque_ref,			200,	200,		obd_units_metric_Nm,	0.1,		obd_units_eng_ftlb}, /* Bad english */
        (tester) {pid_engine_torque_ref,			200,	0.1,		obd_units_metric_Nm,	147.512430,	obd_units_eng_ftlb}, /* Bad metric */
    };

    numberOfTests = 22;

    for(i = 0; i < numberOfTests; i++) {
        testPid.pid = testerArray[i].pid;
        testPid.value = testerArray[i].inValue;

        /* Loop through both unit types */
        for(j = 0; j < 2; j++) {
            if(j == 0) {
                testPid.unit_type = obd_unit_metric;

            } else {
                testPid.unit_type = obd_unit_english;
            }

            fprintf_P(&usart_serial_stream, PSTR("%u %u"), i, testPid.pid);

            /* Test the values. Only valid values will be checked */
            if(obd_translate_pid(&testPid, &calcValue)) {
                fputs_P(PSTR("V "), &usart_serial_stream);

                if(testPid.unit_type == obd_unit_metric) {

                    /* This could cause division by zero, but since this is
                    a testing routine you can see the code and see this
                    warning. Don't be a moron! */
                    calcError = calcValue.value - testerArray[i].outMetric;
                    calcError = calcError / testerArray[i].outMetric;
                    calcError = abs(calcError) * 100;

                    /* Any less than 0.001 and it is probably a rounding error */
                    if(calcError > 0.001) {
                        fputs_P(PSTR("MV X"), &usart_serial_stream);

                    } else {
                        fputs_P(PSTR("MV  "), &usart_serial_stream);
                    }

                    fprintf_P(&usart_serial_stream, PSTR(" %f\t=>\t%f\t(%.3f%%)\t"),
                              (double) testerArray[i].outMetric,
                              (double) calcValue.value,
                              (double) calcError);

                } else {

                    /* This could cause division by zero, but since this is
                    a testing routine you can see the code and see this
                    warning. Don't be a moron! */
                    calcError = calcValue.value - testerArray[i].outEnglish;
                    calcError = calcError / testerArray[i].outEnglish;
                    calcError = abs(calcError) * 100;

                    /* Any less than 0.001 and it is probably a rounding error */
                    if(calcError > 0.001) {
                        fputs_P(PSTR("EV X"), &usart_serial_stream);

                    } else {
                        fputs_P(PSTR("EV  "), &usart_serial_stream);
                    }

                    fprintf_P(&usart_serial_stream, PSTR(" %f\t=>\t%f\t(%.3f%%)\t"),
                              (double) testerArray[i].outEnglish,
                              (double) calcValue.value,
                              (double) calcError);
                }

            } else {
                fputs_P(PSTR("I "), &usart_serial_stream);
            }

            /* Test the units. Even if values are invalid their unit
            will be verified */
            if(testPid.unit_type == obd_unit_metric) {
                if(calcValue.unit != testerArray[i].metricUnit) {
                    fputs_P(PSTR("MU X"), &usart_serial_stream);

                } else {
                    fputs_P(PSTR("MU  "), &usart_serial_stream);
                }

                fprintf_P(&usart_serial_stream, PSTR(" %S"),
                          (wchar_t *)(obdUnitStrings[testerArray[i].metricUnit]));

            } else {
                if(calcValue.unit != testerArray[i].englishUnit) {
                    fputs_P(PSTR("EU X"), &usart_serial_stream);

                } else {
                    fputs_P(PSTR("EU  "), &usart_serial_stream);
                }

                fprintf_P(&usart_serial_stream, PSTR(" %S"),
                          (wchar_t *)(obdUnitStrings[testerArray[i].englishUnit]));
            }

            fprintf_P(&usart_serial_stream, PSTR(" => %S\r\n"),
                      (wchar_t *)(obdUnitStrings[calcValue.unit]));
        }

        fputs_P(PSTR("\r\n"), &usart_serial_stream);
    }
}