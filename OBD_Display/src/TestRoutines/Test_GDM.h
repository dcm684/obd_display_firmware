/*
 * Test_GDM.h
 *
 * Tests the graphical display module and its related function
 *
 * Created: 9/22/2013 1:58:22 PM
 *  Author: Meyer
 */


#ifndef TEST_GDM_H_
#define TEST_GDM_H_

void test_gdm_pixels(void);
void test_gdm_text_scroll(void);
void test_gdm_text_toggle_row(void);
void test_gdm_draw_shapes(void);
void test_gdm_draw_bar_graph(void);
void test_gdm_draw_gauges(void);
void test_gdm_text_pixel_states(void);
void test_gdm_struct(void);
void test_gdm_draw_object(void);
#endif /* TEST_GDM_H_ */