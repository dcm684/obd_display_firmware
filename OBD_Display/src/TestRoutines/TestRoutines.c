/*
 * TestRoutines.c
 *
 * All of the various functions that were created to test each of the
 * individual modules during the development process are here.
 *
 * Created: 9/7/2012 2:35:11 PM
 *  Author: Meyer
 */
#include <avr/pgmspace.h>
#include <string.h>
#include <math.h>

#include "TestRoutines.h"
#include "../OBD_Display.h"

#include "../drivers/LED_Bar.h"
#include "../drivers/MCP7940.h"
#include "../drivers/MMA7361.h"
#include "../drivers/USB_Handler.h"
#include "../drivers/Nav_Switch.h"
#include "../drivers/USART_Handler.h"

#include "drivers/GDM/GDM12864H_Text.h"

#include "../drivers/SDFAT/SDHandler.h"
#include "../drivers/SDFAT/diskio.h"
#include "../drivers/SDFAT/ff.h"

#include "Test_GDM.h"
#include "Test_CAN_OBD.h"

#include "../logger.h"
#include "SettingsManager.h"


/* Cycle through LED bar */
void test_led_pattern(void)
{
    int16_t ledData;

    led_bar_set_pattern(0, 15, LED_PATTERN_RADIATE | LED_PATTERN_FILL);

    for(ledData = 0; ledData < 16; ledData++) {
        led_bar_set_value(&ledData);
        delay_ms(50);
    }
}

/**
 * Print all accelerometer axis values on USART
 */
void test_print_accel(void)
{
#if 0
    fprintf_P(&usart_serial_stream, PSTR("X: %u (%u)\tY: %u (%u)\tZ: %u (%u)\r\n"),
              accelChans.axis_x.value,
              accelChans.axis_x.rawValue,
              accelChans.axis_y.value,
              accelChans.axis_y.rawValue,
              accelChans.axis_z.value,
              accelChans.axis_z.rawValue);
#else
    fprintf_P(&usart_serial_stream, PSTR("g X: %u (%i)\tY: %u (%i)\tZ: %u (%i)\r\n"),
              accelChans.axis_x.value,
              accel_calc_in_g(&(accelChans.axis_x)),
              accelChans.axis_y.value,
              accel_calc_in_g(&(accelChans.axis_y)),
              accelChans.axis_z.value,
              accel_calc_in_g(&(accelChans.axis_z)));
#endif
}

/**
 * Print the time and date on USART
 */
void test_print_time(void)
{
    rtc_time time;
    rtc_date date;

    rtc_get_time(&time);
    rtc_get_date(&date);
    fprintf_P(&usart_serial_stream, PSTR("%u/%u/%u %u:%u:%u"),
              date.year,
              date.month,
              date.date,
              time.hour,
              time.minute,
              time.second);

    if(time.am_pm != RTC_NO_AMPM) {
        fprintf_P(&usart_serial_stream, PSTR(" %s"),
                  ((time.am_pm == RTC_AM) ? "AM" : "PM"));
    }
}

/**
 * Indicates the current states of each of the NAV switch directions
 * and that of the select
 */
void test_nav_switch(void)
{
    uint8_t i;
    nav_button_state currentButton;

    nav_check();
    gdm_move_cursor(0, 0);

    for(i = 0; i < 5; i++) {
        gdm_text_set_current_row_value(pixel_off);

        switch(i) {
        case 0:
            gdm_puts_P(PSTR("UP: "));
            currentButton = nav_all_switches.up;
            break;

        case 1:
            gdm_puts_P(PSTR("RIGHT: "));
            currentButton = nav_all_switches.right;
            break;

        case 2:
            gdm_puts_P(PSTR("DOWN: "));
            currentButton = nav_all_switches.down;
            break;

        case 3:
            gdm_puts_P(PSTR("LEFT: "));
            currentButton = nav_all_switches.left;
            break;

        case 4:
            gdm_puts_P(PSTR("SELECT: "));
            currentButton = nav_all_switches.select;
            break;

        default:
            currentButton = NAV_OFF;
            break;
        }

        if(currentButton == NAV_PRESSED) {
            gdm_puts_P(PSTR("Pressed"));

        } else if(currentButton == NAV_HELD) {
            gdm_puts_P(PSTR("Held"));

        } else if(currentButton == NAV_RELEASED) {
            gdm_puts_P(PSTR("Released"));

        } else if(currentButton == NAV_OFF) {
            gdm_puts_P(PSTR("Off"));
        }

        gdm_puts_P(PSTR("\r\n"));
    }
}

/**
 * User can select which accelerometer axis to display on the LED
 * bar by pressing the Nav switch
 */
#if 0 /* Needs global, axis_used, which is in an #if 0 block as well */
void test_nav_accel_led(void)
{
    int16_t ledData;
    /* Use LED bar to display acceleration */
    led_bar_set_pattern(200, 1840, LED_PATTERN_CENTERED, false, true);

    if(axis_used == 0) {
        ledData = accelChans.axis_x.value;

    } else if(axis_used == 1) {
        ledData = accelChans.axis_y.value;

    } else {
        ledData = accelChans.axis_z.value;
    }

    //fprintf(&usart_serial_stream, "\r\n%i", ledData);
    led_bar_set_value(&ledData);

    /* Nav used to select accel axis */
    if(nav_check() & NAV_PRESS_SELECT) {
        if(nav_all_switches.select == NAV_PRESSED) {
            axis_used++;

            if(axis_used > 2) {
                axis_used = 0;
            }

            fprintf_P(&usart_serial_stream, PSTR("Pressed %u"), axis_used);
            ledData = _BV(axis_used);
            led_bar_set_leds(&ledData);
            delay_ms(100);
        }
    }
}
#endif


/**
 * Reads a file on a FAT32 formatted SD card and prints it on the given
 * stream
 *
 * /param outStream Where to print the debug text
 */
void test_sd_read(FILE *outStream)
{
    //gdm_move_cursor(0,0);
    fprintf_P(outStream, PSTR("\r\n\n%lu\r\n"), get_fattime());
    sd_dump_file_to_display(outStream, "Read.txt");
    delay_ms(1000);
}

/**
 * Create a file with the time as the name then add the current time
 * to the contents, close it. Then open the file and read it. Then
 * delete it.
 *
 * The SD card must already be initialized prior to calling this function.
 *
 * /param outStream Where to print the debug text
 */
void test_sd_create_read_delete(FILE *outStream)
{
    FRESULT result;
    FIL theFile;
    char *fileName;
    char *simpleString;
    uint16_t byteCount;
    UINT stringSize;
    uint16_t filePosition;

    fileName = malloc(14);
    simpleString = malloc(55);

    sprintf_P(fileName, PSTR("/%lu.txt"), get_fattime());
    fprintf_P(outStream, PSTR("\r\n\nFile Name is %s\r\n"), fileName);

    /* Disk must be mounted */
    result = f_mount(0, &sdFAT[0]);

    if(result != FR_OK) {
        fputs_P(PSTR("Mount Failed: "), outStream);
        put_rc(outStream, result);
        return;
    }

#if 1
    /* Once mounted, the file can be opened. Write only and
    overwrite the file if it already exists */
    result = f_open(&theFile, (TCHAR *) fileName, FA_WRITE | FA_CREATE_ALWAYS);

    if(result != FR_OK) {
        fputs_P(PSTR("Open for Write Failed: "), outStream);
        put_rc(outStream, result);
        return;
    }

    /* Create a simple string to add to the file */
    sprintf_P(simpleString, PSTR("This is a simple string, %s\r\n"), fileName);

    /* The file starts off being too short, so it needs to be expanded */
    filePosition = f_tell(&theFile); /* Get the starting point */
    result = f_lseek(&theFile, filePosition + strlen(simpleString));

    /* verify the expansion worked */
    if((result == FR_OK) && (f_tell(&theFile) ==
                             filePosition + strlen(simpleString))) {

        result = f_lseek(&theFile, filePosition); /* Move back to start */

        /* Add the simple string to the file */
        //stringSize = f_puts((TCHAR *) simpleString, &theFile);
        f_write(&theFile, simpleString, strlen(simpleString), &stringSize);

        if(stringSize != strlen(simpleString)) {
            fputs_P(PSTR("Writing a String Failed\r\n"), outStream);
        }

        f_truncate(&theFile); /* Strip off unused space */

    } else {
        fputs_P(PSTR("File Size Allocation Failed: "), outStream);
        put_rc(outStream, result);
        return;
    }

    /* You are done writing the file. It is time to test closing the
    file */
    result = f_close(&theFile);

    if(result != FR_OK) {
        fputs_P(PSTR("Post Write Close Failed: "), outStream);
        put_rc(outStream, result);
        return;
    }

#endif

#if 1
    /* Now open the file to append some text to the end of it */
    result = f_open(&theFile, (TCHAR *) fileName, FA_WRITE | FA_OPEN_ALWAYS);

    if(result != FR_OK) {
        fputs_P(PSTR("Open for Append Failed: "), outStream);
        put_rc(outStream, result);
        return;
    }

    /* Now move the pointer to the write location to the end of the file */
    result = f_lseek(&theFile, f_size(&theFile));

    if(result != FR_OK) {
        fputs_P(PSTR("Moving the cursor failed: "), outStream);
        put_rc(outStream, result);
        return;
    }


    /* Add some bytes to the file that just happen to be chars. */
    result = f_write(&theFile, fileName, strlen(fileName), &byteCount);

    if(result != FR_OK) {
        fputs_P(PSTR("Writing a String Failed: "), outStream);
        put_rc(outStream, result);

    } else if(byteCount != strlen(fileName)) {
        fputs_P(PSTR("Number of bytes written during append is invalid\r\n"), outStream);
    }

    /* You are done appending to the file. It is time to test closing the
    file */
    result = f_close(&theFile);

    if(result != FR_OK) {
        fputs_P(PSTR("Post Append Close Failed: "), outStream);
        put_rc(outStream, result);
        return;
    }

#endif

    /* Now dump the file to the stream */
    fputs_P(PSTR("Contents of the File: \r\n"), outStream);
    result = sd_dump_file_to_display(outStream, fileName);

    if(result != FR_OK) {
        fputs_P(PSTR("File dump failed: "), outStream);
        put_rc(outStream, result);
    }

    fputs("\r\n", outStream);

    /* Give the user a chance to eject the card before it is deleted */
    fprintf_P(outStream, PSTR("About to delete \"%s\"\r\n"), fileName);
    delay_s(2);
    result = f_unlink((TCHAR *) fileName);

    if(result != FR_OK) {
        fputs_P(PSTR("File Unlink Failed: "), outStream);
        put_rc(outStream, result);
        return;

    } else {
        fputs_P(PSTR("File Deleted\r\n"), outStream);
    }

    /* Unmount the drive */
    f_mount(0, NULL);
}

/**
 * Test the settings loader
 */
void test_settings_load_file(void)
{
    settings_load_result result;

    fputs_P(PSTR("Test loading TEST_SETTINGS.OBS\r\n"), &usart_serial_stream);
    result = settings_load_file("TEST_SETTINGS.OBS");

    fprintf_P(&usart_serial_stream, PSTR("Load Result: %u\r\n"), result);
}