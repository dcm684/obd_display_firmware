/*
 * Test_CAN_OBD.h
 *
 * Created: 9/22/2013 2:06:01 PM
 *  Author: Meyer
 */


#ifndef TEST_CAN_OBD_H_
#define TEST_CAN_OBD_H_

void test_can(void);
void test_can_dump(void);

void test_display_obd_init(void);
void test_display_obd(void);
void test_logger_obd(void);
void test_pid_get_metric(void);
void test_pid_translate(void);

#endif /* TEST_CAN_OBD_H_ */