/*
 * Test_GDM.c
 *
 * Created: 9/22/2013 1:58:07 PM
 *  Author: Meyer
 */
#include "Test_GDM.h"

#include <avr/pgmspace.h>
#include <string.h>

#include "delay.h"

#include "../drivers/GDM/GDM12864H.h"
#include "../drivers/GDM/GDM12864H_Text.h"
#include "../drivers/GDM/GDM12864H_Draw.h"
#include "Display.h"

/**
 * Turns all of the pixels on the display on the back off
 */
void test_gdm_pixels(void)
{
    uint8_t x, y;

    for(x = 0; x < 128; x++) {
        for(y = 0; y < 64; y++) {
            gdm_set_pixel_level(x, y, true);
            gdm_flush_cache();
        }
    }

    delay_ms(200);

    for(x = 0; x < 128; x++) {
        for(y = 0; y < 64; y++) {
            gdm_set_pixel_level(x, y, false);
            gdm_flush_cache();
        }
    }

    delay_ms(200);
}

/**
 * Verifies that the text scrolls on the GDM
 */
void test_gdm_text_scroll(void)
{
    gdm_puts_P(PSTR("ABCDEFGHIJKLMNOPQRSTUVWXYZ"));
    gdm_puts_P(PSTR("abcdefghijklmnopqrstuvwxyz"));
    gdm_puts_P(PSTR("1234567890"));
    gdm_puts_P(PSTR("!@#$%^&*()"));
}

/**
 * Clears an entire row of text first by clearing all pixels in the
 * row and the sets all of the pixels in the row.
 */
void test_gdm_text_toggle_row(void)
{
    gdm_text_set_current_row_value(pixel_on);
    delay_ms(200);
    gdm_text_set_current_row_value(pixel_off);
    delay_ms(200);
}

void test_gdm_text_pixel_states(void)
{
    gdm_move_cursor(0, 0);

    /* Text is on while the background in off */
    gdm_text_set_state(pixel_on);
    gdm_text_set_current_row_value(pixel_off);
    gdm_puts_P(PSTR("Pixels are on.\r\n"));

    /* Text should be off with on background */
    gdm_text_set_state(pixel_off);
    gdm_text_set_current_row_value(pixel_off);
    gdm_puts_P(PSTR("Pixels are off.\r\n"));

    /* Test should be off with on background */
    gdm_text_set_state(pixel_invert);
    gdm_text_set_current_row_value(pixel_on);
    gdm_puts_P(PSTR("Pixels are inverted.\r\n"));

    /* Text should be solid with background inverted */
    gdm_text_set_state(pixel_nochange);
    gdm_text_set_current_row_value(pixel_on);
    gdm_puts_P(PSTR("Pixels are unchanged.\r\n"));

    /* Text should be off with background inverted */
    gdm_text_set_state(pixel_nochange);
    gdm_text_set_current_row_value(pixel_off);
    gdm_puts_P(PSTR("Pixels are unchanged.\r\n"));

    delay_ms(500);
}

/**
 * Draw various shapes on the GDM
 */
void test_gdm_draw_shapes(void)
{
    uint8_t r;

    for(r = 15; r < 32; r++) {
        gdm_clear_display();
        gdm_draw_rectangle(0, 0, 2 * r, 2 * r, true, pixel_on);
        /*gdm_draw_circle(r, r, r, gdm_quadrant_ne, false, pixel_invert);
        gdm_draw_circle(r, r, r, gdm_quadrant_se, true, pixel_invert);
        gdm_draw_circle(r, r, r, gdm_quadrant_sw, false, pixel_invert);
        gdm_draw_circle(r, r, r, gdm_quadrant_nw, true, pixel_invert);*/
        gdm_draw_circle(r, r, r, gdm_quadrant_ne | gdm_quadrant_sw, true, pixel_invert);

        gdm_draw_arc(96, 32, r / 2,		-270, 0, pixel_on);
        gdm_draw_arc(96, 32, r / 2 + 5,	0, 270, pixel_on);
        gdm_draw_line(0, 2 * r, 2 * r, 0, pixel_on);
        delay_ms(100);
    }
}

/**
 * Draws a vertical and a horizontal bar graph and goes through
 * different ranges
 */
void test_gdm_draw_bar_graph(void)
{
    uint8_t counter;
    uint32_t value;
    uint8_t thickness;

    thickness = 25;

    gdm_bar_graph bar_horizontal_border;
    gdm_bar_graph bar_horizontal_none;
    gdm_bar_graph bar_vertical_border;
    gdm_bar_graph bar_vertical_none;

    bar_horizontal_border.activeState = pixel_on;
    bar_horizontal_border.coordX = 0;
    bar_horizontal_border.coordY = 0;
    bar_horizontal_border.drawBorder = true;
    bar_horizontal_border.min = 0;
    bar_horizontal_border.max = 127;
    bar_horizontal_border.orientation = horizontal_left;
    bar_horizontal_border.thickness = thickness;
    bar_horizontal_border.length = 63;
    bar_horizontal_border.alreadyDrawn = false;

    bar_horizontal_none.activeState = pixel_on;
    bar_horizontal_none.coordX = 0;
    bar_horizontal_none.coordY = 32;
    bar_horizontal_none.drawBorder = false;
    bar_horizontal_none.min = 0;
    bar_horizontal_none.max = 127;
    bar_horizontal_none.orientation = horizontal_right;
    bar_horizontal_none.thickness = thickness;
    bar_horizontal_none.length = 63;
    bar_horizontal_none.alreadyDrawn = false;

    bar_vertical_border.activeState = pixel_on;
    bar_vertical_border.coordX = 64;
    bar_vertical_border.coordY = 0;
    bar_vertical_border.drawBorder = true;
    bar_vertical_border.min = 0;
    bar_vertical_border.max = 127;
    bar_vertical_border.orientation = vertical_top;
    bar_vertical_border.thickness = thickness;
    bar_vertical_border.length = 64;
    bar_vertical_border.alreadyDrawn = false;

    bar_vertical_none.activeState = pixel_on;
    bar_vertical_none.coordX = 96;
    bar_vertical_none.coordY = 0;
    bar_vertical_none.drawBorder = false;
    bar_vertical_none.min = 0;
    bar_vertical_none.max = 127;
    bar_vertical_none.orientation = vertical_bottom;
    bar_vertical_none.thickness = thickness;
    bar_vertical_none.length = 64;
    bar_vertical_none.alreadyDrawn = false;

    value = 0;

    for(counter = 0; counter < 255; counter++) {
        bar_horizontal_border.value = value;
        bar_horizontal_none.value = value;
        bar_vertical_border.value = value;
        bar_vertical_none.value = value;

        gdm_draw_bar_graph(&bar_horizontal_border);
        gdm_move_cursor(2, 3);
        fprintf_P(&gdm_serial_stream, PSTR("%3lu"), value);
        gdm_draw_bar_graph(&bar_horizontal_none);
        gdm_draw_bar_graph(&bar_vertical_border);
        gdm_draw_bar_graph(&bar_vertical_none);

        if(counter == 128) {
            delay_ms(500);
        }

        if(counter > 127) {
            value--;

        } else {
            value++;
        }

        //delay_ms(250);
    }

    delay_ms(500);

}

void test_gdm_draw_gauges(void)
{
    gdm_analog_dial dial_full;
    //analogDial dial_3_4;
    //analogDial dial_top;
    //analogDial dial_bottom;
    //analogDial dial_left;
    //analogDial dial_right;

    int32_t dummyValue;

    dial_full.min = 0;
    dial_full.max = 100;
    dial_full.centerX = 30;
    dial_full.centerY = 20;
    dial_full.radius = 20;
    dial_full.dialType = semicircle_left;
    dial_full.activeState = pixel_on;
    strcpy(dial_full.title, "Awesomeness");
    dial_full.displayTitle = true;
    dial_full.coarseInterval = 10;
    dial_full.fineInterval = 5;
    dial_full.displayCoarseValues = true;
    dial_full.alreadyDrawn = false;
    dial_full.lastValue = 0;

    gdm_clear_display();

    /*gdm_draw_circle(20,
    				20,
    				20,
    				GDM_CIRCLE_FULL,
    				false,
    				pixel_on);
    delay_ms(250);
    gdm_draw_arc(20,
    			20,
    			20,
    			0,
    			360,
    			pixel_invert);
    delay_ms(250);*/

    for(dummyValue = -20; dummyValue < 120; dummyValue += 2) {
        dial_full.value = dummyValue;
        gdm_draw_dial(&dial_full);
        delay_ms(25);
    }
}

/**
 * Tests the struct versions of drawing the various shapes, graphs, and
 * text
 */
void test_gdm_struct(void)
{
    uint8_t r;
    uint8_t initTextRow;
    gdm_rectangle theRectangle;
    gdm_circle theCircle;
    gdm_arc arcOne;
    gdm_arc arcTwo;
    gdm_line theLine;
    gdm_text theText;

    theRectangle.x1 = 0;
    theRectangle.y1 = 0;
    theRectangle.x2 = 0;
    theRectangle.y2 = 0;
    theRectangle.filled = true;
    theRectangle.activeState = pixel_on;

    theCircle.x_center = 0;
    theCircle.y_center = 0;
    theCircle.radius = 0;
    theCircle.quadrants = gdm_quadrant_ne | gdm_quadrant_sw;
    theCircle.filled = true;
    theCircle.activeState = pixel_invert;

    arcOne.x_center = 96;
    arcOne.y_center = 32;
    arcOne.radius = 0;
    arcOne.start_angle = -270;
    arcOne.stop_angle = 0;
    arcOne.activeState = pixel_on;

    arcTwo.x_center = 96;
    arcTwo.y_center = 32;
    arcTwo.radius = 0;
    arcTwo.start_angle = 0;
    arcTwo.stop_angle = 270;
    arcTwo.activeState = pixel_on;

    theLine.x1 = 0;
    theLine.y1 = 0;
    theLine.x2 = 0;
    theLine.y2 = 0;
    theLine.activeState = pixel_on;

    theText.font = activeFont;
    theText.length = 16;
    theText.string = "test_gdm_struct";
    theText.wordWrap = true;
    theText.column = 0;
    theText.row = 0;

    for(r = 15; r < 32; r++) {
        gdm_clear_display();

        /* On Square top left to 2*r, 2*r */
        theRectangle.x2 = 2 * r;
        theRectangle.y2 = 2 * r;
        gdm_draw_rectangle_struct(&theRectangle);

        /* Inverted NE and SW of circle (clear) centered in square */
        theCircle.x_center = r;
        theCircle.y_center = r;
        theCircle.radius = r;
        gdm_draw_circle_struct(&theCircle);

        /* On arc with r/2 radius. 6 o'clock (-270) to 3 o'clock (0) CW */
        arcOne.radius = r / 2;
        gdm_draw_arc_struct(&arcOne);

        /* On arc with r/2 + 5 radius. 3 o'clock (0) to 12 o'clock (270) CW */
        arcTwo.radius = r / 2 + 5;
        gdm_draw_arc_struct(&arcTwo);

        /* On Line from top right to bottom left of square */
        theLine.x1 = 2 * r;
        theLine.y2 = 2 * r;
        gdm_draw_line_struct(&theLine);

        /* Print the test name with an x offset of r */
        theText.column = r - 15;
        gdm_text_print_struct(&theText);

        /* Print it again lower on the screen and without word wrap */
        initTextRow = theText.row;
        theText.wordWrap = false;
        theText.row = 6;
        gdm_text_print_struct(&theText);
        theText.row = initTextRow;
        theText.wordWrap = true;

        delay_ms(500);
    }
}

/**
 * Tests the function that draws any shape stored in a display_struct
 */
void test_gdm_draw_object(void)
{
    uint8_t r;
    uint8_t initTextRow;

    display_struct theRectangle;
    display_struct theCircle;
    display_struct arcOne;
    display_struct arcTwo;
    display_struct theLine;
    display_struct theText;

    theRectangle.type = display_type_rectangle;
    theRectangle.rectangle.x1 = 0;
    theRectangle.rectangle.y1 = 0;
    theRectangle.rectangle.x2 = 0;
    theRectangle.rectangle.y2 = 0;
    theRectangle.rectangle.filled = true;
    theRectangle.rectangle.activeState = pixel_on;

    theCircle.type = display_type_circle;
    theCircle.circle.x_center = 0;
    theCircle.circle.y_center = 0;
    theCircle.circle.radius = 0;
    theCircle.circle.quadrants = gdm_quadrant_ne | gdm_quadrant_sw;
    theCircle.circle.filled = true;
    theCircle.circle.activeState = pixel_invert;

    arcOne.type = display_type_arc;
    arcOne.arc.x_center = 96;
    arcOne.arc.y_center = 32;
    arcOne.arc.radius = 0;
    arcOne.arc.start_angle = -270;
    arcOne.arc.stop_angle = 0;
    arcOne.arc.activeState = pixel_on;

    arcTwo.type = display_type_arc;
    arcTwo.arc.x_center = 96;
    arcTwo.arc.y_center = 32;
    arcTwo.arc.radius = 0;
    arcTwo.arc.start_angle = 0;
    arcTwo.arc.stop_angle = 270;
    arcTwo.arc.activeState = pixel_on;

    theLine.type = display_type_line;
    theLine.line.x1 = 0;
    theLine.line.y1 = 0;
    theLine.line.x2 = 0;
    theLine.line.y2 = 0;
    theLine.line.activeState = pixel_on;

    theText.type = display_type_text;
    theText.text.font = activeFont;
    theText.text.length = 0;
    theText.text.string = "test_gdm_draw_object";
    theText.text.wordWrap = true;
    theText.text.column = 0;
    theText.text.row = 0;

    for(r = 15; r < 32; r++) {
        gdm_clear_display();

        /* On Square top left to 2*r, 2*r */
        theRectangle.rectangle.x2 = 2 * r;
        theRectangle.rectangle.y2 = 2 * r;
        display_draw_object(&theRectangle);

        /* Inverted NE and SW of circle (clear) centered in square */
        theCircle.circle.x_center = r;
        theCircle.circle.y_center = r;
        theCircle.circle.radius = r;
        display_draw_object(&theCircle);

        /* On arc with r/2 radius. 6 o'clock (-270) to 3 o'clock (0) CW */
        arcOne.arc.radius = r / 2;
        display_draw_object(&arcOne);

        /* On arc with r/2 + 5 radius. 3 o'clock (0) to 12 o'clock (270) CW */
        arcTwo.arc.radius = r / 2 + 5;
        display_draw_object(&arcTwo);

        /* On Line from top right to bottom left of square */
        theLine.line.x1 = 2 * r;
        theLine.line.y2 = 2 * r;
        display_draw_object(&theLine);

        /* Print the test name with an column offset of r-15 */
        theText.text.column = r - 15;
        display_draw_object(&theText);

        /* Print the test name with an column offset of r-15 */
        theText.text.column = r - 15;
        display_draw_object(&theText);

        /* Print it again lower on the screen and without word wrap */
        initTextRow = theText.text.row;
        theText.text.wordWrap = false;
        theText.text.row = 6;
        display_draw_object(&theText);
        theText.text.row = initTextRow;
        theText.text.wordWrap = true;

        delay_ms(500);
    }
}