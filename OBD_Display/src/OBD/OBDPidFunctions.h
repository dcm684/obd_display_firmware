/*
 * OBDPidFunctions.h
 *
 * Created: 1/21/2013
 *  Author: Meyer
 */

#ifndef OBDPIDFUNCTIONS_H_
#define OBDPIDFUNCTIONS_H_

#include <stdint.h>
#include <stdbool.h>
#include "OBD.h"

/**
 * /def PID_NON_STD_START
 * Beginning PID number of the non standard PIDs
 */
#define PID_NON_STD_START 0xE0

/**
 * /def PID_COMBO_START
 * Beginning PID number of PIDs that use multiple base PIDs, e.g.
 * fuel economy uses MAF and vehicle speed
 */
#define PID_COMBO_START 0xF0

typedef enum {
    pid_supported_01_20				= 0x00, /**< PIDs supported 0x01 - 0x20 */
    pid_monitor_status				= 0x01, /**< Monitor status since DTCs cleared */
    pid_freeze_dtc					= 0x02, /**< DTC that caused required freeze frame data storage */
    pid_fuel_system_status			= 0x03, /**< Fuel system statuses */
    pid_engine_load					= 0x04, /**< Calculated LOAD Value */
    pid_engine_coolant_temp			= 0x05, /**< Engine Coolant Temperature */
    pid_bank_1_short_term_trim		= 0x06, /**< Short Term Fuel Trim - Bank 1 */
    pid_bank_1_long_term_trim		= 0x07, /**< Long Term Fuel Trim - Bank 1 */
    pid_bank_2_short_term_trim		= 0x08, /**< Short Term Fuel Trim - Bank 2 */
    pid_bank_2_long_term_trim		= 0x09, /**< Long Term Fuel Trim - Bank 2 */
    pid_fuel_pressure				= 0x0A, /**< Fuel Rail Pressure (gauge) */
    pid_intake_manifold_pressure	= 0x0B, /**< Intake Manifold Absolute Pressure */
    pid_engine_rpm					= 0x0C,	/**< Engine RPM */
    pid_vehicle_speed				= 0x0D, /**< Vehicle Speed Sensor */
    pid_timing_advance				= 0x0E,	/**< Ignition Timing Advance for #1 Cylinder */
    pid_intake_air_temp				= 0x0F,	/**< Intake Air Temperature */
    pid_maf_rate					= 0x10,	/**< Air Flow Rate from Mass Air Flow Sensor */
    pid_throttle_pos				= 0x11, /**< Absolute Throttle Position */
    pid_cmd_secondary_air_status	= 0x12,	/**< Commanded Secondary Air Status */
    pid_o2_sensors_present_2b_4s	= 0x13,	/**< Location of Oxygen Sensors */
    pid_o2_primary_bank_1_sensor_1	= 0x14,	/**< Bank 1 - Sensor 1 */
    pid_o2_primary_bank_1_sensor_2	= 0x15,	/**< Bank 1 - Sensor 2 */
    pid_o2_primary_bank_1_sensor_3	= 0x16,	/**< Bank 1 - Sensor 3 */
    pid_o2_primary_bank_1_sensor_4	= 0x17,	/**< Bank 1 - Sensor 4 */
    pid_o2_primary_bank_2_sensor_1	= 0x18,	/**< Bank 2 - Sensor 1 */
    pid_o2_primary_bank_2_sensor_2	= 0x19,	/**< Bank 2 - Sensor 2 */
    pid_o2_primary_bank_2_sensor_3	= 0x1A,	/**< Bank 2 - Sensor 3 */
    pid_o2_primary_bank_2_sensor_4	= 0x1B,	/**< Bank 2 - Sensor 4 */
    pid_obd_std						= 0x1C,	/**< OBD requirements to which vehicle is designed */
    pid_o2_sensors_present_4b_2s	= 0x1D,	/**< Location of oxygen sensors */
    pid_aux_input_status			= 0x1E,	/**< Auxiliary Input Status */
    pid_runtime_since_engine_start	= 0x1F,	/**< Time Since Engine Start */

    pid_supported_21_40				= 0x20,	/**< PIDs supported 0x21 - 0x40 */
    pid_dist_with_mil_on			= 0x21, /**< Distance Traveled While MIL is Activated */
    pid_fuel_rail_pressure_manifold	= 0x22,	/**< Fuel Rail Pressure relative to manifold vacuum */
    pid_fuel_rail_pressure_gauge	= 0x23,	/**< Fuel Rail Pressure */
    pid_o2_wr_lambda_volt_s1		= 0x24,	/**< Bank 1 - Sensor 1 (wide range O2S) equivalence ration and voltage */
    pid_o2_wr_lambda_volt_s2		= 0x25,	/**< Bank 1 - Sensor 2 (wide range O2S) equivalence ration and voltage */
    pid_o2_wr_lambda_volt_s3		= 0x26,	/**< Bank 1 - Sensor 3 (wide range O2S) equivalence ration and voltage */
    pid_o2_wr_lambda_volt_s4		= 0x27,	/**< Bank 1 - Sensor 4 (wide range O2S) equivalence ration and voltage */
    pid_o2_wr_lambda_volt_s5		= 0x28,	/**< Bank 2 - Sensor 1 (wide range O2S) equivalence ration and voltage */
    pid_o2_wr_lambda_volt_s6		= 0x29,	/**< Bank 2 - Sensor 2 (wide range O2S) equivalence ration and voltage */
    pid_o2_wr_lambda_volt_s7		= 0x2A,	/**< Bank 2 - Sensor 3 (wide range O2S) equivalence ration and voltage */
    pid_o2_wr_lambda_volt_s8		= 0x2B,	/**< Bank 2 - Sensor 4 (wide range O2S) equivalence ration and voltage */
    pid_cmd_egr						= 0x2C,	/**< Commanded EGR */
    pid_egr_error					= 0x2D,	/**< EGR Percent Error */
    pid_cmd_evaporative_purge		= 0x2E,	/**< Commanded Evaporative Purge */
    pid_fuel_level_input			= 0x2F,	/**< Fuel Level Input */
    pid_warm_up_since_dtc_clear		= 0x30,	/**< Number of warm-ups since diagnostic trouble codes cleared */
    pid_dist_since_dtc_clear		= 0x31,	/**< Distance since diagnostic trouble codes cleared */
    pid_evap_sys_vapor_pressure_sm	= 0x32,	/**< Evap System Vapor Pressure */
    pid_barometric_pressure			= 0x33,	/**< Barometric Pressure */
    pid_o2_wr_lambda_current_s1		= 0x34,	/**< Bank 1 - Sensor 1 (wide range O2S) equivalence ration and current */
    pid_o2_wr_lambda_current_s2		= 0x35,	/**< Bank 1 - Sensor 2 (wide range O2S) equivalence ration and current */
    pid_o2_wr_lambda_current_s3		= 0x36,	/**< Bank 1 - Sensor 3 (wide range O2S) equivalence ration and current */
    pid_o2_wr_lambda_current_s4		= 0x37,	/**< Bank 1 - Sensor 4 (wide range O2S) equivalence ration and current */
    pid_o2_wr_lambda_current_s5		= 0x38,	/**< Bank 2 - Sensor 1 (wide range O2S) equivalence ration and current */
    pid_o2_wr_lambda_current_s6		= 0x39,	/**< Bank 2 - Sensor 2 (wide range O2S) equivalence ration and current */
    pid_o2_wr_lambda_current_s7		= 0x3A,	/**< Bank 2 - Sensor 3 (wide range O2S) equivalence ration and current */
    pid_o2_wr_lambda_current_s8		= 0x3B,	/**< Bank 2 - Sensor 4 (wide range O2S) equivalence ration and current */
    pid_catalyst_temp_b1_s1			= 0x3C,	/**< Catalyst Temperature Bank 1, Sensor 1 */
    pid_catalyst_temp_b2_s1			= 0x3D,	/**< Catalyst Temperature Bank 2, Sensor 1 */
    pid_catalyst_temp_b1_s2			= 0x3E,	/**< Catalyst Temperature Bank 1, Sensor 2 */
    pid_catalyst_temp_b2_s2			= 0x3F,	/**< Catalyst Temperature Bank 2, Sensor 2 */

    pid_supported_41_60				= 0x40,	/**< PIDs supported 0x41 - 0x60 */
    pid_drive_cycle_monitor_status	= 0x41,	/**< Monitor status this driving cycle */
    pid_control_module_volt			= 0x42,	/**< Control module voltage */
    pid_absolute_load				= 0x43,	/**< Absolute Load Value */
    pid_cmd_equiv_ratio				= 0x44,	/**< Commanded Equivalence Ratio */
    pid_relative_throttle_pos		= 0x45,	/**< Relative Throttle Position */
    pid_ambient_air_temp			= 0x46,	/**< Ambient air temperature */
    pid_absolute_throttle_pos_b		= 0x47,	/**< Absolute Throttle Position B */
    pid_absolute_throttle_pos_c		= 0x48,	/**< Absolute Throttle Position C */
    pid_absolute_throttle_pos_d		= 0x49,	/**< Absolute Throttle Position D */
    pid_absolute_throttle_pos_e		= 0x4A,	/**< Absolute Throttle Position E */
    pid_absolute_throttle_pos_f		= 0x4B,	/**< Absolute Throttle Position F */
    pid_cmd_throttle_actuator		= 0x4C,	/**< Commanded Throttle Actuator Control */
    pid_time_run_with_mil			= 0x4D,	/**< Time run by the engine while MIL is activated */
    pid_time_since_dtc_cleared		= 0x4E,	/**< Engine Run Time since diagnostic trouble codes cleared */
    pid_max_equiv_o2_manifold		= 0x4F,	/**< Maximum values for O2S */
    pid_max_maf						= 0x50,	/**< Maximum Air Flow Rate from MAF sensor */
    pid_fuel_type					= 0x51,	/**< Type of fuel currently being utilized by the vehicle */
    pid_ethanol_percent				= 0x52,	/**< Alcohol Fuel Percentage */
    pid_evap_sys_vapor_pressure_abs	= 0x53,	/**< Absolute Evap System Vapor Pressure */
    pid_evap_sys_vapor_pressure_big	= 0x54,	/**< Evap System Vapor Pressure */
    pid_o2_second_bank_1_sensor_3	= 0x55,	/**< Short Term Secondary O2 Sensor Fuel Trim for Bank 1 and Bank 3 */
    pid_o2_second_bank_1_sensor_4	= 0x56,	/**< Long Term Secondary O2 Sensor Fuel Trim for Bank 1 and Bank 3 */
    pid_o2_second_bank_2_sensor_3	= 0x57,	/**< Short Term Secondary O2 Sensor Fuel Trim for Bank 2 and Bank 4 */
    pid_o2_second_bank_2_sensor_4	= 0x58,	/**< Long Term Secondary O2 Sensor Fuel Trim for Bank 2 and Bank 4 */
    pid_fuel_rail_pressure_abs		= 0x59,	/**< Fuel Rail Pressure (absolute) */
    pid_accelerator_pedal_pos		= 0x5A,	/**< Relative Accelerator Pedal Position */
    pid_hybrid_battery_life_left	= 0x5B,	/**< Hybrid Battery Pack Remaining Life */
    pid_engine_oil_temp				= 0x5C,	/**< Engine Oil Temperature */
    pid_fuel_injection_timing		= 0x5D,	/**< Fuel Injection Timing */
    pid_engine_fuel_rate			= 0x5E,	/**< Engine Fuel Rate */
    pid_emission_requirements		= 0x5F,	/**< Emission requirements to which vehicle is designed */

    pid_supported_61_80				= 0x60,	/**< PIDs supported 0x61 - 0x80 */
    pid_engine_torque_driver		= 0x61,	/**< Driver's Demand Engine - Percent Torque */
    pid_engine_torque_actual		= 0x62,	/**< Actual Engine - Percent Torque */
    pid_engine_torque_ref			= 0x63,	/**< Engine Reference Torque */
    pid_engine_torque_percent_data	= 0x64,	/**< Engine Percent Torque Data */
    pid_aux_input_output_support	= 0x65,	/**< Auxiliary Inputs / Outputs supported */

    pid_supported_81_A0				= 0x80,	/**< PIDs supported 0x81 - 0xA0 */

    pid_supported_A1_C0				= 0xA0,	/**< PIDs supported 0xA1 - 0xC0 */

    pid_supported_C1_E0				= 0xC0,	/**< PIDs supported 0xC1 - 0xE0 */

    pid_accelerometer_x				= 0xF1,	/**< Non-standard PID for accelerometer x-axis */
    pid_accelerometer_y				= 0xF2,	/**< Non-standard PID for accelerometer y-axis */
    pid_accelerometer_z				= 0xF3,	/**< Non-standard PID for accelerometer z-axis */

    pid_fuel_economy				= 0xF0,	/**< Non-standard PID for fuel economy */

} obd_pid_names;

/*!
 * All of the units used by the OBD functions
 */
typedef enum {
    obd_units_multiple,		/** < Multiple values are read by this PID */
    obd_units_none,			/** < Unitless value */
    obd_units_bitwise,		/** < Bitwise value stored in data from vehicle */
    obd_units_percent,		/** < Percentage */
    obd_units_volts,		/** < Volts */
    obd_units_mA,			/** < Milliamps */
    obd_units_seconds,		/** < Seconds */
    obd_units_minutes,		/** < Minutes */
    obd_units_degrees,		/** < Degrees, the angle */

    obd_units_metric_deg_c,	/** < Degrees celsius */
    obd_units_eng_deg_f,	/** < Degrees celsius */

    obd_units_metric_Pa,	/** < Pascal */
    obd_units_metric_kPa,	/** < kiloPascal */
    obd_units_eng_inH20,	/** < Inches of water */
    obd_units_eng_inHg,		/** < Inches of mercury */
    obd_units_eng_psi,		/** < Pounds per square inch */

    obd_units_rpm,			/** < Rotations per minute */

    obd_units_metric_g_s,	/** < Grams per second */
    obd_units_eng_lb_min,	/** < Pounds per minute */

    obd_units_metric_km,	/** < Kilometers */
    obd_units_eng_miles,	/** < Miles */
    obd_units_metric_km_h,	/** < Kilometers per hour */
    obd_units_eng_mph,		/** < Miles per hour */

    obd_units_metric_L_h,	/** < Liters per hour */
    obd_units_eng_ga_h,		/** < Gallons per hour */

    obd_units_metric_Nm,	/** < Newton meters */
    obd_units_eng_ftlb,		/** < Foot pound */

    obd_units_metric_l_100km,	/** <Liters per 100km */
    obd_units_eng_mpg,		/** Miles per gallon */

    obd_units_gravity,		/** < Force in gs */

    obd_units_invalid,		/** < Invalid PID */

} obd_units;


/*!
 * Contains a value and its unit
 */
typedef struct {
    float value;	/** < Calculated Value */
    obd_units unit;	/** < Unit for the value */
} obd_value_with_unit;

/*!
 * The method of combustion used by an engine
 */
typedef enum {
    engine_compression,			/*!< Engine is compression based, e.g. diesel */
    engine_spark,				/*!< Engine uses spark plugs, e.g. gasoline */
} engine_type;

/*!
 * Status of the engine based on PID 0x01
 */
typedef struct {
    engine_type type;
    bool 		milState;		/*!< The MIL is on */
    uint8_t		availableDTCs;	/*!< Number of confirmed emission DTCs available */
    uint8_t		testByte1;		/*!< Test state byte 1. Varies based on engine type */
    uint8_t		testByte2;		/*!< Test state byte 2. Varies based on engine type */
} engine_status_value;

/*!
 * Fuel system status based on PID 0x03
 */
typedef enum {
    open_loop_insufficient_temp 	= 0x01, /*!< Open loop due to insufficient
												engine temperature */
    closed_loop_fully_functional	= 0x02, /*!< Closed loop, using oxygen
												sensor feedback to
												determine fuel mix */
    open_loop_engine_load_deccel	= 0x04, /*!< Open loop due to engine load
												OR fuel cut due to
												deceleration */
    open_loop_system_failure		= 0x08, /*!< Open loop due to system
												failure */
    closed_loop_feedback_fault		= 0x10, /*!< Closed loop, using at least
												one oxygen sensor but there
												is a fault in the feedback
												system */
    fuel_system_error				= 0xFF	/*!< Invalid value received */
} obd_fuel_system_status_value;

/*!
 * Describes the status of the secondary air system used by PID 0x12
 */
typedef enum {
    up_catalytic_convertor		= 0x01, /*!< Upstream of catalytic converter */
    down_catalytic_convertor	= 0x02, /*!< Downstream of catalytic converter */
    from_outside_or_of			= 0x04,	/*!< From the outside atmosphere or off */
    secondary_air_error			= 0xFF 	/*!< Invalid value received */
} obd_secondary_air_status_value;

/*!
 * Used to tell what oxygen sensors are available by PID 0x13
 */
typedef enum {
    oxygen_b2s4_b1_s1	= 0x01,	/*!< Oxygen sensor 1 on bank 1 is present */
    oxygen_b2s4_b1_s2	= 0x02,	/*!< Oxygen sensor 2 on bank 1 is present */
    oxygen_b2s4_b1_s3	= 0x04,	/*!< Oxygen sensor 3 on bank 1 is present */
    oxygen_b2s4_b1_s4	= 0x08,	/*!< Oxygen sensor 4 on bank 1 is present */
    oxygen_b2s4_b2_s1	= 0x10,	/*!< Oxygen sensor 1 on bank 2 is present */
    oxygen_b2s4_b2_s2	= 0x20,	/*!< Oxygen sensor 2 on bank 2 is present */
    oxygen_b2s4_b2_s3	= 0x40,	/*!< Oxygen sensor 3 on bank 2 is present */
    oxygen_b2s4_b2_s4	= 0x80,	/*!< Oxygen sensor 4 on bank 2 is present */
} obd_oxygen_sensor_b2_4s_value;

/*!
 * Used to tell what oxygen sensors are available by PID 0x1D
 */
typedef enum {
    oxygen_b4s2_b1_s1	= 0x01,	/*!< Oxygen sensor 1 on bank 1 is present */
    oxygen_b4s2_b1_s2	= 0x02,	/*!< Oxygen sensor 2 on bank 1 is present */
    oxygen_b4s2_b2_s1	= 0x04,	/*!< Oxygen sensor 1 on bank 2 is present */
    oxygen_b4s2_b2_s2	= 0x08,	/*!< Oxygen sensor 2 on bank 2 is present */
    oxygen_b4s2_b3_s1	= 0x10,	/*!< Oxygen sensor 1 on bank 3 is present */
    oxygen_b4s2_b3_s2	= 0x20,	/*!< Oxygen sensor 2 on bank 3 is present */
    oxygen_b4s2_b4_s1	= 0x40,	/*!< Oxygen sensor 1 on bank 4 is present */
    oxygen_b4s2_b4_s2	= 0x80,	/*!< Oxygen sensor 2 on bank 4 is present */
} obd_oxygen_sensor_b4_2s_value;

/*!
 * Status of auxiliary input systems
 */
typedef struct {
    bool power_take_off;	/*!<Is the power take off system active? */
} obd_aux_input_state;

/*!
 * Describes the OBD stands that the ECU was designed to comply with used
 * by PID 0x1C
 */
typedef enum {
    carb_obdii		= 0x1,	/*!< OBD-II as defined by the CARB */
    epa_obd			= 0x2,	/*!< OBD as defined by the EPA */
    obd_obdii		= 0x3,	/*!< OBD and OBD-II */
    obdi			= 0x4,	/*!< OBD-I */
    no_comply		= 0x5,	/*!< Not meant to comply with any OBD standard */
    eobd			= 0x6,	/*!< EOBD (Europe) */
    eobd_obdii		= 0x7,	/*!< EOBD and OBD-II */
    eobd_obd		= 0x8,	/*!< EOBD and OBD */
    eobd_obd_obdii	= 0x9,	/*!< EOBD, OBD, and OBD II */
    jobd			= 0xA,	/*!< JOBD (Japan) */
    jobd_obdii		= 0xB,	/*!< JOBD and OBD II */
    jobd_eobd		= 0xC,	/*!< JOBD and EOBD */
    jobd_eobd_obdii	= 0xD	/*!< JOBD, EOBD, and OBD II */
} obd_compliance_level_value;

/*!
 * State of onboard tests used by PID 41
 */
typedef enum {
    test_unavailable,	/*!< OBD test is not available */
    test_incomplete,	/*!< OBD test is available but not complete */
    test_complete,		/*!< OBD test has been completed */
} obd_onboard_test_value;

/*!
 * Collection of onboard tests used by PID 41
 */
typedef struct {
    obd_onboard_test_value misfire;				/*!< Misfire */
    obd_onboard_test_value fuel_sys;			/*!< Fuel system */
    obd_onboard_test_value components;			/*!< Components */
    obd_onboard_test_value reserved;			/*!< Reserved */
    obd_onboard_test_value catalyst;			/*!< Catalyst */
    obd_onboard_test_value heated_catalyst;		/*!< Heated catalyst*/
    obd_onboard_test_value evap_sys;			/*!< Evaporative system */
    obd_onboard_test_value secondary_air;		/*!< Secondary air system */
    obd_onboard_test_value ac_refrig;			/*!< A/C refrigerant */
    obd_onboard_test_value oxy_sensor;			/*!< Oxygen sensor */
    obd_onboard_test_value oxy_sensor_heater;	/*!< Oxygen sensor heater */
    obd_onboard_test_value egr_system;			/*!< EGR system */
} obd_onboard_tests;

/*!
 * Type of fuel used by system
 */
typedef enum {
    fuel_gasoline			= 0x01,	/*!< Gasoline */
    fuel_methanol			= 0x02,	/*!< Methanol */
    fuel_ethanol			= 0x03,	/*!< Ethanol */
    fuel_diesel				= 0x04,	/*!< Diesel */
    fuel_lpg				= 0x05,	/*!< Liquified petroleum gas */
    fuel_cng				= 0x06,	/*!< Compressed natural gas */
    fuel_propane			= 0x07,	/*!< Proane */
    fuel_electric			= 0x08,	/*!< Electric */
    fuel_bifuel_gasoline	= 0x09,	/*!< Bifuel running gasoline */
    fuel_bifuel_methanol	= 0x0A,	/*!< Bifuel running methanol */
    fuel_bifuel_ethanol		= 0x0B,	/*!< Bifuel running ethanol */
    fuel_bifuel_lpg			= 0x0C,	/*!< Bifuel running LPG */
    fuel_bifuel_cng			= 0x0D,	/*!< Bifuel running CNG */
    fuel_bifuel_propane		= 0x0E,	/*!< Bifuel running propane */
    fuel_bifuel_electric	= 0x0F,	/*!< Bifuel running electricity */
    fuel_bifuel_mixed		= 0x10,	/*!< Bifuel running mixed gas electric */
    fuel_hybrid_gasoline	= 0x11,	/*!< Hybrid gasoline */
    fuel_hybrid_ethanol		= 0x12,	/*!< Hybrid ethanol */
    fuel_hybrid_diesel		= 0x13,	/*!< Hybrid diesel */
    fuel_hybrid_electric	= 0x14,	/*!< Hybrid electric */
    fuel_hybrid_mixed		= 0x15,	/*!< Hybrid mixed fuel */
    fuel_hybrid_regenerate	= 0x16	/*!< Hybrid regenerative */
} obd_fuel_type_value;

/*!
 * Wide range lambda sensor value with voltage
 */
typedef struct {
    float equivRatio;	/*!< Lambda equivalence ratio */
    float voltage;		/*!< Sensor voltage */
} obd_wr_lambda_voltage_value;

/*!
 * Wide range lambda sensor value with current
 */
typedef struct {
    float equivRatio;	/*!< Lambda equivalence ratio */
    float current;		/*!< Sensor current */
} obd_wr_lambda_current_value;

typedef struct {
    float trim;			/*!< Oxygen sensor trim */
    float voltage;		/*!< Oxygen sensor voltage */
} obd_oxygen_voltage_value;

/*!
 * Maximum values for equivalence ratio, oxygen sensor voltage and current,
 * intake manifold absolute pressure, and air flow rate from MAF sensor
 * used by PIDs 0x4F and 0x50
 */
typedef struct {
    uint8_t equiv_ratio;		/*!< Maximum value for equivalence ratio */
    uint8_t oxygen_volt;		/*!< Maximum value for oxygen sensor voltage*/
    uint8_t oxygen_current;		/*!< Maximum value for oxygen sensor current */
    uint16_t manifold_pressure;	/*!< Maximum value for manifold pressure*/
    uint16_t air_flow_rate;		/*!< Maximum value for air flow from MAF sensor */
} obd_max_values;

/*!
 * Engine percent torque data from various points
 */
#if 0 //Unused
typedef struct {
    int8_t idle;	/*!< Engine percent torque for idle */
    int8_t point_1;	/*!< Engine percent torque for engine point 1 */
    int8_t point_2;	/*!< Engine percent torque for engine point 2 */
    int8_t point_3;	/*!< Engine percent torque for engine point 3 */
    int8_t point_4;	/*!< Engine percent torque for engine point 4 */
} obd_engine_torque_points;
#endif

bool obd_get_metric_value(obd_pid *inPID, float *outValue);
bool obd_translate_pid(obd_pid *inPID, obd_value_with_unit *outValue);

float obd_general_percentage(uint32_t *inData);
uint32_t obd_coolant_temp(uint32_t *inData);
float obd_fuel_trim(uint32_t *inData);
uint16_t obd_fuel_pressure(uint32_t *inData);
uint32_t obd_general_pass(uint32_t *inData);
float obd_engine_rpm(uint32_t *inData);
float obd_timing_advance(uint32_t *inData);
uint32_t obd_intake_air_temperature(uint32_t *inData);
float obd_maf_rate(uint32_t *inData);
obd_oxygen_voltage_value obd_oxygen_voltage_sensor(uint32_t *inData);
uint16_t obd_run_time(uint32_t *inData);
uint16_t obd_mil_distance(uint32_t *inData);
float obd_fuel_rail_pressure_vacuum(uint32_t *inData);
uint32_t obd_fuel_rail_pressure_gauge(uint32_t *inData);
obd_wr_lambda_voltage_value obd_oxygen_sensor_wr_lambda_voltage(uint32_t *inData);

bool obd_get_fuel_economy(obd_unit_type unit, obd_value_with_unit *outValue);
void obd_calc_fuel_economy(uint32_t kmPerHr, float maf, obd_unit_type unit, obd_value_with_unit *outValue) ;

bool obd_translate_nonstd_pid(obd_pid *inPid, obd_value_with_unit *outValue);
bool obd_check_and_process_accel_pid(uint8_t pidNumber, obd_value_with_unit *outValue);
bool obd_add_real_pids_to_list(volatile obd_pid_list *inList, uint8_t inPid);
#endif /* OBDPIDFUNCTIONS_H_ */