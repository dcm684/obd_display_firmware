/*
 * OBD.c
 *
 * Created: 9/7/2012 3:12:24 PM
 *  Author: Meyer
 */

#include <stdint.h>
#include <string.h>

#include "OBD.h"
#include "drivers/MCP2515.h"
#include "drivers/USART_Handler.h"
#include "tc.h"

/**
 * Initialize the OBD connection.
 *
 * Sets the receive filters to only accept OBD messages.
 */
bool obd_init(void)
{
    multiple_pids_remaining = 0;
    allow_obd_int_processing = true;

    pid_request_pending = false;
    pause_pid_requests = false;
    pids_being_sent = 0;

#ifdef CAN_LOOPBACK_TEST
    return true;
#else
    return obd_set_can_filters();
#endif
}

/**
 * Processes timer interrupt for OBD data
 */
void obd_timer_interrupt_handler(void)
{
    /* Check if data was received from the OBD and if it was load
    it. Otherwise increment the OBD timeout counter */
    if((allow_obd_int_processing) && (pids_being_sent > 0)) {

        /* If PID requests were paused resume requesting them here */
        if((!pid_request_pending) && (multiple_pids_remaining > 0)) {
            obd_multiple_pid_request_next();
        }

#ifdef OBD_DEBUG
        //fprintf_P(&usart_serial_stream, PSTR("Starting it %u %u\r\n"),
        //	multiple_pids_remaining, pendingPID);
#endif /* OBD_DEBUG */

        if(can_was_data_received() && obd_receive_requested()) {
            /* If this was a part of a multiple PID request try the
            next one */
#ifdef OBD_DEBUG
            fprintf_P(&usart_serial_stream, PSTR(
                          "Success:\tPID: %X\tVal:%lu\tTO Count:%u\r\n"),
                      pendingPID->pid,
                      pendingPID->value,
                      counter_obd_timeout);
#endif /* OBD_DEBUG */

            if(!pause_pid_requests) {
                obd_multiple_pid_request_next();
            }

        } else if((pendingPID->rx_state) == obd_sent) {
            counter_obd_timeout++;

            if(counter_obd_timeout >= OBD_TIMEOUT) {

#ifdef OBD_DEBUG
                fprintf_P(&usart_serial_stream,
                          PSTR("Waiting TO: %u\tSecond Attempt: %u\r\n"),
                          counter_obd_timeout,
                          pid_send_second_attempt);
#endif /* OBD_DEBUG */

                if(pid_send_second_attempt) {
                    /* Give up and move on to the next PID if
                    this was part of a multiple PID request */
#ifdef CAN_LOOPBACK_TEST
                    fputs_P(PSTR("Timed Out\r\n"), &usart_serial_stream);
#endif
                    pendingPID->rx_state = obd_timed_out;
                    pid_request_pending = false;

                    /* Try the next PID in the list */
                    if(!pause_pid_requests) {
                        obd_multiple_pid_request_next();
                    }

                } else {
#ifdef CAN_LOOPBACK_TEST
                    fputs_P(PSTR("Second Try\r\n"), &usart_serial_stream);
#endif
                    /* Try the PID for a second time */
                    obd_send_request(pendingPID, true);
                }
            }
        }
    }
}

/**
 * Set the receive filters to only accept data from those in the OBDII
 * address range, 0x7E8 - 0x7EF.
 *
 * /returns A bool indicating whether setting the filters was successful
 *
 */
bool obd_set_can_filters(void)
{
    FILTER0 obdFilter0;
    FILTER1 obdFilter1;

    /* Mask is 0x7F8. Filter will allow 0x7E8 - 0x7EF. Not sure if 7F0 is a
    valid address. */
    obdFilter0.mask = 0x7F8;
    obdFilter0.filters[0] = 0x7E8;
    obdFilter0.filters[1] = 0x7E8;

    obdFilter1.mask = 0x7F8;
    obdFilter1.filters[0] = 0x7E8;
    obdFilter1.filters[1] = 0x7E8;
    obdFilter1.filters[2] = 0x7E8;
    obdFilter1.filters[3] = 0x7E8;

    return (can_set_receive_filters(can_buffer_all, obdFilter0, obdFilter1));
}

/**
 * Requests the given PID and returns the obd_received value
 *
 * Returned value may need further formating, e.g. O2 Sensors return two
 * types of values for each request, coolant temperature is 40C less than
 * what is returned.
 *
 * /param code PID to be requested
 *
 * /returns Value obd_received from the vehicle. When the number of
 * returned data bytes is:
 *      1: A
 *      2: A  * 256 + B
 *      3: A  * (2^16) + B  * (2^8) + C
 *      4: A  * (2^24) + B  * (2^16) + C  * (2^8) + D
 *
 */
bool obd_query_pid(uint8_t code, volatile uint32_t *readData)
{
    CANMSG msg;
    bool rxSuccess;
    unsigned int noMatch = 0;
    unsigned short i;

    uint8_t global_pids_left; /* Stores multiple_pids_remaining since
		calls from this function take precedence */

    pause_pid_requests = true;

    msg.adrsValue = 0x7df;
    msg.isExtendedAdrs = false;
    msg.extendedAdrsValue = 0;
    msg.rtr = false;
    msg.dataLength = 8;
    msg.data[0] = 0x02;
    msg.data[1] = 0x01;
    msg.data[2] = code;
    msg.data[3] = 0;
    msg.data[4] = 0;
    msg.data[5] = 0;
    msg.data[6] = 0;
    msg.data[7] = 0;

    rxSuccess = true;

    /* Pause the transmission of multiple PIDs */
    while(pid_request_pending) {

    }

    allow_obd_int_processing = false;
    global_pids_left = multiple_pids_remaining;
    multiple_pids_remaining = 0;

    if(!can_transmit_message(can_tx_buffer_next, &msg, OBD_TIMEOUT)) {
        rxSuccess = false;
    }

    rxSuccess = can_receive_message(&msg, OBD_TIMEOUT);

    if(rxSuccess) {
        /* Check if the obd_received PID matches the sent PID.
        Added by C. Meyer */
        while(msg.data[2] != code) {
            rxSuccess = can_receive_message(&msg, OBD_TIMEOUT);
            noMatch++;

            if((noMatch >= 100) || (rxSuccess == false)) {

#ifdef OBD_DEBUG
                fprintf_P(&usart_serial_stream, PSTR("Single PID timed out: %X\r\n"), code);
#endif /* OBD_DEBUG */

                rxSuccess = false;
                break;
            }
        }

    } else {
        rxSuccess = false;
    }

    /* Convert the obd_received uint8_ts to a single number.
    Modified by C. Meyer to read all four uint8_ts. */
    if(rxSuccess) {
        *readData = 0;

        for(i = 3; i <= msg.data[0]; i++) {
            *readData = ((*readData) << 8) | msg.data[i] ;
        }
    }

    /* Resume transmitting multiple PIDs */
    multiple_pids_remaining = global_pids_left;

    /* Resend the last PID since its was lost when this
    function was called */
    if(multiple_pids_remaining > 0) {
        obd_send_request(pendingPID, false);
    }

    allow_obd_int_processing = true;
    pause_pid_requests = false;

    return rxSuccess;
}

/**
 * Queries the vehicle for several PIDs vehicle and updates their values
 *
 * If a PID value is not returned the value is unchanged
 *
 * /param inPids		Array of obd_pids to check
 * /param numberOfPids	Number of items in the inPids list
 *
 * /returns Returns true if at least one of the requests was successful
 */
bool obd_query_multiple_pids(obd_pid *inPids, uint8_t numberOfPids)
{
    uint8_t i;
    bool someSuccess = false;

    /* Simplest but inefficient method */
#if 1
    obd_pid *currentPid;

    currentPid = inPids;

    for(i = 0; i < numberOfPids; i++) {
        currentPid->rx_state = obd_query_pid(currentPid->pid,
                                             &(currentPid->value)) ?
                               obd_received : obd_timed_out;
        someSuccess = (currentPid->rx_state == obd_received) || someSuccess;
        currentPid++;
    }

#else
    /* Will do later */
    /* More complex method of sending multiple PIDs but could be fastest */

    /* Fill up two transmit buffers with the first two PIDs */
    /* Once a receive buffer matches a PID send out another PID */
    /* If two PIDs that were sent after a PID were obd_received before the
    first is obd_received, assumed that the PID transmit was a failure.
    Also, assume a failure if no PIDs are obd_received aftera set time */

#endif

    return someSuccess;
}

/**
 * Transmits a request for the given PID and then returns.
 *
 * A pointer to the requested obd_pid is stored in pendingPID. This
 * allows the pbd_pid to be updated once the value is obd_received.
 *
 * /param inPID PID whose value is requested
 * /param secondAttempt Is this the second try to request the PID?
 *
 * /returns Was the transmit successful
 */
bool obd_send_request(volatile obd_pid *inPID, bool secondAttempt)
{
    CANMSG msg;
    bool retVal;

    msg.adrsValue = 0x7df;
    msg.isExtendedAdrs = false;
    msg.extendedAdrsValue = 0;
    msg.rtr = false;
    msg.dataLength = 8;
    msg.data[0] = 0x02;
    msg.data[1] = 0x01;
    msg.data[2] = inPID->pid;
    msg.data[3] = 0;
    msg.data[4] = 0;
    msg.data[5] = 0;
    msg.data[6] = 0;
    msg.data[7] = 0;

    inPID->rx_state = obd_sent;

    allow_obd_int_processing = false;

    pid_request_pending = true;

    if(can_transmit_message(can_tx_buffer_next, &msg, OBD_TIMEOUT)) {
        //if(can_transmit_message(can_tx_buffer_next, &msg, 3)) {
#ifdef OBD_DEBUG
        fprintf_P(&usart_serial_stream, PSTR("Requesting PID from: %X\r\n"), inPID->pid);
#endif /* OBD_DEBUG */
        pendingPID = inPID;
        counter_obd_timeout = 0;
        pid_send_second_attempt = secondAttempt;
        retVal = true;

    } else {
        retVal = false;
    }

    allow_obd_int_processing = true;

    return retVal;
}

/**
 * Reads the CAN transceiver's buffer and will store the value
 * in the obd_pid referenced by pendingPID if it matches the
 * requested PID.
 *
 * /returns Was a correct PID obd_received?
 */
bool obd_receive_requested(void)
{
    CANMSG msg;
    uint8_t i;

    allow_obd_int_processing = false;

    if(can_receive_message(&msg, OBD_TIMEOUT)) {

#ifdef OBD_DEBUG
        fprintf_P(&usart_serial_stream, PSTR("Received PID: %X\tWaiting for: %X\r\n"), msg.data[2], pendingPID->pid);
#endif /* OBD_DEBUG */

        /* Check if the PID obd_received matches the pending PID */
        while(msg.data[2] != pendingPID->pid) {
            /* If data was obd_received check to see if its PID
            matches otherwise return in shame */
            if(can_was_data_received()) {
                can_receive_message(&msg, OBD_TIMEOUT);
#ifdef OBD_DEBUG
                fprintf_P(&usart_serial_stream,
                          PSTR("Received: PID: %X\tLength: %X\tData[0]: %lX\r\n"),
                          msg.data[2],
                          msg.dataLength,
                          msg.data[0]);
#endif /* OBD_DEBUG */

            } else {

#ifdef OBD_DEBUG
                fprintf_P(&usart_serial_stream,
                          PSTR("Mismatch: (RX'd) %X != (Pnd) %X\r\n"),
                          msg.data[2],
                          pendingPID->pid);
#endif /* OBD_DEBUG */

                allow_obd_int_processing = true;
                return false;
            }
        }

        /* We have a matching PID, so store the value */
        pendingPID->value = 0;

        for(i = 3; i <= msg.data[0]; i++) {
            pendingPID->value = ((pendingPID->value) << 8) | msg.data[i];
        }

    } else {
        allow_obd_int_processing = true;
        return false;
    }

    pendingPID->rx_state = obd_received;
    allow_obd_int_processing = true;
    pid_request_pending = false;

    return true;
}

/**
 * Starts the process of requesting multiple PIDs
 *
 * When the process has been completed multiple_pids_remaining will be
 * zero.
 *
 * /param inPIDList List containing the PIDs to request
 */
void obd_send_multiple_requests(volatile obd_pid_list *inPIDList)
{
    allow_obd_int_processing = false;

    multiple_pids_remaining = inPIDList->length - 1;
    pids_being_sent = inPIDList->length;
    pidsToRead = inPIDList->listOfPIDs;
    obd_send_request(&(pidsToRead[0]), false);

    allow_obd_int_processing = true;
}

/**
 * Requests the next PID in a list of PIDs
 */
void obd_multiple_pid_request_next(void)
{
    if(multiple_pids_remaining > 0) {
        allow_obd_int_processing = false;
        //pidsToRead = &pidsToRead[1];//pidsToRead + sizeof(obd_pid);
        obd_send_request(&(pidsToRead[pids_being_sent - multiple_pids_remaining]), false);
        multiple_pids_remaining--;
        allow_obd_int_processing = true;
    }
}

/**
 * Initializes and allocates memory to an empty PID list
 *
 * /param inList A pointer to where the new PID list
 */
void obd_init_pid_list(volatile obd_pid_list *inList)
{
    //inList = malloc(sizeof(obd_pid_list) + sizeof(obd_pid));
    //*(inList->listOfPIDs) = malloc(sizeof(obd_pid));
    inList->length = 0;
}

/**
 * Adds the given PID to the given PID list
 *
 * /param inList List to add the PID to
 * /param newPID PID to add
 *
 * /returns Is the PID now in the list of PIDs? If it was already in
 *			the list this will return true
 */
bool obd_add_pid_to_list(volatile obd_pid_list *inList, uint8_t newPID)
{
    uint8_t i;

    /* Verify that the PID isn't already in the given list */
    for(i = 0; i < inList->length; i++) {
        if(newPID == inList->listOfPIDs[i].pid) {

#ifdef OBD_DEBUG
            fprintf_P(&usart_serial_stream, PSTR("Already in list: %X\r\n"), newPID);
#endif /* OBD_DEBUG */

            return true;
        }
    }

    /* Confirm that there is space available for the new PID. This is
    done after the check for existence because the PID may already be in
    the list */
    if(inList->length >= OBD_PID_LIST_MAX) {
        return false;
    }

#ifdef OBD_DEBUG
    fprintf_P(&usart_serial_stream, PSTR("Adding to PID List: %X\r\n"), newPID);
#endif /* OBD_DEBUG */

    /* Add the PID to the list */
    inList->length++;
    /*inList = realloc(inList->listOfPIDs,
    				sizeof(uint8_t) + sizeof(obd_pid) * inList->length);*/
    inList->listOfPIDs[inList->length - 1].pid = newPID;

    return true;
}

/**
 * Removes the given PID from from the given PID list
 *
 * @TODO: Allow for cases when multiple pseudo-pids use a certain
 * PID. For example if MAF and fuel economy are measured, I should
 * leave MAF in the list if I remove fuel economy. Probably need to
 * put a use counter in the PID struct array
 *
 * /param inList	List to remove the PID from
 * /param thePID	PID to remove
 *
 * /returns The PID was removed
 */
bool obd_remove_pid_from_list(volatile obd_pid_list *inList, uint8_t thePID)
{
    uint8_t i;
    bool matchFound;

    matchFound = false;

    for(i = 0; i < inList->length; i++) {
        /* Shift remaining PIDs one index earlier */
        if(matchFound) {
            inList->listOfPIDs[i - 1] = inList->listOfPIDs[i];

        } else if(inList->listOfPIDs[i].value == thePID) {
            matchFound = true;
        }
    }

    if(matchFound) {
        (inList->length)--;
    }

    return matchFound;
}

/**
 * If it exists in the inList copies the given PID and its data to the
 * given outPID
 *
 * /param inList The list to search for and return the value of
 *		the given PID
 * /param pidToGet	The PID whose value is to be returned
 * /param outPID	Where the PID will be copied to
 *
 * /return True if the PID was found
 */
bool obd_get_pid(volatile obd_pid_list *inList, uint8_t pidToGet, obd_pid *outPID)
{
    bool pidFound;
    uint8_t i;
    pidFound = false;

#ifdef OBD_GET_DEBUG
    fprintf_P(&usart_serial_stream, PSTR("Looking for PID %X - "), pidToGet);
#endif /* OBD_GET_DEBUG */

    for(i = 0; i < inList->length; i++) {
        if(inList->listOfPIDs[i].pid == pidToGet) {
            //outPID = &(inList->listOfPIDs[i]);
            memcpy(outPID, (void *) &(inList->listOfPIDs[i]), sizeof(obd_pid));
            pidFound = true;
            break;
        }
    }

#ifdef OBD_GET_DEBUG
    if(pidFound) {
        fprintf_P(&usart_serial_stream, PSTR("Found (%lX)\r\n"), outPID->value);
    } else {
        fputs_P(PSTR("Not Found\r\n"), &usart_serial_stream);
    }
#endif /* OBD_GET_DEBUG */

    return pidFound;
}

/**
 * Stores the most recent raw value received for the given PID
 *
 * If a value is in the pid list the value is stored, otherwise false
 * is returned
 *
 * /param inList The list to search for and return the value of
 *		the given PID
 * /param pidToGet	The PID whose value is to be returned
 * /param outValue	Where to store the found value
 *
 * /return True if the PID is found
 */
bool obd_get_most_recent_value(volatile obd_pid_list *inList, uint8_t pidToGet, uint32_t *outValue)
{
    obd_pid tempPid;
    bool retVal;

    retVal = obd_get_pid(inList, pidToGet, &tempPid);

    *outValue = tempPid.value;

    return retVal;
}

/**
 * Prints a list of available PIDs
 *
 * /param Where to display the available PIDs
 */
void obd_print_pids(FILE *outStream)
{
    uint32_t availFuncs;
    bool exitAll;
    uint8_t i;
    uint8_t j;

    exitAll = false;

    for(i = 0; i < 5; i++) {
        obd_query_pid(i * 0x20, &availFuncs);

        for(j = 1; j < 0x21; j ++) {
            if(_BV(j - 1) & availFuncs) {
                fprintf_P(outStream, PSTR("0x%X\r\n"), (i * 0x20) + j);

            } else if(j == 0x20) {
                /* When the PID 0x20 * i is zero, it means that the next
                 * available function PID is invalid
                 */
                exitAll = true;
                break;
            }
        }

        if(exitAll) {
            if(i == 0) {
                fputs_P(PSTR("Error Receiving\r\n"), outStream);
            }

            break;
        }
    }
}
