/*
 * OBDPidStrings.h
 *
 * Created: 3/15/2013 11:26:28 AM
 *  Author: cmeyer
 */

#ifndef OBDPIDSTRINGS_H_
#define OBDPIDSTRINGS_H_

#include <avr/pgmspace.h>
#include "OBDPidFunctions.h"

/**
 * @def OBD_UNIT_MAX_LENGTH
 * Maximum number of character (including \0) used by unit strings
 */
#define OBD_UNIT_MAX_LENGTH 8

extern const char PROGMEM obdUnitStrings[][OBD_UNIT_MAX_LENGTH];

#endif /* OBDPIDSTRINGS_H_ */