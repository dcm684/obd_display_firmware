/*
 * OBDPidFunctions.c
 *
 * Created: 1/21/2013
 *  Author: Meyer
 */

#include "OBDPidFunctions.h"

#include "drivers/MMA7361.h"
#include "OBD.h"

extern accel_all_chans accelChans;

/**
 * Gets a meaningful metric value of of the value stored in the given PID
 *
 * @param inPID     PID containing the data to be translated
 * @param retVal    Where the data will be stored in float format
 *
 * /return          True, if the raw PID value was converted to metric
 */
bool obd_get_metric_value(obd_pid *inPID, float *outValue)
{
    bool retVal = false;

    switch(inPID->pid) {

    /* Bit encoded */
    case pid_supported_01_20:
    case pid_supported_21_40:
    case pid_supported_41_60:
    case pid_supported_81_A0:
    case pid_supported_A1_C0:
    case pid_supported_C1_E0:

        retVal = false;
        break;

    /* Pass through */
    case pid_intake_manifold_pressure:
    case pid_vehicle_speed:
    case pid_runtime_since_engine_start:
    case pid_dist_with_mil_on:
    case pid_warm_up_since_dtc_clear:
    case pid_barometric_pressure:
    case pid_dist_since_dtc_clear:
    case pid_time_run_with_mil:
    case pid_time_since_dtc_cleared:
    case pid_engine_torque_ref:

        *outValue = (float) inPID->value;
        retVal = true;
        break;

    /* Single byte percentage */
    case pid_engine_load:
    case pid_throttle_pos:
    case pid_cmd_egr:
    case pid_cmd_evaporative_purge:
    case pid_fuel_level_input:
    case pid_relative_throttle_pos:
    case pid_absolute_throttle_pos_b:
    case pid_absolute_throttle_pos_c:
    case pid_absolute_throttle_pos_d:
    case pid_absolute_throttle_pos_e:
    case pid_absolute_throttle_pos_f:
    case pid_cmd_throttle_actuator:
    case pid_ethanol_percent:
    case pid_accelerator_pedal_pos:
    case pid_hybrid_battery_life_left:

        *outValue = obd_general_percentage((void *)&(inPID->value));
        retVal = true;
        break;

    /* Bit encoded */
    case pid_monitor_status:

        retVal = false;
        break;

    /* Multiple values */
    case pid_freeze_dtc:

        retVal = false;
        break;

    /* Bit encoded */
    case pid_fuel_system_status:

        retVal = false;
        break;

    case pid_engine_coolant_temp:

        *outValue = ((float) inPID->value) - 40;
        retVal = false;
        break;

    case pid_bank_1_short_term_trim:
    case pid_bank_1_long_term_trim:
    case pid_bank_2_short_term_trim:
    case pid_bank_2_long_term_trim:

        *outValue = (((float) inPID->value)  - 128) * 100 / 128;
        retVal = true;
        break;

    case pid_fuel_pressure:

        *outValue = ((float) inPID->value) * 3;
        retVal = true;
        break;

    case pid_engine_rpm:

        *outValue = ((float) inPID->value) / 4;
        retVal = true;
        break;

    case pid_timing_advance:

        *outValue = ((float) inPID->value) / 2 - 64;
        retVal = true;
        break;

    case pid_intake_air_temp:

        *outValue = ((float) inPID->value) - 40;
        retVal = true;
        break;

    case pid_maf_rate:

        *outValue = ((float) inPID->value) / 100;
        retVal = true;
        break;

    /* Bit encoded */
    case pid_cmd_secondary_air_status:

        retVal = false;
        break;

    /* Bit encoded */
    case pid_o2_sensors_present_2b_4s:

        retVal = false;
        break;

    /* Multiple return values */
    case pid_o2_primary_bank_1_sensor_1:
    case pid_o2_primary_bank_1_sensor_2:
    case pid_o2_primary_bank_1_sensor_3:
    case pid_o2_primary_bank_1_sensor_4:
    case pid_o2_primary_bank_2_sensor_1:
    case pid_o2_primary_bank_2_sensor_2:
    case pid_o2_primary_bank_2_sensor_3:
    case pid_o2_primary_bank_2_sensor_4:

        retVal = false;
        break;

    /* Bit encoded */
    case pid_obd_std:

        retVal = false;
        break;

    /* Bit encoded */
    case pid_o2_sensors_present_4b_2s:

        retVal = false;
        break;

    /* Bit encoded */
    case pid_aux_input_status:

        retVal = false;
        break;

    case pid_fuel_rail_pressure_manifold:

        *outValue = ((float) inPID->value) * 0.079;
        retVal = true;
        break;

    case pid_fuel_rail_pressure_gauge:

        *outValue = ((float) inPID->value) * 10;
        retVal = true;
        break;

    /* Multiple return values */
    case pid_o2_wr_lambda_volt_s1:
    case pid_o2_wr_lambda_volt_s2:
    case pid_o2_wr_lambda_volt_s3:
    case pid_o2_wr_lambda_volt_s4:
    case pid_o2_wr_lambda_volt_s5:
    case pid_o2_wr_lambda_volt_s6:
    case pid_o2_wr_lambda_volt_s7:
    case pid_o2_wr_lambda_volt_s8:

        retVal = false;
        break;

    case pid_egr_error:

        *outValue = ((float) inPID->value - 128) * 100 / 128;
        retVal = true;
        break;

    case pid_evap_sys_vapor_pressure_sm:

        /* Find the two's complement value of the given number */
        if(inPID->value >= 0x8000) {
            *outValue = 0 - ((float)((~(inPID->value) & 0xFFFF) + 1));

        } else {
            *outValue = (float) inPID->value;
        }

        *outValue = *outValue / 4;
        retVal = true;
        break;

    /* Multiple return values */
    case pid_o2_wr_lambda_current_s1:
    case pid_o2_wr_lambda_current_s2:
    case pid_o2_wr_lambda_current_s3:
    case pid_o2_wr_lambda_current_s4:
    case pid_o2_wr_lambda_current_s5:
    case pid_o2_wr_lambda_current_s6:
    case pid_o2_wr_lambda_current_s7:
    case pid_o2_wr_lambda_current_s8:

        retVal = false;
        break;

    case pid_catalyst_temp_b1_s1:
    case pid_catalyst_temp_b2_s1:
    case pid_catalyst_temp_b1_s2:
    case pid_catalyst_temp_b2_s2:

        *outValue = ((float) inPID->value) / 10 - 40;
        retVal = true;
        break;

    /* Bit encoded */
    case pid_drive_cycle_monitor_status:

        retVal = false;
        break;

    case pid_control_module_volt:

        *outValue = ((float) inPID->value) / 1000;
        retVal = true;
        break;

    case pid_absolute_load:

        *outValue = ((float) inPID->value) * 100 / 255;
        retVal = true;
        break;

    case pid_cmd_equiv_ratio:

        *outValue = ((float) inPID->value) / 32768;
        retVal = true;
        break;

    case pid_ambient_air_temp:

        *outValue = ((float) inPID->value) - 40;
        retVal = true;
        break;

    /* Multiple return values */
    case pid_max_equiv_o2_manifold:

        retVal = false;
        break;

    case pid_max_maf:

        *outValue = ((float)(inPID->value >> 24)) * 10;
        retVal = true;
        break;

    /* Bit encoded value */
    case pid_fuel_type:

        retVal = false;
        break;

    case pid_evap_sys_vapor_pressure_abs:

        *outValue = ((float) inPID->value) / 200;
        retVal = true;
        break;

    case pid_evap_sys_vapor_pressure_big:

        *outValue = ((float) inPID->value) - 32767;
        retVal = true;
        break;

    /* Multiple return values */
    case pid_o2_second_bank_1_sensor_3:
    case pid_o2_second_bank_1_sensor_4:
    case pid_o2_second_bank_2_sensor_3:
    case pid_o2_second_bank_2_sensor_4:
        break;

    case pid_fuel_rail_pressure_abs:

        *outValue = ((float) inPID->value) * 10;
        retVal = true;
        break;

    case pid_engine_oil_temp:

        *outValue = ((float) inPID->value) - 40;
        retVal = true;
        break;

    case pid_fuel_injection_timing:

        *outValue = (((float) inPID->value) - 26880) / 128;
        retVal = true;
        break;

    case pid_engine_fuel_rate:

        *outValue = ((float) inPID->value) * 0.05;
        retVal = true;
        break;

    /* Bit encoded */
    case pid_emission_requirements:

        retVal = false;
        break;

    case pid_engine_torque_driver:

        *outValue = ((float) inPID->value) - 125;
        retVal = true;
        break;

    case pid_engine_torque_actual:

        *outValue = ((float) inPID->value) - 125;
        retVal = true;
        break;

        /* Multiple return values */
        /* Cannot do, needs 5 bytes */
#if 0

    case pid_engine_torque_percent_data:

        retValue = false;
        break;
#endif

    /* Bit encoded */
    case pid_aux_input_output_support:

        retVal = false;
        break;

    default:

        retVal = false;
        break;


    }

    return retVal;
}

/**
 * Calculates the percentage from the data assuming the value is less
 * than 256
 *
 * Used by PIDs
 *     -0x04 Engine Load
 *     -0x11 Throttle percentage
 *     -0x45 Relative throttle position
 *     -0x47 Absolute throttle position B
 *     -0x48 Absolute throttle position C
 *     -0x49 Absolute throttle position D
 *     -0x4A Absolute throttle position E
 *     -0x4B Absolute throttle position F
 *     -0x4C Commanded throttle actuator
 *     -0x52 Ethanol fuel percentage
 *     -0x5A Relative accelerator position
 *     -0x5B Hybrid battery pack remaining life
 *
 * @param inData    Data received from OBD port
 *
 * /return  Percentage from 0% to 100%
 */
float obd_general_percentage(uint32_t *inData)
{
    return ((float) * inData) * 100 / 255;
}

/**
 * Calculates the engine coolant temperature
 *
 * Used by PIDs
 *     -0x05 Engine coolant temperature
 *
 * @param inData    Data received from OBD port
 *
 * /return  Temperature in Celsius from -40 to 215.
 */
uint32_t obd_coolant_temp(uint32_t *inData)
{
    return *inData - 40;
}

/**
 * Calculates the fuel trim based on the inputted data
 *
 * Used by PIDs
 *     -0x06 Short term fuel percent trim - Bank 1
 *     -0x07 Long term fuel percent trim - Bank 1
 *     -0x08 Short term fuel percent trim - Bank 2
 *     -0x09 Long term fuel percent trim - Bank 2
 *
 * @param inData    Data received from OBD port
 *
 * /return  Fuel trim percentage from -100% to 99.22%
 */
float obd_fuel_trim(uint32_t *inData)
{
    return (*inData - 128) * 100 / 128;
}

/**
 * Calculates the fuel pressure
 *
 * Used by PIDs
 *     -0x0A Fuel pressure
 *
 * @param inData    Data received from OBD port
 *
 * /return  Fuel pressure from 0 to 765 kPa (gauge)
 */
uint16_t obd_fuel_pressure(uint32_t *inData)
{
    return *inData * 3;
}

/**
 * Pointless function that passes the input data back.
 *
 * Used by PIDs
 *     -0x0B Intake manifold absolute pressure
 *     -0x0D Vehicle speed
 *
 * @param inData    Data received from OBD port
 *
 * /return  Input data
 */
uint32_t obd_general_pass(uint32_t *inData)
{
    return *inData;
}

/**
 * Calculates the engine RPM
 *
 * Used by PIDs
 *     -0x0C Engine RPM
 *
 * @param inData    Data received from OBD port
 *
 * /return  Engine speed from 0 to 16,383.75 RPM
 */
float obd_engine_rpm(uint32_t *inData)
{
    return *inData / 4;
}

/**
  * Calculates timing advance relative to #1 cylinder
  *
  * Used by PIDs
  *     -0x0E Timing advance
  *
  * @param inData   Data received from OBD port
  *
  * /return Timing advance in degrees relative to #1 cylinder from -64 to 63.5
  */
float obd_timing_advance(uint32_t *inData)
{
    return *inData / 2 - 64;
}

/**
  * Calculates the intake air temperature
  *
  * Used by PIDs
  *     -0x0F Intake air temperature
  *
  * @param inData   Data received from OBD port
  *
  * /return Intake air temperature from -40 to 215 degrees Celsius
  */
uint32_t obd_intake_air_temperature(uint32_t *inData)
{
    return *inData - 40;
}

/**
 * Calculates the mass air flow rate
 *
 * Used by PIDs
 *     -0x10 Engine RPM
 *
 * @param inData    Data received from OBD port
 *
 * /return  Mass air flow rate from 0 to 655.35 grams / sec
 */
float obd_maf_rate(uint32_t *inData)
{
    return (*inData) / 4;
}

/**
 * Calculates the oxygen sensor voltage or short term fuel trim
 *
 * Used by PIDs
 *     -0x14 Bank 1, Sensor 1 oxygen sensor
 *     -0x15 Bank 1, Sensor 2 oxygen sensor
 *     -0x16 Bank 1, Sensor 3 oxygen sensor
 *     -0x17 Bank 1, Sensor 4 oxygen sensor
 *     -0x18 Bank 2, Sensor 1 oxygen sensor
 *     -0x19 Bank 2, Sensor 2 oxygen sensor
 *     -0x1A Bank 2, Sensor 3 oxygen sensor
 *     -0x1B Bank 2, Sensor 4 oxygen sensor
 *
 * @param inData    Data received from OBD port
 *
 * /return  Struct containing voltage from 0 to 1.275 volts and
 *          short term trim from -100% (lean) to 99.2% (rich)
 */
obd_oxygen_voltage_value obd_oxygen_voltage_sensor(uint32_t *inData)
{
    obd_oxygen_voltage_value outValue;
    outValue.voltage = (*inData >> 8) / 200;
    outValue.trim = ((*inData | 0xFF) - 128) * 100 / 128;

    return outValue;
}

/**
 * Calculates the engine runtime
 *
 * Used by PIDs
 *     -0x1F Run time since engine start
 *
 * @param inData    Data received from OBD port
 *
 * /return  Time in seconds since the engine started
 */
uint16_t obd_run_time(uint32_t *inData)
{
    return *inData;
}

/**
 * Calculates the distance since the Malfunction Indicator Lamp (MIL) or
 * check engine lamp turned on
 *
 * Used by PIDs
 *     -0x1F Distance traveled with malfunction idncator lamp (MIL) on
 *
 * @param inData    Data received from OBD port
 *
 * /return  Distance in kilometers
 */
uint16_t obd_mil_distance(uint32_t *inData)
{
    return (*inData);
}

/**
 * Calculates the fuel rail pressure relative to the manifold vacuum
 *
 * Used by PIDs
 *     -0x22 Fuel rail pressure relative to manifold vacuum
 *
 * @param inData    Data received from OBD port
 *
 * /return  Pressure in kPa from 0 to 5177.265kPa
 */
float obd_fuel_rail_pressure_vacuum(uint32_t *inData)
{
    return (*inData) * 0.079;
}

/**
 * Calculates the fuel rail pressure for diesel or gasoline direct injection
 *
 * Used by PIDs
 *     -0x23 Fuel rail pressure (diesel or gasoline direct inject)
 *
 * @param inData    Data received from OBD port
 *
 * /return  Pressure in kPa (gauge) from 0 to 65,535kPa
 */
uint32_t obd_fuel_rail_pressure_gauge(uint32_t *inData)
{
    return (*inData) * 10;
}

/**
 * Calculates the oxygen sensor lambda equivalence ratio or voltage
 *
 * Used by PIDs
 *     -0x24 Wide band oxygen sensor 1 lambda equivalence ratio and voltage
 *     -0x25 Wide band oxygen sensor 2 lambda equivalence ratio and voltage
 *     -0x26 Wide band oxygen sensor 3 lambda equivalence ratio and voltage
 *     -0x27 Wide band oxygen sensor 4 lambda equivalence ratio and voltage
 *     -0x28 Wide band oxygen sensor 5 lambda equivalence ratio and voltage
 *     -0x29 Wide band oxygen sensor 6 lambda equivalence ratio and voltage
 *     -0x2A Wide band oxygen sensor 7 lambda equivalence ratio and voltage
 *     -0x2B Wide band oxygen sensor 8 lambda equivalence ratio and voltage
 *
 * @param inData    Data received from OBD port
 *
 * /return  Struct containing the sensors voltage from 0 - 8V and
 *          equivalence ratio from 0 - 2
 */
obd_wr_lambda_voltage_value obd_oxygen_sensor_wr_lambda_voltage(uint32_t *inData)
{
    obd_wr_lambda_voltage_value outValue;
    outValue.voltage = (*inData >> 8) * 2 / 65535;
    outValue.equivRatio = (*inData | 0xFFFF) * 2 / 65535;

    return outValue;
}

/**
 * Reads from the vehicle and calculates the fuel economy
 *
 * If a value is requested from the OBD port but not obd_received,
 * the value econ value will be unchanged.
 *
 * @param econ Where to store the calculated fuel economy
 * @param unit The type of units to use when calculating
 *					- metric - L / 100 km
 *					- imperial - miles per gallon
 *
 * @return If all values requested are obd_received from the OBD port
 *          true will be returned, otherwise it will be false.
 */
bool obd_get_fuel_economy(obd_unit_type unit, obd_value_with_unit *outValue)
{
    uint32_t val;
    float maf;

    /* Get maf */
    if(!obd_query_pid(0x10, &val)) {
        return false;
    }

    maf = (float) val;
    maf = maf / 100;

    /* Get speed */
    if(!obd_query_pid(0x0d, &val)) {
        return false;
    }

    obd_calc_fuel_economy(val, maf, unit, outValue);

    return true;
}

/**
 * Calculates the fuel economy from the given speed and mass air flow
 * rate.
 *
 * @param kmPerHr   Vehicle speed in km/hr
 * @param maf       Mass air flow rate in g/sec
 * @param unit      Is the returned value to be in imperial or metric units
 *
 * @return Fuel economy in Miles / Gallon or L / 100 km
 */
void obd_calc_fuel_economy(uint32_t kmPerHr, float maf, obd_unit_type unit, obd_value_with_unit *outValue)
{

    /*
     * (E0 Gas)
     * MPG = (14.7  * 6.17  * 454 * VSS * 0.621371) / (3600 * MAF / 100)
     * MPG = 710.7 * VSS / MAF (With E0 Gasoline)
     * L / 100k = (3600 * MAF / 100) / (14.7 * 739.3 * VSS * 100)
     * L / 100k = (MAF / VSS) / (30188)
     *
     * (E10 Gas)
     * MPG = (14.13 * 6.211 * 454 * VSS * 0.621371) / (3600 * MAF / 100)
     * MPG = 687.7 * VSS / MAF (With E10 Gasoline)
     * L / 100k = (3600 * MAF / 100) / (14.7 * 744.2 * VSS * 100)
     * L / 100k = (MAF / VSS) / (30388)
     *
     * MPG - miles per gallon
     *
     * 14.7 grams of air to 1 gram of gasoline - ideal air/fuel ratio
     * 6.17 pounds per gallon (739.3 g/L) E0 gasoline
     *
     * 14.13 grams of air to 1 gram of E10
     * 6.58 pounds per gallon for E100
     * 6.211 pounds per gallon (744.2 g/L) E10
     *
     * 454 grams per pound - conversion
     * VSS - vehicle speed in kilometers per hour
     * 0.621371 miles per hour/kilometers per hour - conversion
     * 3600 seconds per hour - conversion
     * MAF - mass air flow rate in 100 grams per second
     * 100 - to correct MAF to give grams per second
     */


    /* Unit must always be assigned, regardless of MAF value */
    if(unit == obd_unit_english) {
        outValue->unit = obd_units_eng_mpg;

    } else {
        outValue->unit = obd_units_metric_l_100km;
    }

    if(maf > 0) {
        if(unit == obd_unit_english) {
            outValue->value = 687.7 * kmPerHr / maf;

        } else {
            //@TODO Verify calculation
            outValue->value = maf / kmPerHr / 30388;
        }
    }
}

/**
 * Finds the given value in the desired unit type for the given PID
 *
 * Units were found in SAE1979 document.
 *
 * @param inPid     PID containing the value to be converted
 * @param inType    Return the value in the metric or imperial unit?
 * @param outValue  Where the value and unit combination will be stored
 *
 * @return True, if the translation was successful
 */
bool obd_translate_pid(obd_pid *inPID, obd_value_with_unit *outValue)
{
    bool retVal;

    retVal = obd_get_metric_value(inPID, &(outValue->value));

    switch(inPID->pid)	{

    /* Nothing is going to happen to these numbers, but the least you can
    do is let the caller know that these PIDs are bitwise encoded */
    case pid_supported_01_20:
    case pid_supported_21_40:
    case pid_supported_41_60:
    case pid_supported_61_80:
    case pid_monitor_status:
    case pid_fuel_system_status:
    case pid_cmd_secondary_air_status:
    case pid_o2_sensors_present_2b_4s:
    case pid_obd_std:
    case pid_o2_sensors_present_4b_2s:
    case pid_aux_input_status:
    case pid_drive_cycle_monitor_status:
    case pid_fuel_type:
    case pid_emission_requirements:
        outValue->unit = obd_units_bitwise;
        break;

    /* Nothing will happen to the values here, let the caller know that
    there are multiple values stored in the PID value */
    case pid_o2_primary_bank_1_sensor_1:
    case pid_o2_primary_bank_1_sensor_2:
    case pid_o2_primary_bank_1_sensor_3:
    case pid_o2_primary_bank_1_sensor_4:
    case pid_o2_primary_bank_2_sensor_1:
    case pid_o2_primary_bank_2_sensor_2:
    case pid_o2_primary_bank_2_sensor_3:
    case pid_o2_primary_bank_2_sensor_4:
    case pid_o2_wr_lambda_volt_s1:
    case pid_o2_wr_lambda_volt_s2:
    case pid_o2_wr_lambda_volt_s3:
    case pid_o2_wr_lambda_volt_s4:
    case pid_o2_wr_lambda_volt_s5:
    case pid_o2_wr_lambda_volt_s6:
    case pid_o2_wr_lambda_volt_s7:
    case pid_o2_wr_lambda_volt_s8:
    case pid_o2_wr_lambda_current_s1:
    case pid_o2_wr_lambda_current_s2:
    case pid_o2_wr_lambda_current_s3:
    case pid_o2_wr_lambda_current_s4:
    case pid_o2_wr_lambda_current_s5:
    case pid_o2_wr_lambda_current_s6:
    case pid_o2_wr_lambda_current_s7:
    case pid_o2_wr_lambda_current_s8:
    case pid_max_equiv_o2_manifold:
    case pid_o2_second_bank_1_sensor_3:
    case pid_o2_second_bank_1_sensor_4:
    case pid_o2_second_bank_2_sensor_3:
    case pid_o2_second_bank_2_sensor_4:
        outValue->unit = obd_units_multiple;
        break;

    /* These values are unitless */
    case pid_freeze_dtc:
    case pid_warm_up_since_dtc_clear:
    case pid_cmd_equiv_ratio:
        outValue->unit = obd_units_none;
        break;

    /* Simple percentage */
    case pid_engine_load:
    case pid_bank_1_short_term_trim:
    case pid_bank_1_long_term_trim:
    case pid_bank_2_short_term_trim:
    case pid_bank_2_long_term_trim:
    case pid_throttle_pos:
    case pid_cmd_egr:
    case pid_egr_error:
    case pid_cmd_evaporative_purge:
    case pid_fuel_level_input:
    case pid_absolute_load:
    case pid_relative_throttle_pos:
    case pid_absolute_throttle_pos_b:
    case pid_absolute_throttle_pos_c:
    case pid_absolute_throttle_pos_d:
    case pid_absolute_throttle_pos_e:
    case pid_absolute_throttle_pos_f:
    case pid_cmd_throttle_actuator:
    case pid_ethanol_percent:
    case pid_accelerator_pedal_pos:
    case pid_hybrid_battery_life_left:
    case pid_engine_torque_driver:
    case pid_engine_torque_actual:
        outValue->unit = obd_units_percent;
        break;

    /* Temperature in Celsius and Fahrenheit */
    case pid_engine_coolant_temp:
    case pid_intake_air_temp:
    case pid_catalyst_temp_b1_s1:
    case pid_catalyst_temp_b2_s1:
    case pid_catalyst_temp_b1_s2:
    case pid_catalyst_temp_b2_s2:
    case pid_ambient_air_temp:
    case pid_engine_oil_temp:
        if(inPID->unit_type == obd_unit_metric) {
            outValue->unit = obd_units_metric_deg_c;

        } else {
            outValue->value = (outValue->value) * 9 / 5 + 32;
            outValue->unit = obd_units_eng_deg_f;
        }

        break;

    /* Pressure in kPa and PSI */
    case pid_fuel_pressure:
    case pid_fuel_rail_pressure_manifold:
    case pid_fuel_rail_pressure_gauge:
    case pid_fuel_rail_pressure_abs:
        if(inPID->unit_type == obd_unit_metric) {
            outValue->unit = obd_units_metric_kPa;

        } else {
            outValue->value = (outValue->value) * 0.1450378;
            outValue->unit = obd_units_eng_psi;
        }

        break;

    /* Pressure in kPa and inHg */
    case pid_intake_manifold_pressure:
    case pid_barometric_pressure:
        if(inPID->unit_type == obd_unit_metric) {
            outValue->unit = obd_units_metric_kPa;

        } else {
            outValue->value = (outValue->value) * 0.295300;
            outValue->unit = obd_units_eng_inHg;
        }

        break;

    /* Pressure in kPa and inH20 */
    case pid_evap_sys_vapor_pressure_abs:
        if(inPID->unit_type == obd_unit_metric) {
            outValue->unit = obd_units_metric_kPa;

        } else {
            outValue->value = (outValue->value) * 4.01463;
            outValue->unit = obd_units_eng_inH20;
        }

        break;

    /* Pressure in Pa and inH20 */
    case pid_evap_sys_vapor_pressure_sm:
    case pid_evap_sys_vapor_pressure_big:
        if(inPID->unit_type == obd_unit_metric) {
            outValue->unit = obd_units_metric_Pa;

        } else {
            outValue->value = (outValue->value) * 0.004014631;
            outValue->unit = obd_units_eng_inH20;
        }

        break;

    /* Engine speed in RPM */
    case pid_engine_rpm:
        outValue->unit = obd_units_rpm;
        break;

    /* Vehicle speed in km/h and mph */
    case pid_vehicle_speed:
        if(inPID->unit_type == obd_unit_metric) {
            outValue->unit = obd_units_metric_km_h;

        } else {
            outValue->value = (outValue->value) * 0.621371;
            outValue->unit = obd_units_eng_mph;
        }

        break;

    /* Degrees like the angle not the temperature */
    case pid_timing_advance:
    case pid_fuel_injection_timing:
        outValue->unit = obd_units_degrees;
        break;

    /* Mass air flow rates in grams/s and lbs/min */
    case pid_maf_rate:
    case pid_max_maf:
        if(inPID->unit_type == obd_unit_metric) {
            outValue->unit = obd_units_metric_g_s;

        } else {
            outValue->value = (outValue->value) * 0.132277357;
            outValue->unit = obd_units_eng_lb_min;
        }

        break;

    /* Time in seconds */
    case pid_runtime_since_engine_start:
        outValue->unit = obd_units_seconds;
        break;

    /* Time in minutes */
    case pid_time_run_with_mil:
    case pid_time_since_dtc_cleared:
        outValue->unit = obd_units_minutes;
        break;

    /* Distance in miles */
    case pid_dist_with_mil_on:
    case pid_dist_since_dtc_clear:
        if(inPID->unit_type == obd_unit_metric) {
            outValue->unit = obd_units_metric_km;

        } else {
            outValue->value = (outValue->value) * 0.621371192;
            outValue->unit = obd_units_eng_miles;
        }

        break;

    /* Voltage in volts, as if there were another unit */
    case pid_control_module_volt:
        outValue->unit = obd_units_volts;
        break;

    /* Fuel rate in liters/hour and gallons/hour */
    case pid_engine_fuel_rate:
        if(inPID->unit_type == obd_unit_metric) {
            outValue->unit = obd_units_metric_L_h;

        } else {
            outValue->value = (outValue->value) * 0.264172;
            outValue->unit = obd_units_eng_ga_h;
        }

        break;

    /* Torque in Newton*Meters and Foot*Pounds */
    case pid_engine_torque_ref:
        if(inPID->unit_type == obd_unit_metric) {
            outValue->unit = obd_units_metric_Nm;

        } else {
            outValue->value = (outValue->value) * 0.7375621483695506;
            outValue->unit = obd_units_eng_ftlb;
        }

        break;

    /* An invalid PID was given */
    default:
        outValue->unit = obd_units_invalid;
        return false;
        break;
    }

    return retVal;
}

/**
 * Translates the values on the non-standard PIDs
 *
 * @param inPid Pid to translate. Single value PIDs must have stored values,
 *              but PIDs that require multiple values, e.g. fuel economy,
 *              will use other functions to get those values
 * @param outValue  Translated value with a unit
 *
 * @return  True, if the translation was successful
 */
bool obd_translate_nonstd_pid(obd_pid *inPid, obd_value_with_unit *outValue)
{
    bool retVal = false;
    uint32_t speed;
    uint32_t maf;

    switch(inPid->pid) {
    case pid_fuel_economy:

        if(obd_get_most_recent_value(&pidsToQuery,
                                     pid_vehicle_speed,
                                     &speed) &&
                obd_get_most_recent_value(&pidsToQuery,
                                          pid_maf_rate,
                                          &maf)) {

            obd_calc_fuel_economy(speed,
                                  maf,
                                  inPid->unit_type,
                                  outValue);

            retVal = true;
        }


        break;

    default:
        outValue->unit = obd_units_invalid;
        retVal = false;
        break;
    }

    return retVal;
}

/**
 * Checks if the given PID is an accelerometer PID and if it is processes
 * it
 *
 * @param pidNumber Number of the PID to check and process
 * @param outValue  Where to store the value
 *
 * @return The PID was for an accelerometer value
 */
bool obd_check_and_process_accel_pid(uint8_t pidNumber, obd_value_with_unit *outValue)
{
    int16_t force;

    if(pidNumber == pid_accelerometer_x) {
        force = accel_calc_in_g(&(accelChans.axis_x));

    } else if(pidNumber == pid_accelerometer_y) {
        force = accel_calc_in_g(&(accelChans.axis_y));

    } else if(pidNumber == pid_accelerometer_z) {
        force = accel_calc_in_g(&(accelChans.axis_z));

    } else {
        return false;
    }

    outValue->value = ((float) force) / 1000;

    outValue->unit = obd_units_gravity;

    return true;
}

/**
 * Only adds OBD PIDs to the obd_pid_list and adds the components of
 * composite PIDs
 *
 * @param inList List to add the PID to
 * @param newPID PID to add
 *
 * @return  Is the PID now in the list of PIDs? If it was already in
 *          the list this will return true
 */
bool obd_add_real_pids_to_list(volatile obd_pid_list *inList, uint8_t inPid)
{
    bool addPid;
    addPid = false;

    if(inPid == pid_fuel_economy) {
        /* Fuel economy requires multiple inputs to be calculated */
        if(obd_add_pid_to_list(inList, pid_vehicle_speed)) {
            if(!obd_add_pid_to_list(inList, pid_maf_rate)) {
                /* Remove the vehicle speed since the MAF add failed */
                obd_remove_pid_from_list(inList, pid_vehicle_speed);
                addPid = false;
            }

        } else {
            addPid = false;
        }

    } else if((inPid != pid_accelerometer_x) &&
              (inPid != pid_accelerometer_y) &&
              (inPid != pid_accelerometer_z)) {
        addPid = obd_add_pid_to_list(inList, inPid);
    }
    return addPid;
}