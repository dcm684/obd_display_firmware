/*
 * OBD.h
 *
 * Created: 9/7/2012 3:12:41 PM
 *  Author: Meyer
 */

#ifndef OBD_H_
#define OBD_H_

#include <stdbool.h>
#include <inttypes.h>
#include <stdio.h>
#include <stdint.h>


/* /def OBD_DEBUG
 * Prints debug strings for this file to the uart
 */
//#define OBD_DEBUG

/* /def CAN_LOOPBACK_TEST Puts the system in loopback and doesn't turn
on the OBD receive filter */
//#define CAN_LOOPBACK_TEST

/* /def OBD_TIMEOUT Number 1ms intervals before assuming a OBD request
		failed */
#define OBD_TIMEOUT 400 /* 250 ms is too short */

/* /def OBD_PID_LIST_MAX
 *	Maximum number of PIDs that monitored at one time
 */
#define OBD_PID_LIST_MAX 40

/*!
 * English or metric unit?
 */
typedef enum {
    obd_unit_metric,						/* Metric units */
    obd_unit_english,						/* English / Imperial units */
} obd_unit_type;

/*!
 * The state of an OBD request
 */
typedef enum {
    obd_sent,								/* Request is pending */
    obd_received,							/* Request was successful */
    obd_timed_out							/* Request timed out */
} obd_request_state;

/*!
 *	Container for a PID, raw value, unit type, and request state
 */
typedef struct {
    uint8_t pid;							/* PID number, OBDII standard */
    volatile uint32_t value;				/* Raw value received from vehicle */
    volatile obd_request_state rx_state;	/* State of the request */
    obd_unit_type unit_type;				/* Desired unit, metric or imperial */
} obd_pid;

/*!
 * Container for all of the PIDs to be requested and the number of
 * PIDs being requested
 */
typedef struct {
    uint8_t length;							/* Number of PIDs in the list */
    obd_pid listOfPIDs[OBD_PID_LIST_MAX];	/* Array of PIDs */
} obd_pid_list;

volatile obd_pid *pidsToRead;			/*< List of multiple PIDs that are to
											be read. The next PID that will
											be read is the first in the list */

volatile obd_pid *pendingPID;			/*< Address of the PID whose request
											is currently pending */

volatile bool allow_obd_int_processing; /*< Blocks OBD functions from
											running from the timer
											interrupt while another OBD
											function is running */


volatile uint16_t counter_obd_timeout;	/*< 1kHz counter used to determine
											if a OBD request is taking
											too long */

volatile bool pid_send_second_attempt;	/*< Is this the second try to send
											a PID? */

volatile uint8_t multiple_pids_remaining;	/*< Number of PIDs left to query
												in a list of requested PIDs */

volatile uint8_t pids_being_sent;		/*< Total number of PIDs that are
											being requested */

volatile bool pause_pid_requests;		/*< Don't send any more PID requests */
volatile bool pid_request_pending;		/*< Are there still PID requests
											pending */

volatile obd_pid_list pidsToQuery;		/*< List of PIDs that are to be
											queried */


bool obd_init(void);
void obd_timer_interrupt_handler(void);
bool obd_set_can_filters(void);
bool obd_query_pid(uint8_t code, volatile uint32_t *readData);
void obd_print_pids(FILE *outStream);

bool obd_query_multiple_pids(obd_pid *inPids, uint8_t numberOfPids);
bool obd_receive_requested(void);
bool obd_send_request(volatile obd_pid *inPID, bool secondAttempt);
void obd_send_multiple_requests(volatile obd_pid_list *inPIDList);
void obd_multiple_pid_request_next(void);

void obd_init_pid_list(volatile obd_pid_list *inList);
bool obd_add_pid_to_list(volatile obd_pid_list *inList, uint8_t newPID);
bool obd_remove_pid_from_list(volatile obd_pid_list *inList, uint8_t thePID);

bool obd_get_most_recent_value(volatile obd_pid_list *inList, uint8_t pidToGet, uint32_t *outValue);
bool obd_get_pid(volatile obd_pid_list *inList, uint8_t pidToGet, obd_pid *outPID);
#endif /* OBD_H_ */
