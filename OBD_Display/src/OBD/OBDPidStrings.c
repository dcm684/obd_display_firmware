/*
 * OBDPidStrings.c
 *
 * Created: 3/15/2013 11:26:15 AM
 *  Author: cmeyer
 */
#include <avr/pgmspace.h>

#include "OBDPidStrings.h"
#include "OBDPidFunctions.h"

const char PROGMEM obdUnitStrings[][OBD_UNIT_MAX_LENGTH] = {
    "",		/** < Multiple values are read by this PID */
    "",		/** < Unitless value */
    "",		/** < Bitwise value stored in data from vehicle */
    "%",		/** < Percentage */
    "V",		/** < Volts */
    "mA",		/** < Milliamps */
    "s",		/** < Seconds */
    "m",		/** < Minutes */
    "deg",		/** < Degrees, the angle */

    "C",		/** < Degrees celsius */
    "F",		/** < Degrees celsius */

    "Pa",		/** < Pascal */
    "kPa",		/** < kiloPascal */
    "inH2O",	/** < Inches of water */
    "inHg",	/** < Inches of mercury */
    "PSI",		/** < Pounds per square inch */

    "RPM",		/** < Rotations per minute */

    "g/s",		/** < Grams per second */
    "lb/min",	/** < Pounds per minute */

    "km",		/** < Kilometers */
    "mi",		/** < Miles */
    "km/h",	/** < Kilometers per hour */
    "mph",		/** < Miles per hour */

    "L/h",		/** < Liters per hour */
    "ga/h",	/** < Gallons per hour */

    "Nm",		/** < Newton meters */
    "ftlb",	/** < Foot pound */

    "L/100km",	/** < Liters / 100 km */
    "mpg",		/** < Miles per gallon */

    "gs",		/** < Force in gs */

    "",		/** < Invalid PID */

};